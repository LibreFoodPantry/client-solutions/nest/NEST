**Register the IP Address of the Device on Stargate**

The app will be running on either a virtual device or a physical device, thus you will need the IP address of the computer the virtual device is running on or the phone/tablet that the app is running on. To locate the IP address on:
- Windows - run _ipconfig_ at a command prompt and locate the IPv4 Address
- Android Device - Settings > About phone/tablet > Status information and locate the IP address

Go to:  https://stargate.ncc.edu/dbipregister/:
- Sign in to stargate using the Username and Password provided on Brightspace for dbipregister
- Enter your full name, N#, and the IP address of the device you want to register

Note that the registration information is updated on Stargate every 5 minutes. It is possible that the device will get a different IP address each time it connects to the Student WiFi, so you may need to go through the registration process again.

**Test your Network Connection From a Laptop/Desktop Computer**

In order to verify that the registration process has completed and your device is now registered, you can do the following on:
- Windows - run a powershell prompt and run the following command to test the network connection
 
    tnc stargate.ncc.edu -port 3306 
 
    If successful you should get results similar to this, note the sourceAddress will be different:
    
    ComputerName     : stargate.ncc.edu<br>
    RemoteAddress    : 198.38.8.56<br>
    RemotePort       : 3306<br>
    InterfaceAlias   : Ethernet 2<br>
    SourceAddress    : 10.14.4.172<br>
    TcpTestSucceeded : True<br>
    
    
    If you are not properly registered, you will get results similar to this, again the SourceAddress will be different:

    WARNING: TCP connect to (198.38.8.56 : 3307) failed
    
    ComputerName           : stargate.ncc.edu<br>
    RemoteAddress          : 198.38.8.56<br>
    RemotePort             : 3307<br>
    InterfaceAlias         : Ethernet 2<br>
    SourceAddress          : 10.14.4.172<br>
    PingSucceeded          : True<br>
    PingReplyDetails (RTT) : 0 ms<br>
    TcpTestSucceeded       : False<br>

**Test your Network Connection From an Android device (phone or tablet)**

You will need to install a port scanner app on the device (if it is not already installed). In the PlayStore search for the PortDroid app. Once the app is running you will:
- Choose PortScanner from the Tools menu
- Enter the IP address of Stargate - 198.38.8.56
- Make sure that 3306 is the only port listed under Ports (if you leave all ports that are listed by default, you are likely to get locked out)
- Uncheck the _Reachability Check_ checkbox
- Click SCAN

**To View the Remote Database on Stargate**

You can view the database on Stargate to ensure the guest information was properly added/deleted/updated.
- Go to: https://stargate.ncc.edu/phpmyadmin/index.php?route=/
- Sign in to stargate using the Username and Password provided on Brightspace for phyMyAdmin
- Log in to phyMyAdmin using the Username and Password provided on Brightspace. Please note that this information should NEVER be left is a public place!

