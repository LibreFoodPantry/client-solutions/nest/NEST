# Creating a new branch and setting up a merge request
**Note: Make sure you are in the main branch of the repository on your local machine and that it is up to date.**

1. To make it easy to identify what issue you will be working on in your branch you should start the branch name off with issue_#_ for example, issue_2_ followed by a brief description of what you will do.  For example, if your issue is issue # 51 you might name your branch issue_51_updating_documentation 
or if your issue is issue #74 you might name your branch issue_74_adding_a_license. Create a new branch by typing

`$ git branch BRANCH_NAME`

2. Change to that branch by typing

`$ git checkout BRANCH_NAME`

3. Create an empty commit. If you are working with other members of your team be sure to include your team members GitLab userids & emails as co-authors.  Each co-author will have to be specified a separate line of the commit message, so you cannot use the –m option.  Type  

`$ git commit --allow-empty`

Notice there are two - before allow (- - allow) but no space between them

This will open a vim window for you to type the commit message.  
Each co-author should appear in the message using his/her GitLab information after the label Co-authored-by: NAME <EMAIL>

For example,

Adding a license; 

Co-authored-by: Prof Postner Lori.Postner@ncc.edu

Co-authored-by: Prof Burdge Darci.Burdge@ncc.edu

To enter into insert mode in vim type 

**i**

then type your message – when you are done type

**Esc**

**:wq**

to save your message and quit the vim window.

4.	Push your branch to the NEST repository and set the upstream repository (for future pushes to the fork) by typing

`$ git push -u origin BRANCH_NAME`

5.	Open a merge request from within GitLab.  Make sure you are at LibreFoodPantry/client-solutions/NEST/NEST.  From the menu on the left, choose **Merge Requests** (if you don’t see it, make your browser full screen).  

6.	Click the **New merge request** button on the right side of the page. Select **LibreFoodPantry/client-solutions/nest/NEST / <your branch name>** on the left as the Source branch.  The Target branch should be **LibreFoodPantry/client-solutions/nest/NEST / main**.  This will take the branch you are working on and eventually merge it with the main branch of the original (upstream) repository.

7.	Click the **Compare branches and continue** button.

8.	On the merge request page:

    1. Add **Draft:** to the beginning of the default title.  This will help identify that your merge request is not ready to be merged in. 

    2. Select the merge request template.

    3. Fill out the parts of the template.  Under Related Issues put **Closes #issueNumber**, for example, `Closes #2` so that the issue you are working on will be automatically closed when the merge request is accepted.

    4.	Submit the merge request.
