# Working Locally and Staying Up To Date with the Upstream Repository

1.	As you work locally on the issue you’ve chosen, be sure to:

    1. Comment your code

    2. Commit often

    3. Give credit for code that you are moving from another part of the NEST app within your commit message (for example, “Copied the scanner code from Scanner.java to ScanFragment.java and updated to work from within a fragment”)

2.	Other students will be working at the same time, so you want to make sure your local copy is up to date and doesn’t have any conflicts before you push your changes up to the merge request.  To update your branch to match what is currently on GitLab type

`$ git checkout main`

`$ git pull`


Change back to the branch you’ve made changes on and merge the changes into it by typing

`$ git checkout BRANCH_NAME`

`$ git merge main`

Now your branch should be up to date with the remote main branch.  If you have any merge conflicts you will need to resolve them locally and repeat this process until all conflicts are resolved.

3.	Push to GitLab.  Confirm that your branch on GitLab has your change(s) and your merge request has been updated.

# Testing

4.	Before your merge request will be accepted you need to have two others do a code review by testing your branch.  You should notify others that your merge request is ready for testing by editing the merge reqest and adding the "**Ready to Test**" label (click _Edit_ next to _Labels_ on the right). The  person doing the testing should include a comment in the merge request to indicate that they are testing and then do the following

`$ git checkout main`

`$ git pull`

Which will pull down the most recent changes along with the branch to be tested.  Then change to the branch by typing

`$ git checkout BRANCH_NAME`

Now you can test that branch in Android Studio.  If you find any problems with the code for the issue, respond to the problem within the merge request to let the author know of the problem(s) you found. 

5.	 If the code fixes the issue the tester should put a comment into the merge request indicating that the code is ready to be merged and click _Approve_ in the merge request. Two people should be testing a merge request. The **second** person _**who tests and finds**_ that the code is ready to be merged should remove the "**Ready to Test**" label and _notifiy_ the author that testing is complete by including a comment and mentioning the author using @ followed by their userid. 

6.	The original author of the merge request should remove the **Draft:** prefix from the merge title by going to the merge request and clicking the Edit button to modify the title. Add a comment that the code is ready to be merged and let the maintainer know by “mentioning” them in your comment by using the @ followed by their userid.  The mention will automatically send the maintainer a notification. 
