package edu.ncc.nest.nestapp.GuestDatabaseRegistration.Fragments;

/**
 *
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.fragment.NavHostFragment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.ncc.nest.nestapp.GuestDatabaseRegistration.UIClasses.MultiSelectSpinner;
import edu.ncc.nest.nestapp.R;
import edu.ncc.nest.nestapp.databinding.FragmentGuestDatabaseRegistrationFourthFormBinding;
//import edu.ncc.nest.nestapp.databinding.FragmentGuestDatabaseRegistrationSecondFormBinding;

/**
 * FourthFormFragment: Represents a form that a guest can fill in with more of their information.
 * The fragment then bundles all of the user's inputs (including info passed from
 * {@link FourthFormFragment} and sends them to the next fragment {@link SummaryFragment}.
 */
public class FourthFormFragment extends Fragment {

    private FragmentGuestDatabaseRegistrationFourthFormBinding binding;

    private String householdNum, children1, children5,
            children12, children18;

    private Spinner numPeopleSpinner, children1Spinner,
            children5Spinner, children12Spinner, children18Spinner;

    //private MultiSelectSpinner childcareMultiSelect;

    private TextView textview_numPeople, textview_children1,
            textview_children5, textview_children12, textview_children18;

    private Button nextBtn;
    private Button nextBtnManual;

    // used as a final check on the next button
    private boolean goodData = false;

    private final String TAG = "**NumChildren Test**";

    // flags for hiding/showing views
    private boolean openView1, openView2, openView3, openView4, openView5 = false;
    // flag for disabling nextButton
    private boolean isDisabled = false;
    private boolean fixingInvalid = false;

    // store numPeople selections to validate the number of total people vs children
    private int numChildren1, numChildren5, numChildren12, numChildren18 = 0;
    private int numPeople = 1;  // default to 1 person (user)

    private Bundle result = new Bundle();

    private OnBackPressedCallback backbuttonCallback;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // implement binding to inflate layout
        binding = FragmentGuestDatabaseRegistrationFourthFormBinding.inflate(inflater, container, false);
        return binding.getRoot();

        // Inflate the layout for this fragment (deprecated since bundle added 11.2021)
        //return inflater.inflate(R.layout.fragment_guest_database_registration_fourth_form, container, false);

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        // override back button to give a warning
        backbuttonCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                // show dialog prompting user
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setCancelable(false);
                builder.setTitle(R.string.Are_you_sure);
                builder.setMessage(R.string.Data_on_this_page_may_not_be_saved);
                // used to handle the 'confirm' button
                builder.setPositiveButton(R.string.im_sure, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // continue by disabling this callback then calling the backpressedispatcher again
                        // when this was enabled, it was at top of backpressed stack. When we disable, the next item is default back behavior
                        backbuttonCallback.setEnabled(false);
                        getActivity().getOnBackPressedDispatcher().onBackPressed();
                    }
                });
                // handles the 'cancel' button
                builder.setNegativeButton(R.string.stay_on_page, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel(); // tells android we 'canceled', not dismiss. more appropriate
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        };
        // need to add the callback to the activities backpresseddispatcher stack.
        // if enabled, it will run this first. If disabled, it will run the default (next item in stack)
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), backbuttonCallback);

        // store the bindings so its cleaner
        numPeopleSpinner     = binding.grf4NumPeople;
        children1Spinner     = binding.grf4Children1;
        children5Spinner     = binding.grf4Children5;
        children12Spinner    = binding.grf4Children12;
        children18Spinner    = binding.grf4Children18;
        textview_numPeople   = binding.grf4TextviewNumPeople;
        textview_children1   = binding.grf4TextviewChildren1;
        textview_children5   = binding.grf4TextviewChildren5;
        textview_children12  = binding.grf4TextviewChildren12;
        textview_children18  = binding.grf4TextviewChildren18;

        nextBtn = binding.nextButtonFourthFragmentGRegistrationScanner;
        nextBtnManual = binding.nextButtonFourthFragmentGRegistrationManual;

        // load the multiselect using the class method (CHILDCARE STATUS IS REMOVED AS OF 5/10/2023)
        //childcareMultiSelect.setItems(getResources().getStringArray(R.array.childcare_status));

        // hide views until we need them
        //childcareMultiSelect.setVisibility(View.GONE);
        children1Spinner.setVisibility(View.GONE);
        children5Spinner.setVisibility(View.GONE);
        children12Spinner.setVisibility(View.GONE);
        children18Spinner.setVisibility(View.GONE);
        textview_children1.setVisibility(View.GONE);
        textview_children5.setVisibility(View.GONE);
        textview_children12.setVisibility(View.GONE);
        textview_children18.setVisibility(View.GONE);


        // set onItemSelectedListener for dropdowns.
        children1Spinner.setOnItemSelectedListener(children1Listener);
        children5Spinner.setOnItemSelectedListener(children5Listener);
        children12Spinner.setOnItemSelectedListener(children12Listener);
        children18Spinner.setOnItemSelectedListener(children18Listener);
        numPeopleSpinner.setOnItemSelectedListener(numPeopleListener);
        //(childcareMultiSelect).setOnTouchListener(childcareTouchListener);
        // TODO I can define a custom onclick listener in the MultiSelect class!

        binding.nextButtonFourthFragmentGRegistrationManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                householdNum = binding.grf4NumPeople.getSelectedItem().toString();
                children1 = binding.grf4Children1.getSelectedItem().toString();
                children5 = binding.grf4Children5.getSelectedItem().toString();
                children12 = binding.grf4Children12.getSelectedItem().toString();
                children18 = binding.grf4Children18.getSelectedItem().toString();

                // storing all strings in bundle to send to summary fragment
                result.putString("householdNum", householdNum);
                // pulling from instance variables as these will always be accurate
                result.putString("children1", Integer.toString(numChildren1));
                result.putString("children5", Integer.toString(numChildren5));
                result.putString("children12", Integer.toString(numChildren12));
                result.putString("children18", Integer.toString(numChildren18));

                getParentFragmentManager().setFragmentResult("sending_fourth_form_fragment_info", result);

                NavHostFragment.findNavController(FourthFormFragment.this)
                        .navigate(R.id.action_DBR_FourthFormFragment_to_DBR_ManualFragment);
            }
        });

        // set up on click listener for the 'next' button
        binding.nextButtonFourthFragmentGRegistrationScanner.setOnClickListener(v -> {

            // store the selected items into the instance variables
            householdNum = binding.grf4NumPeople.getSelectedItem().toString();
            children1 = binding.grf4Children1.getSelectedItem().toString();
            children5 = binding.grf4Children5.getSelectedItem().toString();
            children12 = binding.grf4Children12.getSelectedItem().toString();
            children18 = binding.grf4Children18.getSelectedItem().toString();

            // storing all strings in bundle to send to summary fragment
            result.putString("householdNum", householdNum);
            // pulling from instance variables as these will always be accurate
            result.putString("children1", Integer.toString(numChildren1));
            result.putString("children5", Integer.toString(numChildren5));
            result.putString("children12", Integer.toString(numChildren12));
            result.putString("children18", Integer.toString(numChildren18));
            /*
            result.putString("children1", numChildren1);
            result.putString("children5", children5);
            result.putString("children12", children12);
            result.putString("children18", children18);
             */


            //sending bundle
            getParentFragmentManager().setFragmentResult("sending_fourth_form_fragment_info", result);

            // navigate to the summary fragment when clicked
            NavHostFragment.findNavController(FourthFormFragment.this)
                    .navigate(R.id.action_DBR_FourthFormFragment_to_DBR_ScannerFragment);

            /*
            if (true) {

                getParentFragmentManager().setFragmentResult("sending_fourth_form_fragment_info", result);

                // navigate to the summary fragment when clicked
                NavHostFragment.findNavController(FourthFormFragment.this)
                        .navigate(R.id.action_DBR_FourthFormFragment_to_DBR_ScannerFragment);

            } else {
                Toast toast = Toast.makeText(getContext(), "Please make a selection before continuing.", Toast.LENGTH_SHORT);
                toast.show();
            } */

        });
    }


    private View.OnTouchListener childcareTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            Log.d("**ONTOUCH**", "In On Touch EVENT");


            /*
            if (event.getAction() == MotionEvent.ACTION_UP) {
                // call the perform click function using a callback. Callback will be run after dismiss() is pressed.
                boolean done = ((MultiSelectSpinner) v).performClickCallback(verifyMultiselect);
            }

             */
            return true;
        }
    };

    // runnable task that runs after called. Used as a callback to the multiselect dialog
    /*
    private Runnable verifyMultiselect = new Runnable() {
        @Override
        public void run() {
            Log.d("**ONTOUCH**", "In 'runnabl task'");
            MultiSelectSpinner v = childcareMultiSelect;
            List<Integer> selections = ((MultiSelectSpinner) v).getSelectedIndices();

            int count = selections.size();
            int iterator = 0;
            boolean looping = true;

            for (int i = 0; i < selections.size(); i++) {
                //while (count > iterator && looping) {
                // if any selection other than first option (no children) appears
                //Log.d(TAG, "SELECTIONS: " + selections.get(i).toString());
                // dont need to loop? Works better when not looping. First item will always be first position
                if (selections.get(0) == 0) {
                    // hide others
                    children1Spinner.setVisibility(View.GONE);
                    textview_children1.setVisibility(View.GONE);
                    childcareMultiSelect.setSelection(0);
                    looping = false;
                    // TODO add if disabled, enable code
                    if (isDisabled) {
                        setEnabled(nextBtn);
                    }
                } else {
                    // show others
                    children1Spinner.setVisibility(View.VISIBLE);
                    textview_children1.setVisibility(View.VISIBLE);
                }
                // reset next field
                children1Spinner.setSelection(0, true);
            }
            Log.d("**ONTOUCH**", "END 'runnable task'");
        }
    };

     */


    private final AdapterView.OnItemSelectedListener numPeopleListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            // if only one person, don't need to ask about other people in the household
            // pos 0 is placeholder, pos 1 = 1.

            /* CHILDCARE STATUS REMOVED AS OF 5/10/2023
            if(position > 1) {
                // TODO put back after multiselect merge
                childcareMultiSelect.setVisibility(View.VISIBLE);
                //childcareSpinner.setVisibility(View.VISIBLE);
                // reset selection to placeholder NOTE - Need animate:true to trigger the onItemSelectedListener
                childcareMultiSelect.clearSelections();
                //childcareSpinner.setSelection(0, true);
            } else {
                // TODO put back after multiselect merge
                childcareMultiSelect.setVisibility(View.GONE);
                //childcareSpinner.setVisibility(View.GONE);

                // set the selection to "i dont have children" to trigger the other ones
                childcareMultiSelect.setSelection(0);

                // TODO add if disabled, enable code
                if (isDisabled) {
                    setEnabled(nextBtn);
                }
            } */

            if(numPeople>position){
                if(textview_children5.getVisibility()==View.VISIBLE){
                    textview_children5.setVisibility(View.VISIBLE);
                    children5Spinner.setVisibility(View.VISIBLE);
                } if(textview_children12.getVisibility()==View.VISIBLE){
                    textview_children12.setVisibility(View.VISIBLE);
                    children12Spinner.setVisibility(View.VISIBLE);
                } if(textview_children18.getVisibility()==View.VISIBLE){
                    textview_children18.setVisibility(View.VISIBLE);
                    children18Spinner.setVisibility(View.VISIBLE);
                }
            }
            // starts at 1 = 1, so position = people.
            // already initialized to 1, so if position is 0 do nothing, else update numPeople to position.
            if (position > 0) {
                numPeople = position;
                updateSpinnerOptions(children1Spinner,children5Spinner,children12Spinner,children18Spinner,numPeople);
            }
            if(numPeople<=getNumChildren()){
                textview_numPeople.setText(R.string.numPeopleError);
                textview_numPeople.setTextColor(Color.RED);
            }else{
                textview_numPeople.setText(R.string.num_people_household);
                textview_numPeople.setTextColor(Color.BLACK);
            }

            if (position == 1 || position == 0) {
                resetSpinners();
                resetNumChildren();
                if (isDisabled) {
                    setNextEnabled();
                }
                children1Spinner.setVisibility(View.GONE);
                textview_children1.setVisibility(View.GONE);
                children5Spinner.setVisibility(View.GONE);
                textview_children5.setVisibility(View.GONE);
                children12Spinner.setVisibility(View.GONE);
                textview_children12.setVisibility(View.GONE);
                children18Spinner.setVisibility(View.GONE);
                textview_children18.setVisibility(View.GONE);
            } else {
                if(numPeople-1>getNumChildren()) {
                    children1Spinner.setVisibility(View.VISIBLE);
                    textview_children1.setVisibility(View.VISIBLE);
                }
            }

            // check for a number conflict each time. Handles if the user went down the chain and then changed top number
            if (isNumConflict()) {
                Log.d(TAG, "Num conflict found!");
                // TODO disable next button
                if (!isDisabled) {
                    setDisabled();
                }

            } else {
                Log.d(TAG, "No num conflict!");
                fixingInvalid = true;
                // Manually trigger other listeners to allow you to progress after fixing error
                // with current selected positions
                children1Listener.onItemSelected(children1Spinner, null, children1Spinner.getSelectedItemPosition(), children1Spinner.getSelectedItemPosition());
                children5Listener.onItemSelected(children5Spinner, null, children5Spinner.getSelectedItemPosition(), children5Spinner.getSelectedItemPosition());
                children12Listener.onItemSelected(children12Spinner, null, children12Spinner.getSelectedItemPosition(), children12Spinner.getSelectedItemPosition());
                children18Listener.onItemSelected(children18Spinner, null, children18Spinner.getSelectedItemPosition(), children18Spinner.getSelectedItemPosition());

                try {
                    numChildren1 = Integer.parseInt((String) children1Spinner.getSelectedItem());
                    Log.d(TAG, ""+numChildren1);
                } catch (Exception e) {
                    // do nothing
                    Log.d(TAG, (String) children1Spinner.getSelectedItem());
                }

                try {
                    numChildren5 = Integer.parseInt((String) children5Spinner.getSelectedItem());
                    Log.d(TAG, ""+numChildren5);
                } catch (Exception e) {
                    // do nothing
                    Log.d(TAG, (String) children5Spinner.getSelectedItem());
                }

                try {
                    numChildren12 = Integer.parseInt((String) children12Spinner.getSelectedItem());
                    Log.d(TAG, ""+numChildren12);
                } catch (Exception e) {
                    // do nothing
                    Log.d(TAG, (String) children12Spinner.getSelectedItem());
                }

                try {
                    numChildren18 = Integer.parseInt((String) children18Spinner.getSelectedItem());
                    Log.d(TAG, ""+numChildren18);
                } catch (Exception e) {
                    // do nothing
                    Log.d(TAG, (String) children18Spinner.getSelectedItem());
                }
            }



            // force multiselect verification after changing selection. (CHILDCARE STATUS REMOVED AS OF 5/10/2023)
            //verifyMultiselect.run();

            // TODO Remove Test Log
            Log.d(TAG, displayChildrenNums());

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private void updateSpinnerOptions(Spinner children1, Spinner children5, Spinner children12, Spinner children18, int numPeople) {
        // Calculate the remaining people available for the next spinner
        int remaining = numPeople - 1;



        // Create a list to hold the updated options for the spinner
        List<String> updatedValues = new ArrayList<>();
        updatedValues.add("Select One");  // Always allow for 0


        for (int i = 1; i <= remaining; i++) {
            updatedValues.add(String.valueOf(i));
        }

        // Create an adapter to set to the spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, updatedValues);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        int child1 = numChildren1;
        int child5 = numChildren5;
        int child12 = numChildren12;
        int child18 = numChildren18;
        children1.setAdapter(adapter);
        children5.setAdapter(adapter);
        children12.setAdapter(adapter);
        children18.setAdapter(adapter);
        if(!(numChildren1>remaining) && child1!=0)
            children1Spinner.setSelection(child1);
        if(!(numChildren5>remaining) && child5!=0)
            children5Spinner.setSelection(child5);
        if(!(numChildren12>remaining) && child12!=0)
            children12Spinner.setSelection(child12);
        if(!(numChildren18>remaining) && child18!=0)
            children18Spinner.setSelection(child18);
    }

    private final AdapterView.OnItemSelectedListener children1Listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            // 0 = placeholder, 1 = 0. Therefore, num = pos - 1
            //update numChildren1
            // reset it to 0 so it doesn't become -1
            if (position == 0 ) {
                numChildren1 = 0;
            }else
                numChildren1 = position;
            int otherPeople = numPeople - 1;        //remove self from total people
            int childrenNum = getNumChildren();     //calculate numChildren (total)

            // simple if else, hide next field if we are on the placeholder
            if (position == 0 && children5Spinner.getVisibility()!=View.VISIBLE) {
                children5Spinner.setVisibility(View.GONE);
                textview_children5.setVisibility(View.GONE);
            } else {
                if(numPeople-1>getNumChildren()) {
                    children5Spinner.setVisibility(View.VISIBLE);
                    textview_children5.setVisibility(View.VISIBLE);
                }
                // TODO add if disabled, enable code
                if (isDisabled) {
                    setNextEnabled();
                }
            }

            if (!(childrenNum > otherPeople)) {
                // reset text after error text when error fixed
                textview_children1.setTextColor(Color.BLACK);
                textview_children1.setText(R.string.num_children_under_1);
                textview_numPeople.setText(R.string.num_people_household);
                textview_numPeople.setTextColor(Color.BLACK);
            }

            // check if we exceeded. will override the simple if/else if there is a conflict
            if (childrenNum > otherPeople) {
                Log.d(TAG, "Num conflict in children 1!");
                textview_numPeople.setText(R.string.numPeopleError);
                textview_numPeople.setTextColor(Color.RED);
                // TODO disable next button
                if (!isDisabled) {
                    setDisabled();
                }

            } else if (childrenNum == otherPeople) {
                // valid, but need to disable other options
                // TODO add if disabled, enable code
                if (isDisabled) {
                    setNextEnabled();
                }
            }
            if (!fixingInvalid) {
                // reset next
                children5Spinner.setSelection(0, true);
            }
            // TODO Remove Test Log
            if(children5Spinner.getVisibility() == View.VISIBLE){
                if(getNumChildren()<numPeople && children5Spinner.getSelectedItemPosition()!=0){
                    children12Spinner.setVisibility(View.VISIBLE);
                    textview_children12.setVisibility(View.VISIBLE);
                }
            }
                if(children12Spinner.getVisibility() == View.VISIBLE){
                if(getNumChildren()<numPeople && children12Spinner.getSelectedItemPosition()!=0){
                    children18Spinner.setVisibility(View.VISIBLE);
                    textview_children18.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private final AdapterView.OnItemSelectedListener children5Listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            // 0 = placeholder, 1 = 0. Therefore, num = pos - 1
            numChildren5 = position;            //update numChildren5
            // reset it to 0 so it doesn't become -1
            if (numChildren5 < 0) {
                numChildren5 = 0;
            }
            int otherPeople = numPeople - 1;        //remove self from total people
            int childrenNum = getNumChildren();     //calculate numChildren (total)

            // simple if/else. Hide next if on placeholder
            if (position == 0 && children12Spinner.getVisibility()!=View.VISIBLE) {
                children12Spinner.setVisibility(View.GONE);
                textview_children12.setVisibility(View.GONE);
            } else {
                if(numPeople-1>getNumChildren()) {
                    children12Spinner.setVisibility(View.VISIBLE);
                    textview_children12.setVisibility(View.VISIBLE);
                }
                // TODO add if disabled, enable code
                if (isDisabled) {
                    setNextEnabled();
                }
            }

            if (!(childrenNum > otherPeople)) {
                // reset text after error text when error fixed
                textview_children5.setTextColor(Color.BLACK);
                textview_children5.setText(R.string.num_children_1_to_5);
                textview_numPeople.setText(R.string.num_people_household);
                textview_numPeople.setTextColor(Color.BLACK);
            }

            // check if we exceeded. will override simple if/else if conflict
            if (childrenNum > otherPeople) {
                Log.d(TAG, "Num conflict in children 5!");
                textview_numPeople.setText(R.string.numPeopleError);
                textview_numPeople.setTextColor(Color.RED);
                // TODO calls sometimes when first field is set to 1. Fix
                // TODO add disable next button
                if (!isDisabled) {
                    setDisabled();
                }

            } else if (childrenNum == otherPeople) {
                // valid, but need to disable other options
                // TODO add if disabled, enable code
                if (isDisabled) {
                    setNextEnabled();
                }
            }
            if (!fixingInvalid) {
                // reset next
                children12Spinner.setSelection(0, true);
            }
            // TODO Remove Test Log
            Log.d(TAG, displayChildrenNums());
            if(children12Spinner.getVisibility() == View.VISIBLE && children12Spinner.getSelectedItemPosition()!=0){
                if(getNumChildren()<numPeople){
                    children18Spinner.setVisibility(View.VISIBLE);
                    textview_children18.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private final AdapterView.OnItemSelectedListener children12Listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            // 0 = placeholder, 1 = 0. Therefore, num = pos - 1
            numChildren12 = position;            //update numChildren12
            // reset it to 0 so it doesn't become -1
            if (numChildren12 < 0) {
                numChildren12 = 0;
            }
            int otherPeople = numPeople - 1;        //remove self from total people
            int childrenNum = getNumChildren();     //calculate numChildren (total)

            // simple if/else. Hide next if on placeholder
            if (position == 0 && children18Spinner.getVisibility()!=View.VISIBLE) {
                children18Spinner.setVisibility(View.GONE);
                textview_children18.setVisibility(View.GONE);
            } else {
                if(numPeople-1>getNumChildren()) {
                    children18Spinner.setVisibility(View.VISIBLE);
                    textview_children18.setVisibility(View.VISIBLE);
                }
                // TODO add if disabled, enable code
                if (isDisabled) {
                    setNextEnabled();
                }
            }

            if (!(childrenNum > otherPeople)) {
                // reset text after error text when error fixed
                textview_children12.setTextColor(Color.BLACK);
                textview_children12.setText(R.string.num_children_6_to_12);
                textview_numPeople.setText(R.string.num_people_household);
                textview_numPeople.setTextColor(Color.BLACK);
            }

            // check if we exceeded. will override simple if/else if conflict
            if (childrenNum > otherPeople) {
                Log.d(TAG, "Num conflict in children 12!");
                textview_numPeople.setText(R.string.numPeopleError);
                textview_numPeople.setTextColor(Color.RED);
                // TODO disable next button
                if (!isDisabled) {
                    setDisabled();
                }

            } else if (childrenNum == otherPeople) {
                // valid, but need to disable other options
                // TODO add if disabled, enable code
                if (isDisabled) {
                    setNextEnabled();
                }
            }
            if (!fixingInvalid) {
                // reset next
                children18Spinner.setSelection(0, true);
                fixingInvalid = false;
            }
            // TODO Remove Test Log
            Log.d(TAG, displayChildrenNums());
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private final AdapterView.OnItemSelectedListener children18Listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            // 0 = placeholder, 1 = 0. Therefore, num = pos - 1
            numChildren18 = position;            //update numChildren1
            // reset it to 0 so it doesn't become -1
            if (numChildren18 < 0) {
                numChildren18 = 0;
            }
            int otherPeople = numPeople - 1;        //remove self from total people
            int childrenNum = getNumChildren();     //calculate numChildren (total)

            if (!(childrenNum > otherPeople)) {
                // reset text after error text when error fixed
                textview_children18.setTextColor(Color.BLACK);
                textview_children18.setText(R.string.num_children_13_to_18);
                textview_numPeople.setText(R.string.num_people_household);
                textview_numPeople.setTextColor(Color.BLACK);
            }

            // check if we exceeded
            if (childrenNum > otherPeople) {
                Log.d(TAG, "Num conflict in children 18!");
                textview_numPeople.setText(R.string.numPeopleError);
                textview_numPeople.setTextColor(Color.RED);
                // TODO disable next button
                if (!isDisabled) {
                    setDisabled();
                }
            } else if (childrenNum == otherPeople) {
                // valid
                // TODO add if disabled, enable code
                if (isDisabled) {
                    setNextEnabled();
                }
            } else {
                // TODO add if disabled, enable code
                if (isDisabled) {
                    setNextEnabled();
                }
            }

            // TODO Remove Test Log
            Log.d(TAG, displayChildrenNums());

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private int getNumChildren() {
        return numChildren1 + numChildren5 + numChildren12 + numChildren18;
    }

    //isNumConflict does not work
    // all that happens is, it does not let you advance
    // however, it does not disable the dropdown
    // this means that if there is a num conflict, we can still say
    // we have 8 kids of the first age range and 1 member of our family, etc.
    // A proper solution would be to disable the first dropdown
    // when there is a numconflict as well as the rest
    // (it stops generating dropdowns when there is no more people in the family)
    // (however the first dropdown always remains there, which is an error)
    private boolean isNumConflict() {
        // i know, can just call method above, but maybe this is faster?
        return (numChildren1 + numChildren5 + numChildren12 + numChildren18) > numPeople;
    }

    private void resetSpinners() {
        children1Spinner.setSelection(0);
        children5Spinner.setSelection(0);
        children12Spinner.setSelection(0);
        children18Spinner.setSelection(0);
    }
    private void resetNumChildren() {
        numChildren1=0;
        numChildren5=0;
        numChildren12=0;
        numChildren18=0;
    }

    private String displayChildrenNums() {
        StringBuilder str = new StringBuilder();
        str.append("CHILDREN STORED:\n");
        str.append(numChildren1 + "\n");
        str.append(numChildren5 + "\n");
        str.append(numChildren12 + "\n");
        str.append(numChildren18 + "\n");

        return str.toString();
    }

    // These are used for enabling or disabling
    // the scanner next button
    private void setNextEnabled() {
        Log.d(TAG, "ENABLED NEXT");
        nextBtn.setEnabled(true);
        nextBtnManual.setEnabled(true);
        isDisabled = false;
        nextBtn.setBackgroundColor(getResources().getColor(R.color.colorPrimary, requireActivity().getTheme()));
        nextBtnManual.setBackgroundColor(getResources().getColor(R.color.colorPrimary, requireActivity().getTheme()));
    }

    private void setDisabled() {
        Log.d(TAG, "DISABLED NEXT");
        nextBtn.setEnabled(false);
        nextBtnManual.setEnabled(false);
        isDisabled = true;
        nextBtn.setBackgroundColor(Color.GRAY);
        nextBtnManual.setBackgroundColor(Color.GRAY);
    }

}