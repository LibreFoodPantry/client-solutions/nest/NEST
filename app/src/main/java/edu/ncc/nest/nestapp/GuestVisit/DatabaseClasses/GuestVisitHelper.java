package edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses;

/**
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DecimalFormat;

/**
 * GuestVisitHelper: The helper file of the three database files. This file creates the table
 * itself for the database to have items stored in.
 */
public class GuestVisitHelper extends SQLiteOpenHelper {

    public static final String TAG = GuestVisitHelper.class.getSimpleName();

    // Database name and version
    public static final String DATABASE_NAME = "GuestRegistry.db";
    public static final int DATABASE_VERSION = 1;

    // Table name
    public static final String TABLE_NAME = "NestGuestVisit";

    // Columns in the table
    public static final String ROW_ID = "row_id";
    public static final String GUEST_ID = "guest_id"; //Reference to the id num of the guest
    public static final String ADULT_COUNT = "adult_count"; //Reference to the number of non senior adults in the household
    public static final String SENIOR_COUNT = "senior_count"; //Reference to the number of seniors in the household
    public static final String CHILD_COUNT = "child_count"; //Reference to the number of children in the household
    public static final String FIRST_VISIT = "first_visit"; //Reference to whether or not it is the user's first visit to the NEST
    public static final String DATE = "date"; //Date
    public static final String VISIT_COUNTER = "visit_counter"; //Reference to how many times the user has visited the NEST post-registration

    // Used in the user report methods
    public static final DecimalFormat monthFormat = new DecimalFormat("00");

    public GuestVisitHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    ////////////// Lifecycle Methods Start //////////////

    /**
     * onCreate method
     * Method starts on the creation of the class, creating the SQLiteDatabase.
     * @param db the SQLiteDatabase
     */
    @Override
    //@SuppressLint("DefaultLocale")
    public void onCreate(SQLiteDatabase db) { // Creates the database table\
        try {
            db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                    ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    GUEST_ID + " TEXT, " +
                    ADULT_COUNT + " TEXT, " +
                    SENIOR_COUNT + " TEXT, " +
                    CHILD_COUNT + " TEXT, " +
                    FIRST_VISIT + " TEXT, " +
                    DATE + " TEXT, " +
                    VISIT_COUNTER + " TEXT);");
        }
        catch (Exception exception)
        {
            Log.e(TAG, "Error creating NestGuestVisit database.", exception);
        }
    }


    /**
     * onUpgrade method
     * When a new version of the app is created, drop the old table and create a new one
     * @param db SQLiteDatabase
     * @param oldVersion reference to the old app version
     * @param newVersion reference to the new app version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // If we are upgrading to a new version drop the table
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Re-create the table
        onCreate(db);

    }


    /**
     * addToLocalNGVDataBase method-
     * adds data to the table NestGuestVisit in the local database GuestRegistry
     * @param id
     * @param adult_count
     * @param senior_count
     * @param child_count
     * @param first_day
     * @param date
     * @param visit_count
     * @return
     */
    public boolean addToLocalNGVDataBase(String id,String adult_count, String senior_count, String child_count,String first_day, String date, String visit_count)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(GUEST_ID,id);
        values.put(ADULT_COUNT, adult_count);
        values.put(SENIOR_COUNT, senior_count);
        values.put(CHILD_COUNT, child_count);
        values.put(FIRST_VISIT, first_day);
        values.put(DATE, date);
        values.put(VISIT_COUNTER, visit_count);
        long result = db.insert(TABLE_NAME,null,values);

        if (result == -1)
            return false;
        else
            return true;

    }


    ////////////// Custom Methods Start //////////////

    /**
     * validateColumnCount method
     * Confirms that the number of columns corresponds to the number of questions, if not, create
     * a new table with the correct number of columns.
     */
    private void validateColumnCount() {

        //Create a new database
        SQLiteDatabase db = this.getReadableDatabase();

        if (getColumnCount(db) != (8)) {

            // Print a warning, stating why we're dropping the table
            Log.w(TAG, "Dropping table due to question count change.");

            // Drop the table since the column count needs to change
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

            // Recreate the table
            onCreate(db);

        }

        // Done reading from database so close the reference
        db.close();

    }

    /**
     * getColumnCount method
     * Gets and returns the number of columns in the database.
     * @param db SQLiteDatabase
     * @return columnCount integer
     */
    private int getColumnCount(SQLiteDatabase db) {

        // Create a query to get table info
        Cursor c = db.rawQuery("PRAGMA table_info(" + TABLE_NAME + ")", null);

        //Create a primitive and set it equal to the getColumnCount of c
        int columnCount = c.getCount();

        // Done reading from cursor so make sure we close it
        c.close();

        //Return columnCount integer
        return columnCount;

    }

    /**
     * isDataInLocal - checks if there is rows of data in the local database
     * @return - true or false, true if there is more than 0 rows (which means there is data in the local database)
     * in the local database else return false(there is no data in local)
     */
    public boolean isDataInLocal() {
        SQLiteDatabase db = null;
        Cursor cursor = null;
        int questionSubRows = 0;
        try {
            db = this.getWritableDatabase();
            //run query
            cursor = db.rawQuery("SELECT COUNT(*)  FROM NestGuestVisit", null);

            if (cursor.moveToFirst()) {
                questionSubRows = cursor.getInt(0);
            }
            Log.d("##NGRowCount", String.valueOf(questionSubRows));
        } catch (Exception exception) {
            Log.e(TAG,"An error occurred while checking the number of rows in the local database.", exception);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        if (questionSubRows > 0)
            return true;
        else
            return false;
    }

    /**
     * deleteTableRow -  deletes a rows of data in the local database (The id of a row does not change
     * when a deletion occurs so check app inspector before deleting)
     * @param rowid, the int index of the row you wish to delete (id is one based, example: 1,2,3,4)
     * @return - true or false, true deletion was successful and false if not
     */
    public boolean deleteTableRow(int rowid) {

        SQLiteDatabase db = this.getWritableDatabase();
        if (db == null || !this.isDataInLocal()) {
            Log.d("##NGRowDeletion" , "There are no rows to be Deleted (database is null)");
            return false;
        }

        int rowsDeleted = db.delete("questionnaire_submission","row_id=" + rowid,null);

        if (rowsDeleted <= 0) {
            Log.d("##NGRowDeletion" , "Deletion UNSuccessful");
            return false;
        }

        Log.d("##NGRowDeletion" , "Deletion Successful");
        return true;
    }




    //TODO add getTotalVisitors method for a date range instead of a specific month


    //TODO add getTotalServed method for a date range instead of a specific month

    /**
     * getTotalServedByAge - Counts the number of people served within a given age range and month
     * based on the visitors' family size
     * @param db - The database you wish to query
     * @param month - The month you wish to check
     * @param year - The year you wish to check
     * @param age - Use GuestVisitHelper.AGE_RANGE enum for this value. Specifies the age range you
     *            wish to search within
     * @return - The number of people served in the given month and age range
     */
//    public int getTotalServedByAge(SQLiteDatabase db, int month, int year, AGE_RANGE age) {
//        int servedCount = 0;
//        Cursor c;
//        String ageRange;
//        String queryArgument = monthFormat.format(month) + "__" + year + "%";
//
//        // Sets which column to pull up based on the given AGE_RANGE variable
//        switch(age) {
//            case ADULT:
//                ageRange = ADULT_COUNT;
//                break;
//            case SENIOR:
//                ageRange = SENIOR_COUNT;
//                break;
//            case CHILD:
//                ageRange = CHILD_COUNT;
//                break;
//            default:
//                ageRange = "";
//        }
//
//        // Pulls the necessary data from the database
//        c = db.query(
//                TABLE_NAME,
//                new String[]{ageRange},
//                DATE + " LIKE ? AND ",
//                new String[]{queryArgument},
//                null,
//                null,
//                null
//        );
//
//        // Count the entries
//        while (c.moveToNext())
//            servedCount += c.getInt(0);
//
//        // Close the database link
//        c.close();
//
//        // return the number of entries
//        return servedCount;
//    }

    //TODO add getTotalServedByAge method for a date range instead of a specific month



    //TODO add getTotalPersonVisits method for a date range instead of a specific month

}