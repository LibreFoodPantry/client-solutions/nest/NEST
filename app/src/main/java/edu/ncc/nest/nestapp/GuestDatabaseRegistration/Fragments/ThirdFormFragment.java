package edu.ncc.nest.nestapp.GuestDatabaseRegistration.Fragments;

/**
 *
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import edu.ncc.nest.nestapp.GuestDatabaseRegistration.UIClasses.MultiSelectSpinner;
import edu.ncc.nest.nestapp.R;
import edu.ncc.nest.nestapp.databinding.FragmentGuestDatabaseRegistrationThirdFormBinding;

/**
 * ThirdFormFragment: Represents a form that a guest can fill in with more of their information.
 * The fragment then bundles all of the user's inputs (including info passed from
 * {@link SecondFormFragment} and sends them to the next fragment (will be Fourth Fragment).
 */
public class ThirdFormFragment extends Fragment {

    private FragmentGuestDatabaseRegistrationThirdFormBinding binding;
    // for testing
    private MultiSelectSpinner multiselectDietary, multiselectEmployment, multiselectHealth, multiselectHousing;
    // instance variables for summary fragment
    private String dietary, employment, health, housing, snap, programs, income;
    private Bundle result = new Bundle();

    //for logcat
    public static final String TAG = ThirdFormFragment.class.getSimpleName();


    // initialize backButtonCallback
    private OnBackPressedCallback backbuttonCallback;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentGuestDatabaseRegistrationThirdFormBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences sharedPref = getActivity().getSharedPreferences("BackFrag3", Context.MODE_PRIVATE);
        backbuttonCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                //TODO: BACK BUTTON ONLY SAVES SNAP AND PROGRAMS! Waseh
                SharedPreferences.Editor editor = sharedPref.edit();

                //Clear Shared preferences to make sure
                editor.clear().commit();
                //Place typed info into shared preferences
                editor.putStringSet("diet", (binding.grf3Dietary.getSelectedStringsSet()));
                editor.putStringSet("employ", binding.grf3StatusEmployment.getSelectedStringsSet());
                editor.putStringSet("health", (binding.grf3StatusHealth.getSelectedStringsSet()));
                editor.putStringSet("housing", (binding.grf3StatusHousing.getSelectedStringsSet()));
                editor.putInt("snap", binding.grf3Snap.getSelectedItemPosition());
                editor.putInt("programs", binding.grf3OtherProgs.getSelectedItemPosition());
                editor.putString("income", binding.grf3Income.getText().toString());
                editor.putBoolean("backPressed", true);
                editor.commit();
                //set back button back to false then recall the back button invoking default behvior
                backbuttonCallback.setEnabled(false);
                getActivity().getOnBackPressedDispatcher().onBackPressed();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), backbuttonCallback);

        //This code is for visuals and adds nothing to logic just adds a toast to let the user know what they selected
        //binding.grf3Dietary.setOnItemSelectedListener(dropdownListener);
        //binding.grf3Dietary.setOnItemSelectedListener(dropdownListener);
        //binding.grf3OtherProgs.setOnItemSelectedListener(dropdownListener);
        //binding.grf3Snap.setOnItemSelectedListener(dropdownListener);
        //binding.grf3StatusEmployment.setOnItemSelectedListener(dropdownListener);
        //binding.grf3StatusHealth.setOnItemSelectedListener(dropdownListener);
        //binding.grf3StatusHousing.setOnItemSelectedListener(dropdownListener);

        // target multiselect spinners on the layout
        multiselectDietary = binding.grf3Dietary;
        multiselectEmployment = binding.grf3StatusEmployment;
        multiselectHealth = binding.grf3StatusHealth;
        multiselectHousing = binding.grf3StatusHousing;
        // load them with items using the setItems() method in the MultiSelectSpinner class
        multiselectDietary.setItems(getResources().getStringArray(R.array.dietary_needs));
        multiselectEmployment.setItems(getResources().getStringArray(R.array.employment_status));
        multiselectHealth.setItems(getResources().getStringArray(R.array.health_status));
        multiselectHousing.setItems(getResources().getStringArray(R.array.housing_status));

        //Default String Set for nothing selected yet
        Set<String> nothingSel = new HashSet<String>();
        nothingSel.add("0");

        //sets prefer not to say as the default values for all spinners
        multiselectDietary.setSelection((getResources().getStringArray(R.array.dietary_needs)).length - 1);
        multiselectEmployment.setSelection((getResources().getStringArray(R.array.employment_status)).length - 1);
        multiselectHealth.setSelection((getResources().getStringArray(R.array.health_status)).length - 1);
        multiselectHousing.setSelection((getResources().getStringArray(R.array.housing_status)).length - 1);
        binding.grf3Snap.setSelection((getResources().getStringArray(R.array.SNAP)).length - 1);
        binding.grf3OtherProgs.setSelection((getResources().getStringArray(R.array.other_programs)).length - 1);
        
        //check if the back button was pressed
        if(sharedPref.getBoolean("backPressed", false)) {
        //Return possible saved bindings after back press
        //checks if the spinner is empty. if it, it resets the spinner so the instructions are displayed.
           if (sharedPref.getStringSet("diet", nothingSel).isEmpty()) {
               binding.grf3Dietary.clearSelections();
           } else {
               binding.grf3Dietary.setSelection(sharedPref.getStringSet("diet", nothingSel));
           }

            //checks if the spinner is empty. if it, it resets the spinner so the instructions are displayed.
           if (sharedPref.getStringSet("employ", nothingSel).isEmpty()){
                binding.grf3StatusEmployment.clearSelections();
           } else {
                binding.grf3StatusEmployment.setSelection(sharedPref.getStringSet("employ", nothingSel));
           }

           //checks if the spinner is empty. if it, it resets the spinner so the instructions are displayed.
           if (sharedPref.getStringSet("health", nothingSel).isEmpty()){
               binding.grf3StatusHealth.clearSelections();
           } else {
                binding.grf3StatusHealth.setSelection(sharedPref.getStringSet("health", nothingSel));
           }

           //checks if the spinner is empty. if it, it resets the spinner so the instructions are displayed.
           if (sharedPref.getStringSet("housing", nothingSel).isEmpty()){
                binding.grf3StatusHousing.clearSelections();
           } else {
                binding.grf3StatusHousing.setSelection(sharedPref.getStringSet("housing", nothingSel));
           }

            binding.grf3Snap.setSelection(sharedPref.getInt("snap", 0));
            binding.grf3OtherProgs.setSelection(sharedPref.getInt("programs", 0));
            binding.grf3Income.setText(sharedPref.getString("income", ""));
        }

        // adds the onClick listener to the 'next' button
        binding.nextButtonThirdFragmentGRegistration.setOnClickListener(v -> {
            //Will check if all fields are selected other than the default values
            boolean allFieldsSelected = true;

            // Checks if "Do you have dietary..." drop-down menu is selected other than the default value
            if (binding.grf3Dietary.getSelectedItemsAsString().isEmpty()) {
                // if its the default value(nothing) then set the text view to red
                binding.grf3TextviewDietary.setTextColor(Color.RED);
                // and appends a "*" to indicate necessity
                binding.grf3TextviewDietary.append(" *");
                //sets the text to "select all that apply"
                binding.grf3Dietary.clearSelections();
                //sets the boolean to false due to the field not being filled properly
                allFieldsSelected = false;
            } else {

                //if it was correctly selected then set the color to/back to black
                binding.grf3TextviewDietary.setTextColor(Color.BLACK);
                //changes it back to original text so the "*" is not compounded in black
                binding.grf3TextviewDietary.setText(R.string.dietary_needs);
                Log.d(TAG, "Dietary: " + binding.grf3Dietary.getSelectedItemsAsString());
            }

            // Checks if "Do you receive SNAP benefits?" drop-down menu is selected other than the default value
            if (binding.grf3Snap.getSelectedItemPosition() == 0) {
                // set the text view to red
                binding.grf3TextviewSnap.setTextColor(Color.RED);
                // append a "*" to indicate necessity
                binding.grf3TextviewSnap.append(" *");
                // set the boolean to false due to the field not being filled properly
                allFieldsSelected = false;
            } else {
                // if it was correctly selected then set the color back to black
                binding.grf3TextviewSnap.setTextColor(Color.BLACK);
                //changes it back to original text so the "*" is not compounded in black
                binding.grf3TextviewSnap.setText(getString(R.string.snap));
                Log.d(TAG, "SNAP: " + binding.grf3Snap.getSelectedItem());
            }

            // Checks if "Other Programs" drop-down menu is selected other than the default value
            if (binding.grf3OtherProgs.getSelectedItemPosition() == 0) {
                // set the text view to red
                binding.grf3TextviewOtherProgs.setTextColor(Color.RED);
                // append a "*" to indicate necessity
                binding.grf3TextviewOtherProgs.append(" *");
                // set the boolean to false due to the field not being filled properly
                allFieldsSelected = false;
            } else {
                // if it was correctly selected then set the color back to black
                binding.grf3TextviewOtherProgs.setTextColor(Color.BLACK);
                //changes it back to original text so the "*" is not compounded in black
                binding.grf3TextviewOtherProgs.setText(getString(R.string.other_programs));
                Log.d(TAG, "Other Programs: " + binding.grf3OtherProgs.getSelectedItem());
            }

            // Checks if "Employment Status" drop-down menu is selected other than the default value
            if (binding.grf3StatusEmployment.getSelectedItemsAsString().isEmpty()) {
                // set the text view to red
                binding.grf3TextviewStatusEmployment.setTextColor(Color.RED);
                // append a "*" to indicate necessity
                binding.grf3TextviewStatusEmployment.append(" *");
                // set the boolean to false due to the field not being filled properly
                allFieldsSelected = false;
            } else {
                // if it was correctly selected then set the color back to black
                binding.grf3TextviewStatusEmployment.setTextColor(Color.BLACK);
                //changes it back to original text so the "*" is not compounded in black
                binding.grf3TextviewStatusEmployment.setText(getString(R.string.employment_status));
                Log.d(TAG, "Employment Status: " + binding.grf3StatusEmployment.getSelectedItemsAsString());
            }

            // Checks if "Health Status" drop-down menu is selected other than the default value
            if (binding.grf3StatusHealth.getSelectedItemsAsString().isEmpty()) {
                // set the text view to red
                binding.grf3TextviewStatusHealth.setTextColor(Color.RED);
                // append a "*" to indicate necessity
                binding.grf3TextviewStatusHealth.append(" *");
                // set the boolean to false due to the field not being filled properly
                allFieldsSelected = false;
            } else {
                // if it was correctly selected then set the color back to black
                binding.grf3TextviewStatusHealth.setTextColor(Color.BLACK);
                //changes it back to original text so the "*" is not compounded in black
                binding.grf3TextviewStatusHealth.setText(getString(R.string.health_status));
                Log.d(TAG, "Health Status: " + binding.grf3StatusHealth.getSelectedItemsAsString());
            }

            // Checks if "Housing Status" drop-down menu is selected other than the default value
            if (binding.grf3StatusHousing.getSelectedItemsAsString().isEmpty()) {
                // set the text view to red
                binding.grf3TextviewStatusHousing.setTextColor(Color.RED);
                // append a "*" to indicate necessity
                binding.grf3TextviewStatusHousing.append(" *");
                // set the boolean to false due to the field not being filled properly
                allFieldsSelected = false;
            } else {
                // if it was correctly selected then set the color back to black
                binding.grf3TextviewStatusHousing.setTextColor(Color.BLACK);
                //changes it back to original text so the "*" is not compounded in black
                binding.grf3TextviewStatusHousing.setText(getString(R.string.housing_status));
                Log.d(TAG, "Housing Status: " + binding.grf3StatusHousing.getSelectedItemsAsString());
            }

            //Checks if house hold income was filled out
            //unfortunatly wasnt able to figure out the visuals but logic works
            if(binding.grf3Income.length()==0){

                //testing visuals, nothing worked
                //binding.grf3IncomeWrapper.setHintAnimationEnabled(true);
                //binding.grf3Income.setTextColor(Color.RED);
                //binding.grf3Income.append(" *");

                //logic to make sure field is filled out
                /*
                 * if in the future "HOUSEHOLD INCOME"
                 * uncomment allFieldsSelected
                 */
                //allFieldsSelected = false;
            } else{
                //binding.grf3Income.setTextColor(Color.BLACK);
                //binding.grf3Income.setText(getString(R.string.household_income));
                Log.d(TAG, "Income: " + binding.grf3Income.getText());
            }

            // If all fields are selected, proceed
            if (allFieldsSelected) {

                // Store the selected items into the instance variables
                //if language is set to Spanish, get English strings
                Configuration config = getResources().getConfiguration();
                if(config.getLocales().get(0).toString().equals("es")) {
                    // Save selected items into the instance variable for the NESTGuestRegistry data set
                    //config.setLocale(new Locale("en"));
                    Context context1 = getContext().createConfigurationContext(config);

                    dietary = context1.getResources().getStringArray(R.array.dietary_needs)[binding.grf3Dietary.getSelectedItemPosition()];
                    programs = context1.getResources().getStringArray(R.array.yes_no_select_one)[binding.grf3OtherProgs.getSelectedItemPosition()];
                    snap = context1.getResources().getStringArray(R.array.yes_no_select_one)[binding.grf3Snap.getSelectedItemPosition()];
                    employment = context1.getResources().getStringArray(R.array.employment_status)[binding.grf3StatusEmployment.getSelectedItemPosition()];
                    health = context1.getResources().getStringArray(R.array.health_status)[binding.grf3StatusHealth.getSelectedItemPosition()];
                    housing = context1.getResources().getStringArray(R.array.housing_status)[binding.grf3StatusHousing.getSelectedItemPosition()];
                    //set language back to Spanish to prevent any potential weirdness
                    //config.setLocale(new Locale("es"));
                } else {
                    dietary = multiselectDietary.getSelectedItemsAsString();
                    programs = binding.grf3OtherProgs.getSelectedItem().toString();
                    snap = binding.grf3Snap.getSelectedItem().toString();
                    employment = multiselectEmployment.getSelectedItemsAsString();
                    health = multiselectHealth.getSelectedItemsAsString();
                    housing = multiselectHousing.getSelectedItemsAsString();
                }

                income = binding.grf3Income.getText().toString();

                // Store all values in a bundle to send to the summary fragment
                result.putString("dietary", dietary);
                result.putString("programs", programs);
                result.putString("snap", snap);
                result.putString("employment", employment);
                result.putString("health", health);
                result.putString("housing", housing);
                result.putString("income", income);

                // Navigate to the fourth fragment
                getParentFragmentManager().setFragmentResult("sending_third_form_fragment_info", result);
                NavHostFragment.findNavController(ThirdFormFragment.this)
                        .navigate(R.id.action_DBR_ThirdFormFragment_to_FourthFormFragment);
            } else {
                // Inform the user to complete all selections
                Toast.makeText(getContext(), "Please make selections in all fields.", Toast.LENGTH_SHORT).show();
            }
        });


    }

    // This dropdown listener currently changes the first item in the spinner to muted text.
    // When a user selects an item other than the first, text changes to standard color.
    // Later, we can use this same logic for verification.
    // TODO can this be moved to a separate file and then just called?
    private final AdapterView.OnItemSelectedListener dropdownListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            //an item is selected. Below uses the "ColorPrimaryDark" variable. This will allow us to
            // keep universal themes and styling across the app.
            // HAVE TO ADD TRY STATMENT DUE TO APP CRASHING WHEN PRESSING BACK BUTTOM
            try {
                int viewId = parent.getId();
                String message = "";

                switch (viewId) {
                    case R.id.grf_3_snap:
                        message = "Selected SNAP: " + binding.grf3Snap.getSelectedItem().toString();
                        break;
                    case R.id.grf_3_otherProgs:
                        message = "Selected Other Programs: " + parent.getSelectedItem().toString();
                        break;
                    case R.id.grf_3_statusEmployment:
                        message = "Selected Employment Status: " + binding.grf3StatusEmployment.getSelectedItemsAsString();
                        break;
                    case R.id.grf_3_statusHealth:
                        message = "Selected Health Status: " + ((MultiSelectSpinner) parent).getSelectedItemsAsString();
                        break;
                    case R.id.grf_3_statusHousing:
                        message = "Selected Housing Status: " + ((MultiSelectSpinner) parent).getSelectedItemsAsString();
                        break;
                    case R.id.grf_3_dietary:
                        message = "Selected Dietary Needs: " + ((MultiSelectSpinner) parent).getSelectedItemsAsString();
                        break;
                }

                if (!message.isEmpty()) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                }
            } catch (NullPointerException e) {
                System.err.println("ERROR: " + e.getMessage());
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }

    };
}