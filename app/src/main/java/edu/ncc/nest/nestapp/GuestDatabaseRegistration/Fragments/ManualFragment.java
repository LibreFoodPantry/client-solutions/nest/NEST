package edu.ncc.nest.nestapp.GuestDatabaseRegistration.Fragments;

/**
 *
 * Copyright (C) 2019 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import edu.ncc.nest.nestapp.databinding.FragmentGuestDatabaseRegistrationManualEntryBinding;

import edu.ncc.nest.nestapp.R;

public class ManualFragment extends Fragment {

    private FragmentGuestDatabaseRegistrationManualEntryBinding binding;
    private boolean validBarcode;

    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        binding = FragmentGuestDatabaseRegistrationManualEntryBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        binding.buttonEnterNext.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String barcode = binding.edittextEnterUpc.getText().toString();
                validBarcode = checkBarcode(barcode);
                if(validBarcode) {
                    Bundle result = new Bundle();
                    result.putString("registrationBarcode", barcode);
                    getParentFragmentManager().setFragmentResult("sending_barcode_info", result);

                    NavHostFragment.findNavController(edu.ncc.nest.nestapp.GuestDatabaseRegistration.Fragments.ManualFragment.this)
                            .navigate(R.id.action_DBR_ManualFragment_to_DBR_SummaryFragment);
                }
                else{
                    String text = "The given UPC code is invalid";
                    Toast.makeText(view.getContext(), text, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
    This method checks to see if the provided barcode is valid.

     Parameters: string which will contain the given barcode
     Returns: True if the given barcode is valid.  False if otherwise
     */
    private boolean checkBarcode(String string){
        //Checks if the length of the string is 8 and if the string contains a '-'
        if(string.length() == 8 && string.contains("-")){
            int dashIndex = string.indexOf('-');
            String letterPart = string.substring(0, dashIndex);
            String numberPart = string.substring(dashIndex + 1);
            // This is a regex expression that checks to see if letterPart has any characters that is not from a-z or A-Z
            // and the + is to tell matches that a-z or A-Z will appear more than once
            if(letterPart.matches("[a-zA-Z]+") && numberPart.matches("[0-9]+")){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
}
