package edu.ncc.nest.nestapp;
/**
 *
 * Copyright (C) 2019 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

//******************************testing**************************************************************************
import com.mysql.fabric.xmlrpc.base.Data;

import edu.ncc.nest.nestapp.CheckExpirationDate.Activities.CheckExpirationDateActivity;
import edu.ncc.nest.nestapp.CheckExpirationDate.Fragments.MoreInfoFragment;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.Activities.GuestDatabaseRegistrationActivity;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.DataSyncManager;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper;
import edu.ncc.nest.nestapp.GuestVisit.Activities.GuestVisitActivity;

public class Choose extends AppCompatActivity implements OnClickListener {
    private static final String TAG = Choose.class.getSimpleName();

    private boolean isDataInLocal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        isDataInLocal = databaseStatus();



        /* Makes sure that the last sync date is whatever was last stored in shared preferences. */
        SharedPreferences prefs = this.getSharedPreferences(getString(R.string.syncDate),MODE_PRIVATE);
        String lastSync = prefs.getString(getString(R.string.syncDate),"Not synced yet.");

        OffsetDateTime odt;

        try {
            odt = OffsetDateTime.parse(lastSync);
            DateTimeFormatter odtFormatter = DateTimeFormatter.ofPattern("MM-dd-yyyy hh:mm:ss a");
            String lastSyncFormatted = odt.format(odtFormatter);

            TextView text = findViewById(R.id.lastSyncLabel);
            text.setText(getString(R.string.last_synced) + " " + lastSyncFormatted);
        } catch (DateTimeParseException ex) {
            TextView text = findViewById(R.id.lastSyncLabel);
            text.setText(getString(R.string.last_synced) + " " + lastSync);
        }
    }

    private boolean databaseStatus(){
        return (new DataSyncManager(this).isDataInLocal() || new GuestRegistryHelper(this).isDataInLocal());
    }

    //implements the menu options for the toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        //instead of inflating menu_main, now this class inflates choose_menu_main.xml, this way
        // allowing the future efforts button to only appear at the app bar of the launch UI
        inflater.inflate(R.menu.choose_menu_main, menu);

        if(!isDataInLocal){
            menu.findItem(R.id.sync_button).setVisible(false);
        }

        return true;

        //since the toolbar has 2 different button that goes to different places
        //ask ho to make the button just show up in the launch UI

    }


    /**
     *
     * Title : onClick Method -- Whenever a certain button is clicked it would
     * call the method and inside that method would launch an activity and display to the user
     * and then would break afterwords..
     *
     * @param v - The activity that was clicked by the user.
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.getUPCBtn:
                launchGetUPC();
                break;
            case R.id.guestRegBtn:
                launchGuestReg();
                break;
            case R.id.guestVisitBtn:
                launchGuestVisit();
                break;
        }
    }

    /**
     * launchGuestVisit - starts the GuestVisitActivity activity
     */
    public void launchGuestVisit() {
        // Call sync method from DataSyncManager, syncToRemoteDb.
        // Retrieves the updated last sync'd time from the DataSyncManager,
        // formats it for easier readability,
        // and updates the UI to reflect the new time.
        Intent intent = new Intent(this, GuestVisitActivity.class);

        if(isDataInLocal){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.syncquestion);
            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Log.d(TAG,"Sync before guest visit.");
                    sync();
                    startActivity(intent);
                }
            });
            builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    startActivity(intent);
                }
            });

            builder.create().show();
        } else {
            startActivity(intent);
        }



    }

    /**
     * launchGuestDatabaseRegistrationActivity - starts the GuestDatabaseRegistrationActivity activity
     */
    public void launchGuestReg() {
        Intent intent = new Intent(this, GuestDatabaseRegistrationActivity.class);
        startActivity(intent);
    }

    /**
     * launchGetUPC - starts the Get UPC activity
     */
    public void launchGetUPC() {
        Intent intent = new Intent(this, CheckExpirationDateActivity.class);
        startActivity(intent);
    }

    /**
     * onOptionsItemSelected method --
     *
     * description: this method handle the action bar items that were clicked.
     * When you click the future efforts button it launches the FutureEfforts class.
     * @param item
     * @return
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.futureEffortBtn) {
            launchFutureEfforts();
            return true;
        } else if (id == R.id.sync_button) {
            // Call sync method from DataSyncManager, syncToRemoteDb.
            // Retrieves the updated last sync'd time from the DataSyncManager,
            // formats it for easier readability,
            // and updates the UI to reflect the new time.
            Log.d(TAG,"Sync button pressed.");
            sync();
            return true;
        }
        /*
        if(item.getItemId() == R.id.login_btn){
            launchLoginActivity();
            return true;
        }

         */

        return super.onOptionsItemSelected(item);
    }

    public void sync(){
        DataSyncManager syncManager = new DataSyncManager(this);
        if(syncManager.syncToRemoteDb()) {
            SharedPreferences prefs = this.getSharedPreferences(getString(R.string.syncDate),MODE_PRIVATE);
            String lastSync = prefs.getString(getString(R.string.syncDate),"Not Synced yet.");

            OffsetDateTime odt = OffsetDateTime.parse(lastSync);
            DateTimeFormatter odtFormatter = DateTimeFormatter.ofPattern("MM-dd-yyyy hh:mm:ss a");
            String lastSyncFormatted = odt.format(odtFormatter);

            TextView text = findViewById(R.id.lastSyncLabel);
            text.setText(getString(R.string.last_synced) + " " + lastSyncFormatted);
            Log.d(TAG,"Sync process complete.");
            isDataInLocal = false;
        }
        //guest visit database sync code goes here


        //upload for guest visit database should also set isDataInLocal to false
        if(!isDataInLocal)
            findViewById(R.id.sync_button).setVisibility(View.GONE);
    }

    /**
     * launchFutureEfforts - starts the Future Efforts activity
     */
    public void launchFutureEfforts() {
        Intent intent = new Intent(this, FutureEfforts.class);
        startActivity(intent);
    }

    /**
     * launchLoginActivity - starts the Login Activity
     */
    public void launchLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


    //    ******************************************TESTING
    /**
     * launchTrueDate - starts the MoreInfoFragment fragment
     */
    public void launchTrueDate() {
        ((Button)findViewById(R.id.getUPCBtn)).setVisibility(View.GONE);
        ((Button)findViewById(R.id.guestRegBtn)).setVisibility(View.GONE);
        ((Button)findViewById(R.id.guestVisitBtn)).setVisibility(View.GONE);
        //((Button)findViewById(R.id.futureEffortsBtn)).setVisibility(View.GONE);
        //((Button)findViewById(R.id.trueDate)).setVisibility(View.GONE);
        ((TextView)findViewById(R.id.nestTxt)).setVisibility(View.GONE);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.RelativeLayoutMain, new MoreInfoFragment() ).commit();

    }
}






