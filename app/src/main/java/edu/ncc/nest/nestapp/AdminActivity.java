package edu.ncc.nest.nestapp;
/**
 *
 * Copyright (C) 2019 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

//******************************testing**************************************************************************
import edu.ncc.nest.nestapp.CheckExpirationDate.Activities.CheckExpirationDateActivity;
import edu.ncc.nest.nestapp.CheckExpirationDate.Fragments.MoreInfoFragment;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.Activities.GuestDatabaseRegistrationActivity;
import edu.ncc.nest.nestapp.GuestGoogleSheetRegistration.Activities.GuestGoogleSheetRegistrationActivity;
import edu.ncc.nest.nestapp.GuestVisit.Activities.GuestVisitActivity;

public class AdminActivity extends AppCompatActivity implements OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    //implements the menu options for the toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();


        inflater.inflate(R.menu.menu_main, menu);

        return true;

    }

    public void onClick(View v) {

        if (v.getId()==R.id.volGetUPC){
            launchGetUPC();
        }

        if(v.getId()==R.id.guestRegGoogle){
            launchGuestRegGoogle();
        }

        if(v.getId()==R.id.guestVisitGoogle){
            launchGuestCheckIn();
        }
    }

    public void launchGetUPC() {
        Intent intent = new Intent(this, CheckExpirationDateActivity.class);
        startActivity(intent);
    }


    public void launchGuestRegGoogle() {
        Intent intent = new Intent(this, GuestGoogleSheetRegistrationActivity.class);
        startActivity(intent);
    }

    public void launchGuestCheckIn(){
        Intent intent = new Intent(this, GuestVisitActivity.class);
        startActivity(intent);
    }


}