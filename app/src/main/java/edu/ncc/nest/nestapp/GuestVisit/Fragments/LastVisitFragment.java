package edu.ncc.nest.nestapp.GuestVisit.Fragments;

/* Copyright (C) 2020 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import static edu.ncc.nest.nestapp.GuestVisit.Fragments.GuestSubmissionFragment.TAG;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

import edu.ncc.nest.nestapp.Choose;

import edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses.GuestVisitSource;

import edu.ncc.nest.nestapp.R;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistrySource;
import edu.ncc.nest.nestapp.databinding.FragmentGuestVisitLastVisitBinding;


public class LastVisitFragment extends Fragment {
    private FragmentGuestVisitLastVisitBinding binding;
    private GuestRegistrySource registrySource;
    private GuestVisitSource guestVisitSource;
    private OffsetDateTime lastVisitDate;
    private OffsetDateTime todaysDate;
    private int numDays;
    private String guestBarcode;
    private String guestPhone;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentGuestVisitLastVisitBinding.inflate(inflater, container, false);
        registrySource = new GuestRegistrySource(getContext());
        guestVisitSource = new GuestVisitSource(getContext());

        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState ) {
        super.onViewCreated( view, savedInstanceState );
        numDays = -1;
        todaysDate = OffsetDateTime.now();
        /* temp can be used for testing purposes - check if the guest's last visit updated */
        // LocalDate temp = LocalDate.of(2024, 1, 12);

        /* retrieve the guest info from the nav arguments and then get their last visit date */
        String guestInfo[] = getArguments().getStringArray("GUEST_INFO");
        guestBarcode = guestInfo[0];
        Log.d(TAG, "guest barcode: " + guestBarcode);
        guestPhone = guestInfo[1];
        Log.d(TAG, "guest phone number: " + guestPhone);
        String guestLastVisit = registrySource.getLastVisit(guestBarcode, guestPhone);
        Log.d(TAG, "guest last visit date: " + guestLastVisit);

        /* parse the formatted date from the retrieved field */
        String dateFormat[] = guestLastVisit.split("-");
        int year = Integer.parseInt(dateFormat[0]);
        int month = Integer.parseInt(dateFormat[1]);
        String daytime = getDayFromDayTime(dateFormat[2]);
        int day = Integer.parseInt(daytime);

        /* create a new LocalDate object that is the guest's original last visit date */
        LocalDate originalLastDate = LocalDate.of(year, month, day);

        /* calculate the number of days it has been since the guest last visited */
        if ( originalLastDate != null ) {
            numDays = getNumDaysLastVisit(originalLastDate);
        } else {
            Log.d(TAG, "originalLastDate is null");
        }

        // if the number of days was never changed after calling the method, the guest status is unknown
        if ( numDays == -1 ) {
            binding.confirmationIcon.setImageResource(R.drawable.ic_warning);
            binding.statusMsg.setText("The guest's last visit date could not be determined.");
            binding.daysText.setText("?");
            binding.lastVisitDateText.setText("?");

        // the guest can be confirmed for check in depending on the amount of days it has been since they last visited
        } else {
            binding.daysText.setText(String.valueOf(numDays));
            binding.lastVisitDateText.setText(guestLastVisit);

            /* reusing the confirmation UI images */
            if ( numDays < 7 ) {
                binding.confirmationIcon.setImageResource(R.drawable.guest_not_found_icon);
                binding.statusMsg.setText("The guest has already visited the NEST within the past 7 days.");
            } else {
                binding.confirmationIcon.setImageResource(R.drawable.guest_found_icon);
                binding.statusMsg.setText("The guest last visited the NEST over 7 days ago.");
            }
        }
        binding.confirmationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* the record in the GR database will only be updated if the guest is allowed to visit */
                if ( numDays > 7 ) {
                    registrySource.setLastVisit(guestBarcode, guestPhone, todaysDate); // todaysDate can be replaced with temp
                    // test to see if the date updates as accordingly
                    String updatedDate = registrySource.getLastVisit(guestBarcode, guestPhone);
                    Log.d(TAG, "the updated last visit: " + updatedDate);
                }
                getActivity().finish(); // finish check in

                // create an Intent that will bring the user back to the choose activity
                Intent intent = new Intent(requireContext(), Choose.class);

                startActivity(intent);
            }
        });

    }


    private String getDayFromDayTime(String dayHourSec){
        String onlyDay = dayHourSec.substring(0,2);
//        if (onlyDay.indexOf(1))
        if (Character.isLetter(onlyDay.indexOf(1)))
            onlyDay= onlyDay.substring(0,1);

        return onlyDay;
    }
    /**
     * getNumDaysLastVisit -- Calculates the number of days it has been since the guest last visited
     * @param lastDate - the last date the guest visited the NEST
     */
    private int getNumDaysLastVisit(LocalDate lastDate) {
        long numDays =  -1; // initialize to -1 for a default value

        /* test to see the order of operations */
        // temp can be used for testing purposes
        // LocalDate temp = LocalDate.of(2024, 1, 12);
        /* numDays = originalDate.until(temp, ChronoUnit.DAYS);
        numDays = temp.until(originalDate, ChronoUnit.DAYS); */

        // replace todaysDate with temp
        numDays = lastDate.until(todaysDate, ChronoUnit.DAYS);

        return (int) numDays;
    }
}
