package edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses;

/**
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 *
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.ADDRESS;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.AFFILIATION;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.AGE;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.BARCODE;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.CHILDREN_1;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.CHILDREN_12;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.CHILDREN_18;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.CHILDREN_5;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.CITY;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.DATE;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.DIET;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.EMPLOYMENT;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.GENDER;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.HEALTH;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.HOUSEHOLD_NUM;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.HOUSING;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.INCOME;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.NAME;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.NCC_ID;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.PHONE;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.PROGRAMS;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.SNAP;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.STATE;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.TABLE_NAME;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.ZIP;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.SimpleDateFormat;
import android.util.Log;

import androidx.annotation.NonNull;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
//import java.util.concurrent.atomic.AtomicBoolean;
//import java.util.concurrent.atomic.AtomicReference;

import edu.ncc.nest.nestapp.BuildConfig;
import edu.ncc.nest.nestapp.InventoryInfoHelper;

import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

//import edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses.GuestVisitHelper;

/**
 * GuestRegistrySource: Handles the insertion and removal of data into the GuestRegistry database
 * (See {@link GuestRegistryHelper}). Also has methods that allow for searching the database.
 */
public class GuestRegistrySource {

    private static final String USERNAME = BuildConfig.GUEST_REGISTRY_DB_USERNAME; // Username for the database
    private static final String PASSWORD = BuildConfig.GUEST_REGISTRY_DB_PASSWORD; // Password for the database
    private static final String DRIVER = "jdbc:mysql://";                          // Driver that is used to connect to the database
    private static final String DB_NAME = "GuestRegistry";                         // Name of the remote DB
    private static final String SERVER = "stargate.ncc.edu:3306/";                      // Name of the server that the DB is hosted on
    private static final String TAG = "GuestRegistrySource";                      // Tag for logging
    private final GuestRegistryHelper registryHelper;
    private final SQLiteDatabase database;
    private final String URI = DRIVER + SERVER + DB_NAME;                          // URI to communicate with the database

    /**
     * Default Constructor --
     *
     * @param context
     */
    public GuestRegistrySource(Context context) {

        registryHelper = new GuestRegistryHelper(context);

        // Open a database that will be used for reading and writing
        database = registryHelper.getWritableDatabase();

    }

    /**
     * close --
     * Closes the database.
     */
    public void close() {

        database.close();

        registryHelper.close();

    }

    /**
     * insertData method --
     * Takes the data passed as arguments and inserts it into the database appropriately.
     * If the data is inserted without issue, the value for the row ID will be returned,
     * and in turn the method will return a boolean value of true. If an issue does occur,
     * -1 will be returned and the method will return a boolean value of false.
     *
     * @param name            - the name of the guest
     * @param phone           - the phone number of the guest
     * @param nccID           - the NCC ID of the guest
     * @param address         - the address of the guest
     * @param city            - the city of the guests' address
     * @param zipcode         - the zip-code of the guests' address
     * @param state           - state that guest's address is belong to
     * @param affiliation     - affiliation of the guest
     * @param age             - age of the guest
     * @param gender          - gender of the guest
     * @param diet            - diet that guest follows
     * @param programs        - guest's programs
     * @param snap            - does the guest have SNAP / Food stamps
     * @param employment      - employment status of the guest
     * @param health          - what kind of insurance does the guest have
     * @param housing         - housing of the guest
     * @param income          - the income of the guest
     * @param householdNum    - the number of people in the guest's household
     * @param children1       - number of guest's children under the age of 1 year old
     * @param children5       - number of guest's children between the age of 13 months and 5 years old
     * @param children12      - number of guest's children between the age of 6 and 12 years old
     * @param children18      - number of the guest's children between the age of 13 and 18 years old
     * @param barcode         - the barcode that belongs to the guest
     * @param lastVisit       - the last date the guest visited the nest
     * @return true if the data has been inserted without issue, false otherwise
     */
    public boolean insertData(String name, String phone, String nccID, String address, String city, String zipcode,
                              String state, String affiliation, String age, String gender, String diet, String programs,
                              String snap, String employment, String health, String housing, String income, String householdNum,
                              String children1, String children5, String children12, String children18,
                              String barcode, String lastVisit) {

        // Creates an ExecutorService that uses a single worker thread operating off an unbounded queue.
        ExecutorService executor = Executors.newSingleThreadExecutor();

        AtomicBoolean didInsert = new AtomicBoolean(false);

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
        String date = sdf.format(new Date());

        // Executes the thread that will insert the data into the remote db
        executor.execute(() -> {

            //Try to establish a connection to the db
            try (Connection connection = DriverManager.getConnection(URI, USERNAME, PASSWORD)) {

                //Compiles the sql statement
                PreparedStatement statement = connection.prepareStatement("INSERT INTO " + TABLE_NAME + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

                // sets the values for the statement
                statement.setString(1, null);
                statement.setString(2, name);
                statement.setString(3, phone);
                statement.setString(4, nccID);
                statement.setString(5, date);
                statement.setString(6, address);
                statement.setString(7, city);
                statement.setString(8, zipcode);
                statement.setString(9, state);
                statement.setString(10, affiliation);
                statement.setString(11, age);
                statement.setString(12, gender);
                statement.setString(13, diet);
                statement.setString(14, programs);
                statement.setString(15, snap);
                statement.setString(16, employment);
                statement.setString(17, health);
                statement.setString(18, housing);
                statement.setString(19, income);
                statement.setString(20, householdNum);
                statement.setString(21, children1);
                statement.setString(22, children5);
                statement.setString(23, children12);
                statement.setString(24, children18);
                statement.setString(25, barcode);
                statement.setString(26, lastVisit);
                // Execute the statement
                statement.executeUpdate();
                didInsert.set(true);
            } catch (SQLException e) {
                Log.e(TAG, "Error inserting data into database", e);
            }
        });

        // Shuts down the executor service so that it no longer accepts new tasks
        executor.shutdown();

        // Waits for executor to finish before returning from this method.
        // Otherwise, this method will always return false
        try {
            executor.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        if(didInsert.get())
            Log.d(TAG, "Inserted into remote");

        // Insert method will return a negative 1 if there was an error with the insert
        return didInsert.get();

    }

    public int removeData() {
        return database.delete(TABLE_NAME, null, null);
    }

    /**
     * isRegistered - Takes 1 parameter
     * Determines whether or not a guest is currently registered with the provided barcode
     * This method appears to cause hanging on newer versions of Pixel.
     * TODO: Have this method instead of returning two objects, return one. The name will no longer be
     *  in use.
     *
     * @param barcode - The barcode to search the database for
     * @return Returns the phone number. As per administration, there is no reason for this method
     * to return a name, as a result, this method now returns a single string.
     */
    public String isRegistered(@NonNull String barcode) throws ExecutionException, InterruptedException {
        if (isConnected()) {
            Log.d(TAG, "IS CONNECTED!!!!!");
            final String[] info = new String[1];
            
            Log.d(TAG, "Current connection to the database: " + isConnected());
            ExecutorService executor = Executors.newSingleThreadExecutor();
            AtomicReference<String> phone = new AtomicReference<>("");

            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                try (Connection connection = DriverManager.getConnection(URI, USERNAME, PASSWORD)) {
                    PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + TABLE_NAME + " WHERE " + BARCODE + " = ?");
                    statement.setString(1, barcode);
                    ResultSet resultSet = statement.executeQuery();
                    if (resultSet.next()) {
                        phone.set(resultSet.getString(PHONE));
                        info[0] = phone.get();
                    }
                } catch (SQLException e) {
                    Log.e(TAG, "Error selecting data from the database", e);
                }
            }, executor);

            // Wait for the thread to complete before returning the result
            future.get();

            return info[0];
        }
        return null;
    }

    // Administration has requested that the name be removed.

    /**
     * isRegisteredPhone checks the database and returns a String array depending on what was found with the
     * phone number that was entered.
     *
     * This method is in progress and has not been tested.
     *
     * @param phoneNumber phone number to be searched.
     * @return String array that returns the phone number and barcode
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public String[] isRegisteredPhone(@NonNull String phoneNumber) throws ExecutionException, InterruptedException
    {
        if(isConnected()) {

            Log.d(TAG, "IS CONNECTED!!!!!");
            String[] info = new String[2];

            ExecutorService executor = Executors.newSingleThreadExecutor();
            AtomicReference<String> barcode = new AtomicReference<>("");

            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                try (Connection connection = DriverManager.getConnection(URI, USERNAME, PASSWORD)) {
                    PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + TABLE_NAME + " WHERE " + PHONE + " = ?");
                    statement.setString(1, phoneNumber);
                    ResultSet resultSet = statement.executeQuery();
                    if (resultSet.next()) {
                        barcode.set(resultSet.getString(BARCODE));
                        info[0] = phoneNumber;
                        info[1] = barcode.get(); // Use get() method to retrieve the value of the AtomicReference
                    }
                } catch (SQLException e) {
                    Log.e(TAG, "Error selecting data into database", e);
                }
            }, executor);

            // Wait for the thread to complete before returning the result
            future.get();

            return info;
        }
        return null;
    }



    /**
     * doesExist method --
     * Determines if the user is trying to register with an NCC ID or phone number
     * that already used to register another account. Selects the phone and nccID columns from
     * the database, and checks if phoneNum or nccId already exist in one of the rows.
     *
     * @param phoneNum - phone number that user trying to register with
     * //@param nccId    - NCC ID that user trying to register with
     * @return if phone number or NCC ID already exist in the database return true, otherwise false
     */
    public boolean doesExist(String phoneNum) {
        // Creates an ExecutorService that uses a single worker thread operating off an unbounded queue.
        ExecutorService executor = Executors.newSingleThreadExecutor();

        AtomicBoolean doesExist = new AtomicBoolean(false);

        // Executes the thread that will insert the data into the remote db
        executor.execute(() -> {
            //Try to establish a connection to the db
            try (Connection connection = DriverManager.getConnection(URI, USERNAME, PASSWORD)) {
                // prepares the statement passing the ContentValues object
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + TABLE_NAME + " WHERE " + PHONE + " = ? ");

                // Set the barcode in the statement
                statement.setString(1, phoneNum);

                // Execute the statement
                ResultSet resultSet = statement.executeQuery();

                // If the result set is not empty, the guest is registered and return the name
                if (resultSet.next()) {
                    Log.d("TESTING THE DOES EXIST", "doesExist: " + resultSet.getString(PHONE));
                    doesExist.set(true);
                }
            } catch (SQLException e) {
                Log.e(TAG, "Error selecting data from the database", e);
            }
        });

        // Determine if there is at least 1 guest registered with the phone number or NCC ID
        Log.d("TESTING THE DOES EXIST", "doesExist: " + doesExist.get());
        return doesExist.get();
    }

    @Override // Called by the garbage collector
    protected void finalize() throws Throwable {

        // Make sure we close the writable database before this object is finalized
        database.close();

        super.finalize();

    }
    /**
     * getTotalGuests method - Counts the total number of guests registered with the NEST.
     * @return - THe total number of guests registered with the NEST.
     */
    public int getTotalGuests() {
        Cursor c = database.query(
                TABLE_NAME,
                new String[] {registryHelper._ID},
                null,
                null,
                null,
                null,
                null
        );
        int count = 0;
        while (c.moveToNext())
            count++;
        c.close();
        return count;
    }

    /**
     * getMonthlyGuests method - Counts the number of guests registered to the NEST in a given month
     * @param month - The month you wish to check
     * @param year - The year you wish to check
     * @return - The number of guests registered to the NEST in the given month
     */
    public int getMonthlyGuests(int month, int year) {
        DecimalFormat monthFormat = new DecimalFormat("00");
        String queryArgument = monthFormat.format(month) + "__" + year + "%";
        Cursor c = database.query(
                TABLE_NAME,
                new String[] {registryHelper._ID},
                registryHelper.DATE + " LIKE ? ",
                new String[] {queryArgument},
                null,
                null,
                null
        );
        int count = 0;
        while (c.moveToNext())
            count++;
        c.close();
        return count;
    }

    /**
     * setLastVisit -- Updates the guest's last visit date to the date passed
     * @param barcode - the guest's barcode
     * @param phoneNumber - the guest's phoneNumber
     * @param newDate - the date that the guest's last visit column will change to
     */
    public void setLastVisit(String barcode, String phoneNumber, OffsetDateTime newDate) {
        ContentValues values = new ContentValues();
        String todaysDateStr = newDate.toString();

        values.put(GuestRegistryHelper.LASTVISIT, todaysDateStr);
        if ( barcode != null ) {
            database.update(GuestRegistryHelper.TABLE_NAME, values, GuestRegistryHelper.BARCODE + " = ? ",
                    new String[]{barcode});
            Log.d(TAG, "using barcode to find guest, todaysDateStr: " + todaysDateStr);
        } else {
            database.update(GuestRegistryHelper.TABLE_NAME, values, GuestRegistryHelper.PHONE + " = ? ",
                    new String[]{phoneNumber});
            Log.d(TAG, "using phone number to find guest, todaysDateStr: " + todaysDateStr);
        }

    }

    /**
     * getLastVisit -- Retrieves the guest's last visit date
     * @param barcode - the guest's barcode
     * @param phoneNumber - the guest's phoneNumber
     */
    public String getLastVisit(String barcode, String phoneNumber) {
        Cursor cursor;
        String lastDate = null;

        /* if the guest's barcode was successfully passed, use that to query for the guest */
        if (barcode != null) {
            cursor = database.query(GuestRegistryHelper.TABLE_NAME,
                    new String[] {"lastVisit"},
                    "barcode = ? ",
                    new String[] {barcode},
                    null, null, null, null);

            /* if the barcode was not successfully passed, use the guest's phone number */
        } else {
            cursor = database.query(GuestRegistryHelper.TABLE_NAME,
                    new String[] {"lastVisit"},
                    "phone = ? ",
                    new String[] {phoneNumber},
                    null, null, null, null);
        }
        if ( cursor.getCount() > 0 ) {
            Log.d(TAG, "cursor has data");
        }
        cursor.moveToFirst();

        lastDate = cursor.getString(0);
        cursor.close();
        return lastDate;
    }


    /**
     * isConnected checks to see if the app is currently connected to the database.
     * Newer phones seem to hang if the database is called and there is none. After a specified time,
     * false is returned.
     *
     * @return true if connected, false if a connection couldn't be found.
     */
    public boolean isConnected() {
        //This line of code helps improve performance, as older machines such as the pixel 2
        // hang a little longer.
        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        try {
            CompletableFuture<Boolean> future = CompletableFuture.supplyAsync(() -> {
                try {
                    DriverManager.setLoginTimeout(1); // Set a timeout for the connection attempt (e.g., 1 second)
                    try (Connection connection = DriverManager.getConnection(URI, USERNAME, PASSWORD)) {
                        return true;
                    }
                } catch (SQLException e) {
                    return false;
                }
            }, executor);

            return future.get(1, TimeUnit.SECONDS); // Wait for the CompletableFuture to complete within 1 second and return the result
        } catch (TimeoutException e) {
            return false; // Return false if the CompletableFuture takes more than 3 seconds to complete
        } catch (Exception e) {
            return false;
        } finally {
            executor.shutdown(); // Remember to shut down the executor to release resources
        }
    }

}

