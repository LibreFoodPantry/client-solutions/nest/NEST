package edu.ncc.nest.nestapp.GuestVisit.Activities;

/**
 *
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.Locale;

import edu.ncc.nest.nestapp.Choose;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.Activities.GuestDatabaseRegistrationActivity;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistrySource;
import edu.ncc.nest.nestapp.R;

/**
 * GuestVisitActivity: Underlying activity for fragments of the GuestVisit feature.
 */
public class GuestVisitActivity extends AppCompatActivity {
    GuestRegistrySource datasource;
    private String language;
    private static final String TAG = GuestVisitActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Clear the back data for every new activity cycle of guest registration
        deleteSharedPreferences("BackFrag2");
        deleteSharedPreferences("BackFrag3");
        deleteSharedPreferences("BackFrag5");

        setContentView(R.layout.activity_guest_visit);

        // Set the support action bar to the respective toolbar from the layout file
        setSupportActionBar((Toolbar) findViewById(R.id.guest_visit_toolbar));

        //V Change made 12/21/21 V
        //Chris, made change to show SupportActionBar for better navigation
        //getSupportActionBar().hide();

        //Initializing and opening the datasource for the Guest Registry database
        datasource = new GuestRegistrySource(this);

        // Preserves user language selection across fragments and activity state changes.
        // Tags have been provided to help with breakpoints and debugging.
        // Retrieves language from prefs and is used in a conditional to check what language
        // was previously selected / in use in order to keep language up to date. Depending on
        // selection, language is then selected appropriately.
        SharedPreferences prefs = getSharedPreferences("LanguagePrefs",MODE_PRIVATE);
        language = prefs.getString("language",null);

        Log.d(TAG, "Value of language on retrieval in onCreate is: " + language);
        if(language != null) {
            if (language.equals("en")) {
                setLocale("en");
                Log.d(TAG, "Setting language to EN");
            }
            if (language.equals("es")) {
                setLocale("es");
                Log.d(TAG, "Setting language to ES");
            }
        }

        //Spinner for Language translator
        Spinner spinner = findViewById(R.id.guest_visit_manual_language_spinner);
        //ArrayAdapter<CharSequence> arrayAdapter=ArrayAdapter.createFromResource(this,R.array.localestrings, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item);
        if (spinner != null) {
            ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this, R.array.localestrings, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item);
            spinner.setAdapter(arrayAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    // Prepares SharedPreferences for edit, allowing for the preservation of user language.
                    SharedPreferences prefs = getSharedPreferences("LanguagePrefs",MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    if (i==1){
                        // Manually sets language to "english."
                        // Will update when startActivity(getIntent()) is called.
                        setLocale("en");
                        Log.d(TAG, "Setting language to EN");

                        // Saves language selection into prefs (shared preferences).
                        // This is maintained persistently.
                        language = "en";
                        editor.putString("language","en");
                        editor.apply();

                        Intent intent = getIntent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                        // Restarts activity to update UI elements to "english."
                        startActivity(getIntent());
                    } else if (i == 2) {
                        // Manually sets language to "spanish."
                        // Will update when startActivity(getIntent()) is called.
                        setLocale("es");
                        Log.d(TAG, "Setting language to ES");

                        // Saves language selection into prefs (shared preferences).
                        // This is maintained persistently.
                        language = "es";
                        editor.putString("language","es");
                        editor.apply();

                        Intent intent = getIntent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                        // Restarts activity to update UI elements to "spanish."
                        startActivity(getIntent());
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } else {
            Log.d(TAG, "Spinner is null.");
        }

        /*
        //inserts fake guest if they aren't already in the database
        if(datasource.isRegistered("GHI-9012") == null) {
            datasource.insertData("John Doe", "123-456-7890", "N00123456",
                    "01-01-9999", "123 Simple Ave", "Nothingtown", "1234", "NY",
                    "NY", null, null, null, null, null,
                    null, null, null, null, null, null,
                    null, null, null, null,null, null, "GHI-9012");

         */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the toolbar with the menu menu_main
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {// When home button is pressed, resets language selection preference, starting from fresh.
        // Also calls finish();
        SharedPreferences prefsClear = null;
        if (item.getItemId() == R.id.home_btn) {
            prefsClear = getSharedPreferences("LanguagePrefs",MODE_PRIVATE);
            prefsClear.edit().remove("language").commit();
            finish();
        }

        return super.onOptionsItemSelected(item);

    }

    // SetLocale to English or Spanish
    public void setLocale(String language) {
        Locale myLocale = new Locale(language);
        Resources res = this.getResources();
        DisplayMetrics display = res.getDisplayMetrics();
        Configuration configuration = res.getConfiguration();
        configuration.setLocale(myLocale);
        //configuration.locale = myLocale;
        res.updateConfiguration(configuration, display);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override
    protected void onDestroy()
    {
        SharedPreferences prefsClear = null;

        prefsClear = getSharedPreferences("LanguagePrefs",MODE_PRIVATE);
        prefsClear.edit().remove("language").commit();

        deleteSharedPreferences("BackFrag2");
        deleteSharedPreferences("BackFrag3");
        deleteSharedPreferences("BackFrag5");
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        // Redundancy fallback for language
        if (language != null) {
            outState.putString("language",language);
            Log.d(TAG, "Value of language on save is: " + language);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle inState) {
        super.onRestoreInstanceState(inState);
        // Redundancy save for language
        language = inState.getString("language");
        Log.d(TAG, "Value of language on restore is: " + language);
    }

}
