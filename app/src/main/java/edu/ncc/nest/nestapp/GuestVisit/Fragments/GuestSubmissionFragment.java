package edu.ncc.nest.nestapp.GuestVisit.Fragments;

/**
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog.Builder;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;
import java.util.List;

//Removed #502--import edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses.GuestVisitHelper;
import edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses.GuestVisitHelper;
import edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses.GuestVisitSource;
import edu.ncc.nest.nestapp.R;
//Removed #502--import edu.ncc.nest.nestapp.databinding.FragmentGuestVisitQuestionnaireBinding;
//Removed #502-- import edu.ncc.nest.nestapp.databinding.FragmentQuestReportBinding;

/**
 * GuestSubmissionFragment: Used to ask a guest a set of questions about their visit.
 */
public class GuestSubmissionFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = GuestSubmissionFragment.class.getSimpleName();

    // Stores a list of all the views that contain the user's responses
    private List<View> inputFields;

    private Button submitButton;
    private Button lastVisitButton;

    private String guestID;
    private String guestBarcode;
    private String guestPhoneNum;

    GuestVisitSource datasource;


    ////////////// Lifecycle Methods Start //////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_guest_visit_questionnaire, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get all of the input fields from the view
        inputFields = getInputFields(view);

        // Get a reference to the submit button and set this class as the onClickListener
        submitButton = view.findViewById(R.id.questionnaire_submit_btn);

        submitButton.setOnClickListener(this);

        // Get a reference to the temporary button and set this class as the onClickListener
        lastVisitButton = view.findViewById(R.id.last_visit_btn);

        lastVisitButton.setOnClickListener(this);

            // Get info passed from the fragment result
            getParentFragmentManager().setFragmentResultListener("GUEST_CONFIRMED",
                    this, (requestKey, result) -> {

                        /* retrieve the bundle from confirmation */
                        guestPhoneNum = result.getString("GUEST_PHONE");
                        Log.d(TAG, "guest phone number: " + guestPhoneNum);
                        guestBarcode = result.getString("GUEST_BARCODE");
                        Log.d(TAG, "guest barcode: " + guestBarcode);

                        if ((guestID = result.getString("GUEST_ID")) != null) {

                            // Get the EditText of the first question
                            EditText guestIDInputField = (EditText) inputFields.get(0);

                            // Set the input field's text related to the Guest's Id as the barcode we scanned in previous fragment
                            guestIDInputField.setText(guestID);

                            // Disable it so the user can't modify his check-in id.
                            guestIDInputField.setEnabled(false);

                        } else

                            Log.e(TAG, "ERROR: Failed to retrieve GUEST_ID.");

                    });

    }

    ////////////// Other Event Methods Start  //////////////

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.questionnaire_submit_btn) {

            // Used to store the guest's answers
            List<String> fieldTexts = new ArrayList<>();

            for (View inputField : inputFields) {

                // Get the text from the current input field
                String fieldText = getInputFieldText(inputField);

                if (fieldText.isEmpty()) {

                    // Create a Builder for an AlertDialog
                    Builder alertDialogBuilder = new Builder(requireContext());

                    // Build the AlertDialog
                    alertDialogBuilder.setTitle(R.string.questionnaire_fragment_alert_title);

                    alertDialogBuilder.setMessage(R.string.questionnaire_fragment_alert_msg);

                    alertDialogBuilder.setPositiveButton("OK", null);

                    alertDialogBuilder.setCancelable(false);

                    // Create the AlertDialog and show it
                    alertDialogBuilder.create().show();

                    return;

                } else

                    // If the field is empty set its value in the list to null
                    fieldTexts.add(fieldText);

            }

            // If we make it here then the questionnaire is OK to submit

            // Disable all the input fields and submit button so the guest can review their answers

            for (View inputField : inputFields)

                inputField.setEnabled(false);

            submitButton.setEnabled(false);

            // Store answers into the Questionnaire database



            // Open the local database
            GuestVisitHelper localdb = new GuestVisitHelper(getContext());

            //region Database Code
            GuestVisitSource db = new GuestVisitSource(requireContext()).open();
            long rowID;

            // Submit the questionnaire into the database

            //Reference for a string to be stored into the first visit column upon the submission call
            String firstVisit = "";

            //Stores a reference to the currently logged number of visits in the database then increments it by 1
            int numVisits = 0;
            //testing data base for the data

            // getting todays date
            Date today = new Date();
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            String date = formater.format(today);
            Log.d(TAG, "Today's date: " + date);

            //if(db.getVisitCount(guestID) != null)
               // numVisits = Integer.parseInt(db.getVisitCount(guestID));


            numVisits++;

            //Determines if it is the user's first visit by checking the number of visits already logged
            if (numVisits == 1)
                firstVisit = "Yes";
            else if(numVisits > 1)
                firstVisit = "No";

            // checks if connected to remote database
            if (db.isConnected())
            {
                // Sends it to the remote database
                // for now, the parameters are hard coded
                db.insertData("21", fieldTexts.get(3), fieldTexts.get(4), fieldTexts.get(2), "44", date, fieldTexts.get(5));

                Toast.makeText(getContext(), "Questionnaire Submitted. See Log.", Toast.LENGTH_SHORT).show();
                NavHostFragment.findNavController(GuestSubmissionFragment.this)
                        .navigate(R.id.action_GV_QuestionnaireFragment_to_GV_SelectionFragment);
                db.printSubmissions(guestID);
            }
            else
            {
                // adds guest info to the localDB
                boolean addData = localdb.addToLocalNGVDataBase(fieldTexts.get(0), fieldTexts.get(3), fieldTexts.get(4), fieldTexts.get(2), firstVisit, date , fieldTexts.get(5));

                Log.d("**SUBMISSION CHECK**" ,fieldTexts.get(0) +" "+ fieldTexts.get(1) +" "+ fieldTexts.get(2) +" "+ fieldTexts.get(3) +" "+ fieldTexts.get(4) +" "+ fieldTexts.get(5));

                // If there wasn't any errors submitting the database
                if (addData) {

                    // Print the answers to the log and display a toast

                    Log.d(TAG, "Questionnaire Submitted: Guest ID: [" + guestID + "], Submission: " + fieldTexts.toString() + "}");

                    Toast.makeText(getContext(), "Questionnaire Submitted. See Log.", Toast.LENGTH_SHORT).show();
                    NavHostFragment.findNavController(GuestSubmissionFragment.this)
                            .navigate(R.id.action_GV_QuestionnaireFragment_to_GV_SelectionFragment);
                    // TODO Delete this or comment it out as this is only temporary and for debugging purposes
                    // NOTE: The list may grow extremely long in some cases and may clutter the log.
                    // Print a list of all submissions by this guest from the database
                    db.printSubmissions(guestID);

                } else

                    Log.e(TAG, "ERROR: Failed to submit questionnaire.");
            }


        }

        else if ( view.getId() == R.id.last_visit_btn ) {
            /* TODO: clicking submit should navigate to the last visit UI, currently it navigates to selection to avoid merge conflicts
             *   below navigates to the last visit UI */
            Bundle guestInfo = new Bundle();

            /* send over the guest info recieved from confirmation to last visit */
            String guestInfoArray[] = {guestBarcode, guestPhoneNum};
            guestInfo.putStringArray("GUEST_INFO", guestInfoArray);
            NavHostFragment.findNavController(GuestSubmissionFragment.this)
                    .navigate(R.id.action_GV_QuestionnaireFragment_to_GV_LastVisitFragment, guestInfo);
        }

    }


    ////////////// Custom Methods Start  //////////////

    /**
     * Takes 1 NonNull parameter. Gets and returns all of the input field views from the view
     * supplied in a list. It will look through the whole view and get each EditText and Spinner
     * in their respective order.
     * @param view The root view to get the input fields from
     * @return A list containing all of the input fields it found
     */
    private static List<View> getInputFields(@NonNull View view) {

        List<View> idList = new ArrayList<>();

        if (view instanceof ViewGroup) {

            // Cast the root view to a ViewGroup
            ViewGroup viewGroup = (ViewGroup) view;

            // Loop through each child of the view
            for (int i = 0, c = viewGroup.getChildCount(); i < c; i++) {

                if ((view = viewGroup.getChildAt(i)) instanceof ViewGroup)

                    // Add all the children of the child ViewGroup to the list
                    idList.addAll(getInputFields(view));

                if (view instanceof EditText || view instanceof Spinner)

                    idList.add(view);

            }

        }

        return idList;

    }

    /**
     * Takes 1 NonNull parameter. Checks whether or not the supplied View is an instance of
     * EditText or Spinner then casts to it. It then returns the string entered by the user, that
     * is stored within that view.
     * @param view The View/field to get the text from
     * @return The string entered into that View/field by the user
     * @throws ClassCastException If the View/field is not an instance of EditText or Spinner
     */
    private static String getInputFieldText(@NonNull View view) {

        if (view instanceof EditText)

            return ((EditText) view).getText().toString();

        else if (view instanceof Spinner)

            return ((Spinner) view).getSelectedItem().toString();

        throw new ClassCastException("Parameter view is not a valid input field");

    }

}
