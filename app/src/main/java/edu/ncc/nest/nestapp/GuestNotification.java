package edu.ncc.nest.nestapp;
/**
 *

 Copyright (C) 2022 The LibreFoodPantry Developers.


 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.


 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.


 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.
 */
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;

public class GuestNotification extends AppCompatActivity {

    private static final String TAG = "email test";
    Button sendBtn;
    EditText subject, message;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_notification);

        //Retrieve the text from subject EditText
        subject = findViewById(R.id.subMsgTxt);
        //Retrieve the text from message EditText
        message = findViewById(R.id.msgTxt);
        sendBtn = (Button) findViewById(R.id.sendBtn);


        /**
         * get the nccID from the database and store in this array
         */
        //Array which stores the email addresses to send
        String TO[] = {};

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //implementation of Intent to send gmail using Gmail App
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:"));
                //The Strings from TO will go to BCC widget in Gmail App keeping privacy, sometime gmail goes to spam
                emailIntent.putExtra(Intent.EXTRA_BCC, TO);
                //The text in subject will be place in the Subject Widget in Gmail App
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, String.valueOf(subject.getText()));
                //The text in message will be place in the Compose Widget in Gmail App
                emailIntent.putExtra(Intent.EXTRA_TEXT, message.getText());
                startActivity(emailIntent);
            }
        });
    }
}