package edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses;

/**
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.BARCODE;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.NCC_ID;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.PHONE;
import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.TABLE_NAME;

import android.content.ContentValues;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import edu.ncc.nest.nestapp.BuildConfig;

/**
 * GuestRegistryDriver: Handles the connection to the database and the queries to it.
 */
public class GuestRegistryDriver {


    private static final String USERNAME = BuildConfig.GUEST_REGISTRY_DB_USERNAME; // Username for the database
    private static final String PASSWORD = BuildConfig.GUEST_REGISTRY_DB_PASSWORD; // Password for the database
    private static final String DRIVER = "jdbc:mysql://";                          // Driver that is used to connect to the database
    private final String tableName;                                                // Name of the table that is being used
    private String URI = DRIVER + "stargate.ncc.edu:3306/";                             // URI to communicate with the database


    /*
     * Parameterized constructor -
     * Constructor for the GuestRegistryDriver class. This constructor will set the db and table name that it will connect to.
     *
     * @dbName - the name of the database that is being used
     * @param tableName - the name of the table that is being used
     */
    public GuestRegistryDriver(String dbName, String tableName) {
        this.URI += dbName;
        this.tableName = tableName;
    }

    /*
     * insert -
     * This method will insert a new entry into the database.
     *
     * @param values - ContentValues object that contains the values that will be inserted into the database
     */
    public void insert(ContentValues values) {
        // Create a string array of all the data that has been passed in
        String[] valuesArray = values.keySet().toArray(new String[0]);

        // loops through the valuesArray array and creates a string of the column names and values that will be used in the sql query
        String columns = "";
        String valuesString = "";

        for (String key : valuesArray) {
            columns += key + ", ";
            valuesString += "'" + values.get(key) + "', ";
        }

        columns = columns.substring(0, columns.length() - 2);
        valuesString = valuesString.substring(0, valuesString.length() - 2);

        // Create the sql query command
        String sqlQuery = "INSERT INTO " + tableName + " (" + columns + ") VALUES (" + valuesString + ");";
        Log.d("***", sqlQuery);
        // Executes the sql query
        runAsyncUpdate(sqlQuery);
    }

    public boolean doesEntryExist(String phoneNum, String nccID) {
        String sqlQuery = "SELECT phone, nccID FROM " + tableName + " WHERE " + PHONE + " = ? OR " + NCC_ID + " = ?";

        ResultSet resultSet = runAsyncQuery(sqlQuery);

        boolean next = false;
        try {
            next = resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return next;
    }

    public boolean isRegistered(String barcode) {
        String sqlQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + BARCODE + " = ?";


        return false;
    }


    /*
     * runAsyncQuery -
     * This method will run the sql query in a separate thread.
     *
     * @param sqlQuery - the sql query that will be executed
     */
    private void runAsyncUpdate(String sqlQuery) {
        // Create a new thread for asynchronous execution
        ExecutorService executor = Executors.newSingleThreadExecutor();

        // Async task to execute the sql query
        executor.execute(() -> {
            // Try to connect to the database
            try (Connection connection = DriverManager.getConnection(URI, USERNAME, PASSWORD)) {
                PreparedStatement statement = connection.prepareStatement(sqlQuery);
                statement.executeUpdate(sqlQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private ResultSet runAsyncQuery(String sqlQuery) {
        // Create a new thread for asynchronous execution
        ExecutorService executor = Executors.newSingleThreadExecutor();
        final ResultSet[] resultSet = {null};

        // Async task to execute the sql query
        executor.execute(() -> {
            // Try to connect to the database
            try (Connection connection = DriverManager.getConnection(URI, USERNAME, PASSWORD)) {
                PreparedStatement statement = connection.prepareStatement(sqlQuery);
                statement.executeQuery(sqlQuery);
                resultSet[0] = statement.getResultSet();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        return resultSet[0];
    }
}
