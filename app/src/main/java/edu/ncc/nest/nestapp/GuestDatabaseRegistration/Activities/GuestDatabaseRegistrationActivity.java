package edu.ncc.nest.nestapp.GuestDatabaseRegistration.Activities;

/**
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;


import java.util.Locale;

import edu.ncc.nest.nestapp.Choose;
import edu.ncc.nest.nestapp.R;


/**
 * GuestDatabaseRegistrationActivity: This is the underlying activity for the fragments of the
 * GuestDatabaseRegistration feature.
 */

public class GuestDatabaseRegistrationActivity extends AppCompatActivity {

    private String language;
    private static final String TAG = GuestDatabaseRegistrationActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //Clear the back data for every new activity cycle of guest registration
        deleteSharedPreferences("BackFrag2");
        deleteSharedPreferences("BackFrag3");
        deleteSharedPreferences("BackFrag5");

        setContentView(R.layout.activity_guest_database_registration);

        setSupportActionBar(findViewById(R.id.database_registration_toolbar));

        //Spinner for Language translator
        Spinner spinner = findViewById(R.id.language_spinner);
        //ArrayAdapter<CharSequence> arrayAdapter=ArrayAdapter.createFromResource(this,R.array.localestrings, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item);
        if (spinner != null) {
            ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this, R.array.localestrings, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item);
            spinner.setAdapter(arrayAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Prepares SharedPreferences for edit, allowing for the preservation of user language.
                SharedPreferences prefs = getSharedPreferences("LanguagePrefs",MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                if (i==1) {
                    // Manually sets language to "english."
                    // Will update when startActivity(getIntent()) is called.
                    setLocale("en");
                    Log.d(TAG, "Setting language to EN");

                    // Saves language selection into prefs (shared preferences).
                    // This is maintained persistently.
                    language = "en";
                    editor.putString("language","en");
                    editor.apply();

                    // Restarts activity to update UI elements to "english."
                    //fixes the issue of having multiple activity's stacked on each other
                    //when a new language is chosen, the previous activity is taken off the
                    //stack and the new updated activity with the updated language is pushed
                    //onto the stack with startActivity(getIntent());
                    Intent intent = getIntent();
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(getIntent());
                } else if (i == 2) {
                    // Manually sets language to "spanish."
                    // Will update when startActivity(getIntent()) is called.
                    setLocale("es");
                    Log.d(TAG, "Setting language to ES");

                    // Saves language selection into prefs (shared preferences).
                    // This is maintained persistently.
                    language = "es";
                    editor.putString("language","es");
                    editor.apply();

                    //fixes the issue of having multiple activity's stacked on each other
                    Intent intent = getIntent();
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(getIntent());
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        } else {
            Log.d(TAG, "Spinner is null.");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;

    }

    /**
     * Finishes the current activity --GuestDatabaseregistration-- and goes to the homepage
     * {@link Choose} Activity
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // When home button is pressed, resets language selection preference, starting from fresh.
        // Also calls finish();
        SharedPreferences prefsClear = null;
        if (item.getItemId() == R.id.home_btn) {
            prefsClear = getSharedPreferences("LanguagePrefs", MODE_PRIVATE);
            prefsClear.edit().remove("language").commit();

            finish();
        }
        if(item.getItemId() == R.id.action_spanish){
            SharedPreferences prefs = getSharedPreferences("LanguagePrefs", MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            // Manually sets language to "spanish."
            // Will update when startActivity(getIntent()) is called.
            setLocale("es");
            Log.d(TAG, "Setting language to ES");

            // Saves language selection into prefs (shared preferences).
            // This is maintained persistently.
            language = "es";
            editor.putString("language", "es");
            editor.apply();

            //fixes the issue of having multiple activity's stacked on each other
            Intent intent = getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(getIntent());
        }
        if(item.getItemId() == R.id.action_english){
            SharedPreferences prefs = getSharedPreferences("LanguagePrefs", MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            // Manually sets language to "english."
            // Will update when startActivity(getIntent()) is called.
            setLocale("en");
            Log.d(TAG, "Setting language to EN");

            // Saves language selection into prefs (shared preferences).
            // This is maintained persistently.
            language = "en";
            editor.putString("language", "en");
            editor.apply();

            // Restarts activity to update UI elements to "english."
            //fixes the issue of having multiple activity's stacked on each other
            //when a new language is chosen, the previous activity is taken off the
            //stack and the new updated activity with the updated language is pushed
            //onto the stack with startActivity(getIntent());
            Intent intent = getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(getIntent());
        }

        return super.onOptionsItemSelected(item);

    }



    /*
    /**
     * home --
     * Starts the {@link Choose} Activity
     */
    /*
    public void home() {

        Intent intent = new Intent(this, Choose.class);

        startActivity(intent);

    }

     */

    // SetLocale to English or Spanish
    public void setLocale(String language) {
        Locale myLocale = new Locale(language);
        Resources res = this.getResources();
        DisplayMetrics display = res.getDisplayMetrics();
        Configuration configuration = res.getConfiguration();
        configuration.setLocale(myLocale);
        //configuration.locale = myLocale;
        res.updateConfiguration(configuration, display);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {

        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy()
    {
        SharedPreferences prefsClear = null;

        prefsClear = getSharedPreferences("LanguagePrefs",MODE_PRIVATE);
        prefsClear.edit().remove("language").commit();

        deleteSharedPreferences("BackFrag2");
        deleteSharedPreferences("BackFrag3");
        deleteSharedPreferences("BackFrag5");
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        // Redundancy fallback for language
        if (language != null) {
            outState.putString("language",language);
            Log.d(TAG, "Value of language on save is: " + language);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle inState) {
        super.onRestoreInstanceState(inState);
        // Redundancy save for language
        language = inState.getString("language");
        Log.d(TAG, "Value of language on restore is: " + language);
    }
}
