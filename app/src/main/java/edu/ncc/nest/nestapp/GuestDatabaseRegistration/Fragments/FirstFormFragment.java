package edu.ncc.nest.nestapp.GuestDatabaseRegistration.Fragments;

/**
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.fragment.NavHostFragment;

import java.util.List;

import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistrySource;
import edu.ncc.nest.nestapp.GuestVisit.Activities.GuestVisitActivity;
import edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses.GuestVisitSource;
import edu.ncc.nest.nestapp.R;
import edu.ncc.nest.nestapp.databinding.FragmentGuestDatabaseRegistrationFirstFormBinding;


/**
 * FirstFormFragment: Represents a form that a guest can fill in with their personal information
 * such as, name, phone-number, email-address, ncc-id, postal-address, city, zip-code, birth-date,
 * and date-of-registration. The fragment should then bundle all of the user's inputs and sends them
 * to the next fragment {@link SecondFormFragment}.
 */
public class FirstFormFragment extends Fragment {

    public static final String TAG = FirstFormFragment.class.getSimpleName();
    public static final String AUTOFILL_HINTS_FORMAT = "please enter phone number in this format: 8004445555";

    private final Bundle result = new Bundle();
    // declare the binding for the current fragment.
    private FragmentGuestDatabaseRegistrationFirstFormBinding binding;
    // Database where we will store user information
    private GuestRegistrySource db;

    private GuestVisitSource qdb;
    // Variables to store user information
    private EditText firstName, lastName, phoneNumber, nccID;
    private String inputFirstName, inputLastName, inputPhoneNumber;
    private List<String> stateInfo;

    private boolean validFName, validLName, validPhone, validInput = false;

    // handles the backbutton override warning
    private OnBackPressedCallback backbuttonCallback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        binding = FragmentGuestDatabaseRegistrationFirstFormBinding.inflate(inflater, container, false);
        return binding.getRoot();

        /* for binding, change this commented code with the code currently being used
        return inflater.inflate(R.layout.fragment_guest_database_registration_first_form,
                container, false);
        */
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //pop up to make sure you registered ip
        //remove this later when we can pull ip from the users
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true); //let's the user click out of the pop up
        builder.setTitle("Make sure you're ip is registered");
        builder.setMessage("To get your ip click on your Start Menu and type cmd in the search box and press enter. A black and white window will open where you will type ipconfig /all and press enter. There is a space between the command ipconfig and the switch of /all. Your ip address will be the IPv4 address.\n\nTo register your ip go to https://stargate.ncc.edu/dbipregister/ and fill out the form.");
        // handles the 'cancel' button
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel(); // tells android we 'canceled', not dismiss. more appropriate
            }
        });
        AlertDialog ipMsg = builder.create();
        ipMsg.show();


        // override back button to give a warning
        backbuttonCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                // show dialog prompting user
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setCancelable(false);
                builder.setTitle(R.string.Are_you_sure);
                builder.setMessage(R.string.Data_on_this_page_may_not_be_saved);
                // used to handle the 'confirm' button
                builder.setPositiveButton(R.string.im_sure, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // continue by disabling this callback then calling the backpressedispatcher again
                        // when this was enabled, it was at top of backpressed stack. When we disable, the next item is default back behavior
                        backbuttonCallback.setEnabled(false);
                        requireActivity().getOnBackPressedDispatcher().onBackPressed();
                    }
                });
                // handles the 'cancel' button
                builder.setNegativeButton(R.string.stay_on_page, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel(); // tells android we 'canceled', not dismiss. more appropriate
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        };
        // need to add the callback to the activities backpresseddispatcher stack.
        // if enabled, it will run this first. If disabled, it will run the default (next item in stack)
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), backbuttonCallback);

        //Toast.makeText(getContext(), "WARNING: Pressing back will clear data. Please double check before continuing.", Toast.LENGTH_LONG).show();


        Log.d("***", result.toString());


        // Creating the database and passing the correct context as the argument
        db = new GuestRegistrySource(requireContext());

        binding.nextButtonFirstFragmentGRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputFirstName = ((EditText) getView().findViewById(R.id.grf_1_fName)).getText().toString();
                inputLastName = ((EditText) getView().findViewById(R.id.grf_1_lName)).getText().toString();
                inputPhoneNumber = ((EditText) getView().findViewById(R.id.grf_1_phone)).getText().toString();

                Log.d(TAG, "first name: " + inputFirstName);
                Log.d(TAG, "last name: " + inputLastName);
                Log.d(TAG, "phone number: " + inputPhoneNumber);
               // Log.d(TAG, "NCC ID: " + inputNCCID);

                // leaving the '= false' in case user goes back and deletes.

                if(inputFirstName.matches(".*[0-9\\[\\]\\\\!@#$/%^?><,.;:|~`&*()_+={}].*")) {
                    binding.enterFirstName.setText(R.string.Incorrect_first_name_format);
                    binding.enterFirstName.setTextColor(Color.RED);
                    validFName = false;
                }
                else if (inputFirstName.length() == 0) {
                    binding.enterFirstName.setTextColor(Color.RED);
                    binding.enterFirstName.setText(R.string.You_must_enter_a_first_name);
                    validFName = false;
                } else {
                    binding.enterFirstName.setTextColor(Color.GRAY);
                    binding.enterFirstName.setText(R.string.Enter_your_first_name);
                    result.putString("First Name", inputFirstName);
                    validFName = true;
                }

                if(inputLastName.matches(".*[0-9\\[\\]\\\\!@#$/%^?><,.;:|~`&*()_+={}].*")) {
                    binding.enterLastName.setText(R.string.Incorrect_last_name_format);
                    binding.enterLastName.setTextColor(Color.RED);
                    validLName = false;
                }
                else if (inputLastName.length() == 0) {
                    binding.enterLastName.setTextColor(Color.RED);
                    binding.enterLastName.setText(R.string.You_must_enter_a_last_name);
                    validLName = false;
                }
                 else {
                    binding.enterLastName.setTextColor(Color.GRAY);
                    binding.enterLastName.setText(R.string.Enter_your_last_name);
                    result.putString("Last Name", inputLastName);
                    validLName = true;
                }


              if (inputPhoneNumber.matches(".*[-()N+*)#;.,/+].*")) {
                  binding.enterPhoneNumber.setTextColor(Color.RED);
                  binding.enterPhoneNumber.setText(R.string.Please_fix_phone_num);
                  validPhone = false;
              }
                else if (inputPhoneNumber.length() !=10 ) {
                  binding.enterPhoneNumber.setTextColor(Color.RED);
                  binding.enterPhoneNumber.setText(R.string.You_must_enter_a_valid_phone_number);
                  validPhone = false;
              }
               else {
                    binding.enterPhoneNumber.setTextColor(Color.GRAY);
                    binding.enterPhoneNumber.setText(R.string.enter_your_phone_number);
                    result.putString("Phone Number", inputPhoneNumber);
                    validPhone = true;
                }




                // check all the fields
                validInput = validFName && validLName && validPhone;

                if (validInput) {
                    getParentFragmentManager().setFragmentResult("sending_first_form_fragment_info", result);

                    // To test the doesExist() method uncomment this,
                    // and comment out the findNavController() that's currently being used

                    // Creating the database and passing the correct context as the argument
                    db = new GuestRegistrySource(requireContext());
                    GuestRegistryHelper dbLocal = new GuestRegistryHelper(getContext());

                    /*
                    Test
                    dbLocal.deleteTableRow(2);
                    dbLocal.isDataInLocal();*/


                    // if true, user already exist in the db, else user can register
                  if ( ((db.isConnected()) && db.doesExist(inputPhoneNumber)) || dbLocal.doesExistInLocal(inputPhoneNumber)   ) {
                        Log.d("TESTING THE DOES EXIST", "onClick: " + "User already exists");
                        Log.d(TAG, "onClick: An user already registered with this phone number !");


                        binding.enterPhoneNumber.setTextColor(Color.RED);
                        binding.enterPhoneNumber.setText("This phone number is already being used!");

                        Intent intent = new Intent(requireContext(), GuestVisitActivity.class);

                        Toast.makeText( getContext(),"This phone number is already being used! Please Sign In Here", Toast.LENGTH_LONG).show();

                        // Go to the GuestVisit activity
                        startActivity(intent);

                    } else {
                        Log.d("TESTING THE DOES EXIST", "onClick: " + "User does not exist");
                        NavHostFragment.findNavController(FirstFormFragment.this)
                                .navigate(R.id.action_DBR_FirstFormFragment_to_SecondFormFragment);



                    }

                }
            }
        });

//        // Creating the database and passing the correct context as the argument
//        db = new GuestRegistrySource(requireContext());
//
//        view.findViewById(R.id.next_button_first_fragment_gRegistration).setOnClickListener(v ->
//        {
//
//            // Getting a handle on info from the UI
//            // TODO: line #108
//            // name = view.findViewById(R.id.editText);
//            // phone = view.findViewById(R.id.editText2);
//            // email = view.findViewById(R.id.editText3);
//
//            inputFirstName = ((EditText) getView().findViewById(R.id.guestreg_fname)).getText().toString();
//            inputLastName = ((EditText) getView().findViewById(R.id.guestreg_lname)).getText().toString();
//            inputPhoneNumber = ((EditText) getView().findViewById(R.id.guestreg_phone)).getText().toString();
//            inputNCCID = ((EditText) getView().findViewById(R.id.guestreg_nccid)).getText().toString();
//
//            /*
//            firstName = view.findViewById(R.id.guestreg_fname);
//            lastName = view.findViewById(R.id.guestreg_lname);
//            phoneNumber = view.findViewById(R.id.guestreg_phone);
//            nccID = view.findViewById(R.id.guestreg_nccid);
//
//            firstName.getText().toString();
//            lastName.getText().toString();
//            phoneNumber.getText().toString();
//            nccID.getText().toString();
//
//             */
//
//            Log.d("**", firstName.getText().toString());
//
//            if(inputFirstName.length() > 0 &&
//                    inputLastName.length() > 0 &&
//                    inputPhoneNumber.length() == 10 &&
//                    inputNCCID.length() == 8) {
//
//                Bundle bundle = new Bundle();
//
//                bundle.putString("First Name", inputFirstName);
//                bundle.putString("Last Name", inputLastName);
//                bundle.putString("Phone Number", inputPhoneNumber);
//                bundle.putString("NCC ID", inputNCCID);
//
//                getParentFragmentManager().setFragmentResult("sending_first_fragment_result", bundle);
//
//                Log.d("**", "in here");
//
////                NavHostFragment.findNavController(FirstFormFragment.this)
////                        .navigate(R.id.action_DBR_FirstFormFragment_to_SecondFormFragment);
//            }
//            else{
//                Log.d("**", "in here else");
//            }


//            // For testing purposes
//            Log.d(TAG, "The name is: " + inputName);
//            Log.d(TAG, "The phone is: " + inputPhone);
//            Log.d(TAG, "The email is: " + inputEmail);
//
//            // TODO: When fragments updated by the UI team, use bindings to store the rest of the inputs
//            String inputId = id.getText().toString();
//            String inputAddress = address.getText().toString();
//            String inputCity = city.getText().toString();
//            String inputZip = zip.getText().toString();
//            String inputDate = date.getText().toString();
//
//            Bundle bundle = new Bundle();
//
//            bundle.putString("NAME", inputName);
//            bundle.putString("PHONE", inputPhone);
//            bundle.putString("EMAIL", inputEmail);
//            bundle.putString("ID", inputId);
//            bundle.putString("ADDRESS", inputAddress);
//            bundle.putString("CITY", inputCity);
//            bundle.putString("ZIP", inputZip);
//            bundle.putString("DATE", inputDate);

        // TODO Set the bundle as the FragmentResult for the next fragment to retrieve

        // Navigate to the SecondFormFragment
//            NavHostFragment.findNavController(FirstFormFragment.this)
//                    .navigate(R.id.action_DBR_FirstFormFragment_to_SecondFormFragment);

//        });

    }


//    /**
//     * onClick --
//     * Submits user-data when click is received.
//     * Notifies user in a toast if the adding is successful
//     *
//     * @param view The view that was clicked
//     */
//    @Override
//    public void onClick(View view) {
//
//        // TODO This method may be getting replaced by sending the data to the next fragment instead.
//        // If the data is being passed through fragments, you may need to move this method to the
//        // last fragment. See onViewCreated()
//
//        // Variable used for checks
//        boolean ins = false;
//
//        // Adding the values into the database if submit button is pressed
//        if (view.getId() == R.id.next_button_first_fragment_gRegistration) {
//
//
//
//            // NOTE: The parameter 'barcode' was recently added to this method
//            // TODO: Update parameter 'barcode' to the barcode representing this user
//            //ins = db.insertData(name.getText().toString(), email.getText().toString(), phone.getText().toString(), date.getText().toString(), address.getText().toString(), city.getText().toString(), zip.getText().toString(), null);
//
//        }
//
//        // Notifying the user if the add was successful
//        if (ins) {
//
//            // For testing purposes, comment out if not needed.
//            Toast.makeText(requireContext(), "User Added", Toast.LENGTH_LONG).show();
//
//        }
//
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}
