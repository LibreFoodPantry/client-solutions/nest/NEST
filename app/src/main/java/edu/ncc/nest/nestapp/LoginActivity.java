package edu.ncc.nest.nestapp;


import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

/**
 *
 * Copyright (C) 2023 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class LoginActivity extends AppCompatActivity {

    //This UI was created in a video on YouTube https://www.youtube.com/watch?v=BNBICA80tko&t=1349s.
    // I followed his tutorial and did some tweaks.
    
    private static final String TAG = "**Login**";
    EditText emailLogin, passwordLogin;
    Button btnSubmit;
    CardView btnReg;
    String adminUser = "Admin"; //default admin username
    String volUser = "Volunteer"; //default volunteer username
    String adminPass = "Admin"; //default admin password
    String volPass = "Volunteer"; //default volunteer password

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        emailLogin = findViewById(R.id.login_email);
        passwordLogin = findViewById(R.id.login_password);
        // updating the font of the password hint to match that of the email hint
        passwordLogin.setTypeface(Typeface.DEFAULT_BOLD );
        passwordLogin.setTransformationMethod(new PasswordTransformationMethod());
        btnSubmit = findViewById(R.id.submit_btn);
        //btnReg = findViewById(R.id.btnReg);
        Intent int1 = new Intent(this, AdminActivity.class);
        Intent int2 = new Intent(this, VolunteerAccountActivity.class);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strEmail = emailLogin.getText().toString();
                String strPassword = passwordLogin.getText().toString();
                //check if email and password are for the Admin
                if(adminUser.equals(strEmail) && adminPass.equals(strPassword))
                {
                    //code to move to Admin screen
                    startActivity(int1);
                    Toast.makeText(LoginActivity.this, "Admin login success!",
                            Toast.LENGTH_SHORT).show();
                }
                //check if email and password are for the Volunteer
                else if(volUser.equals(strEmail) && volPass.equals(strPassword))
                {
                    startActivity(int2);
                    Toast.makeText(LoginActivity.this, "Volunteer login success!",
                            Toast.LENGTH_SHORT).show();
                }
                //invalid login info
                else
                {
                    Toast.makeText(LoginActivity.this, "Invalid Login!",
                            Toast.LENGTH_SHORT).show();
                }


                    Log.d(TAG, "User email: " + strEmail + " and password: "
                        + strPassword);


            }
        });
    }

    }