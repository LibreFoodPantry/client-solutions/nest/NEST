package edu.ncc.nest.nestapp.GuestDatabaseRegistration.Fragments;

/**
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

import java.time.OffsetDateTime;
import java.util.Locale;
import java.util.Arrays;

import edu.ncc.nest.nestapp.CheckExpirationDate.Activities.CheckExpirationDateActivity;

import edu.ncc.nest.nestapp.Choose;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.Activities.GuestDatabaseRegistrationActivity;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistrySource;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.UIClasses.MultiSelectSpinner;
import edu.ncc.nest.nestapp.GuestFormTesting;
import edu.ncc.nest.nestapp.GuestVisit.Activities.GuestVisitActivity;
import edu.ncc.nest.nestapp.MainActivity;
import edu.ncc.nest.nestapp.R;
import edu.ncc.nest.nestapp.databinding.FragmentGuestDatabaseRegistrationSummaryBinding;

/**
 * SummaryFragment: This fragment represent a summary of the registration process. Displays messages
 * to the guest depending on whether or not the registration process was successful or not. Should
 * also generate a barcode for the guest if needed.
 */
public class SummaryFragment extends Fragment {
    private FragmentGuestDatabaseRegistrationSummaryBinding binding;
    private OnBackPressedCallback backbuttonCallback;

    // Instance variables for the FirstFormFragment
    private String fname; //First Name
    private String lname; //Last Name
    private String phoneNum; //Phone Number
    private String nccId; //NCCID

    // Instance variables for the SecondFormFragment
    private String streetAddress1;
    private String streetAddress2;
    private String city;
    private String state;
    private String zip;
    private String affiliation;
    private String age;
    private String gender;

    // Instance variables for the ThirdFormFragment
    private String dietary; //Dietary needs
    private String snap; //If they have snap
    private String otherProg; // if they know about any other programs other then nest
    private String employment; // Employment information
    private String health; //health information
    private String housing; // housing status
    private String income;

    // Instance variables for the FourthFormFragment
    private String householdNum;
    private String children1;
    private String children5;
    private String children12;
    private String children18;

    // barcode info
    private String barcode;

    // guest last visit - date of registration
    private String lastVisit;

    // Database where we will store user information
    private GuestRegistrySource db;

    //LogD Tag
    public static final String TAG = SummaryFragment.class.getSimpleName();

    private boolean doneButtonClicked = false;
    private boolean hasValidationErrors = false;
    private boolean regButtonClicked = false;
    private boolean checkInButtonClicked = false;

    private boolean isEditable = false;

    private MultiSelectSpinner multiselectDietary, multiselectEmployment, multiselectHealth, multiselectHousing;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentGuestDatabaseRegistrationSummaryBinding.inflate(inflater, container, false);

        Log.d(TAG, "In SummaryFragment onCreateView()");

        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // override back button to give a warning
        backbuttonCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                // show dialog prompting user
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setCancelable(false);
                builder.setTitle("Are you sure?");
                builder.setMessage("Data entered on this page may not be saved.");
                // used to handle the 'confirm' button
                builder.setPositiveButton("Yes, I'm Sure", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // continue by disabling this callback then calling the backpressedispatcher again
                        // when this was enabled, it was at top of backpressed stack. When we disable, the next item is default back behavior
                        backbuttonCallback.setEnabled(false);
                        requireActivity().getOnBackPressedDispatcher().onBackPressed();
                    }
                });
                // handles the 'cancel' button
                builder.setNegativeButton("Stay On This Page", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel(); // tells android we 'canceled', not dismiss. more appropriate
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        };
        // need to add the callback to the activities backpresseddispatcher stack.
        // if enabled, it will run this first. If disabled, it will run the default (next item in stack)
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), backbuttonCallback);

        // TODO do we need a back verification here? prob not if fields are not editable. otherwise copy/paste from previous fragments

        // Creating the database and passing the correct context as the argument
        db = new GuestRegistrySource(requireContext());

        //retrieving first name, last name, phone number and NCC ID from FirstFormFragment bundle.
        getParentFragmentManager().setFragmentResultListener("sending_first_form_fragment_info",
                this, new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {

                        //retrieving information & assigning to instance variables
                        fname = result.getString("First Name");
                        lname = result.getString("Last Name");
                        phoneNum = result.getString("Phone Number");

                        //LogD
                        Log.d(TAG, "The first name obtained is: " + fname);
                        Log.d(TAG, "The last name is: " + lname);
                        Log.d(TAG, "The phone number is: " + phoneNum);

                        //setting summary fragment textviews
                        binding.grf1FName.setText(fname);
                        binding.grf1LName.setText(lname);
                        binding.grf1Phone.setText("(" + phoneNum.substring(0, 3) + ") " + phoneNum.substring(3, 6) + "-" + phoneNum.substring(6));
                    }
                });//end retrieving FirstFormFragement

        //retrieving streetAddress1&2, city, state zip, affiliation, age, and gender from SecondFormFragment bundle.
        getParentFragmentManager().setFragmentResultListener("sending_second_form_fragment_info",
                this, new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {

                        //retrieving information & assigning to instance variables
                        streetAddress1 = result.getString("Street Address 1");
                        streetAddress2 = result.getString("Street Address 2");  //optional
                        city = result.getString("City");
                        state = result.getString("State");
                        zip = result.getString("Zip");
                        affiliation = result.getString("Affiliation");
                        age = result.getString("Age");
                        gender = result.getString("Gender");
                        nccId = result.getString("NCC ID");

                        //LogD
                        Log.d(TAG, "The street address 1: " + streetAddress1);
                        Log.d(TAG, "The street address 2 (optional): " + streetAddress2);
                        Log.d(TAG, "The city is: " + city);
                        Log.d(TAG, "The state is: " + state);
                        Log.d(TAG, "The zip code is: " + zip);
                        Log.d(TAG, "The affiliation is: " + affiliation);
                        Log.d(TAG, "The age is: " + age);
                        Log.d(TAG, "The gender is: " + gender);
                        Log.d(TAG, "The NCC ID is: " + nccId);

                        //setting summary fragment textview streetaddress
                        binding.grf2Address1.setText(streetAddress1);
                        //if street adress 2 was subbmitted then assign to street address 2
                        if (streetAddress2 != null)
                            binding.grf2Address2.setText(streetAddress2);
                        //setting the rest of the summary fragment textviews
                        binding.grf2City.setText(city);
                        binding.grf2State.setText(state);
                        binding.grf2Zip.setText(zip);
                        binding.grf2Affiliation.setText(affiliation);
                        binding.grf2Age.setText(age);
                        binding.grf2Gender.setText(gender);
                        if (nccId.equals("N/A"))
                            binding.grf2NccId.setText(nccId);
                        else
                            binding.grf2NccId.setText("N" + nccId);


                        initializeAndPreselectSpinner(binding.edittingGrf2Affiliation, R.array.affiliation_array, affiliation);
                        initializeAndPreselectSpinner(binding.edittingGrf2Age, R.array.age_array, age);
                        initializeAndPreselectSpinner(binding.edittingGrf2Gender, R.array.gender_array, gender);
                        initializeAndPreselectSpinner(binding.edittingGrf2State, R.array.states_array, state);


                    }
                });//end retrieving SecondFormFragement


        // retrieving dietary, other programs, snap, employment, health, and housing info from ThirdFormFragment bundle.
        getParentFragmentManager().setFragmentResultListener("sending_third_form_fragment_info",
                this, new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                        //retrieving information & assigning to instance variables
                        dietary = result.getString("dietary");
                        snap = result.getString("snap");
                        otherProg = result.getString("programs");
                        employment = result.getString("employment");
                        health = result.getString("health");
                        housing = result.getString("housing");
                        income = result.getString("income");

                        //LogD
                        Log.d(TAG, "The dietary information is: " + dietary);
                        Log.d(TAG, "The snap information is: " + snap);
                        Log.d(TAG, "The other program information is: " + otherProg);
                        Log.d(TAG, "The employment obtained is: " + employment);
                        Log.d(TAG, "The health information is: " + health);
                        Log.d(TAG, "The housing information is: " + housing);
                        Log.d(TAG, "The income information is: " + income);

                        /*setting summary fragment textviews
                         *format of setText(replaceAll) is:
                         * \nBOLD LABEL:
                         * Text 1
                         * Text 2
                         * Text 3
                         */
                        binding.grf3Dietary.setText(dietary.replaceAll(", ", "\n"));
                        binding.grf3Snap.setText("\n" + snap);
                        binding.grf3OtherProgs.setText(otherProg.trim());
                        binding.grf3StatusEmployment.setText(employment.replaceAll(", ", "\n"));
                        binding.grf3StatusHealth.setText(health.replaceAll(", ", "\n"));
                        binding.grf3StatusHousing.setText(housing.replaceAll(", ", "\n"));
                        binding.grf3Income.setText("\n" + income);

                        // Initialize the spinners and preselect values after data is retrieved
                        initializeAndPreselectSpinner(binding.edittingGrf3OtherProgs, R.array.yes_no_select_one, otherProg);
                        initializeAndPreselectSpinner(binding.edittingGrf3Snap, R.array.yes_no_select_one, snap);

                        initializeMultiSelectSpinners(dietary,housing,health,employment);


                    }
                });//end retrieving ThirdFormFragement

        // retrieving household number, childcare status, and age info of children from FourthFormFragment bundle.
        getParentFragmentManager().setFragmentResultListener("sending_fourth_form_fragment_info",
                this, new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {

                        //retrieving information & assigning to instance variables
                        householdNum = result.getString("householdNum");
                        children1 = result.getString("children1");
                        children5 = result.getString("children5");
                        children12 = result.getString("children12");
                        children18 = result.getString("children18");

                        //LogD
                        Log.d(TAG, "The house hold number is: " + householdNum);
                        Log.d(TAG, "The children 1 is: " + children1);
                        Log.d(TAG, "The children 5 is: " + children5);
                        Log.d(TAG, "The children 12 is: " + children12);
                        Log.d(TAG, "The children 18 is: " + children18);

                        //setting summary fragment textviews
                        binding.grf4NumPeople.setText(householdNum);
                        binding.grf4Children1.setText(children1);
                        binding.grf4Children5.setText(children5);
                        binding.grf4Children12.setText(children12);
                        binding.grf4Children18.setText(children18);

                        // Initialize the spinners and preselect values after data is retrieved
                        initializeAndPreselectSpinner(binding.edittingGrf4Children1, R.array.nums_0_20, children1);
                        initializeAndPreselectSpinner(binding.edittingGrf4Children5, R.array.nums_0_20, children5);
                        initializeAndPreselectSpinner(binding.edittingGrf4Children12, R.array.nums_0_20, children12);
                        initializeAndPreselectSpinner(binding.edittingGrf4Children18, R.array.nums_0_20, children18);
                        initializeAndPreselectSpinner(binding.edittingGrf4NumPeople, R.array.nums_1_20, householdNum);
                    }
                });//end retrieving FouthFormFragement

        getParentFragmentManager().setFragmentResultListener("sending_barcode_info",
                this,
                new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                        barcode = result.getString("registrationBarcode");

                        binding.edittextEnterUpc.setText(barcode);
                        Log.d("**BARCODE**", "BARCODE: " + barcode);
                    }
                });//end retrieving FirstFormFragement

        // OnClickListener for the "Done" button

        view.findViewById(R.id.button_done_summary).setOnClickListener(clickedView -> {
            /* is there a way to use the saveToDatabase method here and streamline this code?
             * see code commented out below around line 394*/
            // registering the guest to the database
            // TODO: null values needs to be retrieved and replaced.
            //String nameOfVolunteer = volunteerFName + " " + volunteerLName;
            lastVisit = OffsetDateTime.now().toString(); // last visit date will start as the date of registration

            doneButtonClicked = true;
            saveToDatabase();
            /*
                    if (db.isConnected()) {
                        db.insertData(fname + " " + lname, phoneNum, nccId, streetAddress1 + ", " + streetAddress2,
                                city, zip, state, affiliation, age, gender, dietary, otherProg, snap, employment, health, housing,
                                income, householdNum, children1, children5, children12, children18, barcode, lastVisit);


                    } else {

                        //Opens Local Database
                        //maybe use requirecontext instead of getcontext
                        GuestRegistryHelper localRegDatabase = new GuestRegistryHelper(getContext());
                        OffsetDateTime todaysDate = OffsetDateTime.now();
                        lastVisit = todaysDate.toString();
                        Log.d(TAG, "todays date: " + lastVisit);
                        doneButtonClicked = true;
                        saveToDatabase();

                        boolean addedData = localRegDatabase.addToLocalGRDataBase(fname + " " + lname, phoneNum, nccId, streetAddress1 + ", " + streetAddress2,
                                city, zip, state, affiliation, age, gender, dietary, otherProg, snap, employment, health, housing,
                                income, householdNum, children1, children5, children12, children18,
                                barcode, lastVisit);


                        if (addedData) {
                            Log.d(TAG, "data successfully added to localdatabase");
                        } else {
                            Log.d(TAG, "onViewCreated: data NOT added to localdatabase!!!");
                        }


                    }
                    */
            // go back to 'guest forms' page. decided this makes more sense than app home.
            // see method definition below for explanation how to switch to to navigate to app home instead
            createConfirmedDialog(); // show a dialog first so they know it worked.
        });

        // Navigate back to splash screen.
        // later, make if/else to go to scanner or login if scanner already in db
            /*
            NavHostFragment.findNavController(SummaryFragment.this)
                    .navigate(R.id.action_DBR_SummaryFragment_to_DBR_StartFragment);

             */


        // OnClickListener for the "Check-in" button
        view.findViewById(R.id.button_check_in).setOnClickListener(clickedView -> {
            checkInButtonClicked = true;
            saveToDatabase();
        });
        // OnClickListener for the "Done" button

        view.findViewById(R.id.button_registration).setOnClickListener(clickedView -> {
            regButtonClicked = true;
            saveToDatabase();
        });

        /*
        // OnClickListener for the "Done" button
        //TODO store in database when done button is clicked
          view.findViewById(R.id.button).setOnClickListener(clickedView -> {

              // Navigate back to splash screen.
              // later, make if/else to go to scanner or login if scanner already in db
              NavHostFragment.findNavController(SummaryFragment.this)
                      .navigate(R.id.action_DBR_SummaryFragment_to_DBR_StartFragment);
          });
        */
        binding.buttonConfirmRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                boolean isValid = true;
                if(fname == null || fname.isEmpty()) {
                    binding.grf1FName.setError("First name is required!");
                    isValid = false;
                }
                if(lname == null || fname.isEmpty()) {
                    binding.grf1LName.setError("Last name is required!");
                    isValid = false;
                }
                // extract the phone number, removing non-numeric characters
                String phoneInput = binding.grf1Phone.getText().toString().replaceAll("[^0-9]","");
                if(phoneInput.length() !=10) {
                    binding.grf1Phone.setError("Enter a valid 10-digit number!");
                    isValid = false;
                }
                if(isValid) {
                    lastVisit = OffsetDateTime.now().toString();
                    doneButtonClicked = true;
                    saveToDatabase();
                    createConfirmedDialog();
                }else{
                    hasValidationErrors = true; //trigger error message dialog
                    createConfirmedDialog();
                }
            }
        });


        binding.buttonEditInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toggle the editability state
                isEditable = !isEditable;
                hideOrShow(isEditable);
            }
        });

        binding.buttonSaveInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isEditable = !isEditable;
                hideOrShow(isEditable);
                //flag to track if input fields are valid
                boolean isValid = true;

                // First form fragment fields
                fname = binding.grf1FName.getText().toString();
                if(fname == null || fname.isEmpty()){
                    //display error if name is invalid/empty
                    binding.grf1FName.setTextColor(Color.RED);
                    binding.grf1FName.setError("First name is required!");
                    isValid = false; // mark form invalid
                }else{ //reset any previous error message if the input is valid
                    binding.grf1FName.setTextColor(Color.BLACK);
                    binding.grf1FName.setError(null);
                }
                lname = binding.grf1LName.getText().toString();
                if(lname == null || lname.isEmpty()){
                    binding.grf1LName.setTextColor(Color.RED);
                    binding.grf1LName.setError("Last name is required!");
                    isValid = false;
                }else{
                    binding.grf1LName.setTextColor(Color.BLACK);
                    binding.grf1LName.setError(null);//reset if any error text exists
                }
                // Extract the phone number, removing non-numeric characters
                String phoneInput = binding.grf1Phone.getText().toString().replaceAll("[^0-9]","");
                if(phoneInput.length() != 10) {
                    binding.grf1Phone.setText(phoneInput);
                    binding.grf1Phone.setTextColor(Color.RED);
                    binding.grf1Phone.setError("Enter valid 10-digit number!");
                    isValid = false;
                }else {
                    binding.grf1Phone.setTextColor(Color.BLACK);
                    binding.grf1Phone.setError(null);
                    // change phone number into the correct format
                    phoneNum = phoneInput;
                    String formatPhoneNum = "(" + phoneNum.substring(0, 3) + ")-" +
                            phoneNum.substring(3,6) + "-" +
                            phoneNum.substring(6,10);
                    binding.grf1Phone.setText(formatPhoneNum);
                }

                nccId = binding.grf2NccId.getText().toString();

                //fragment 2
                streetAddress1 = binding.grf2Address1.getText().toString();
                streetAddress2 = binding.grf2Address1.getText().toString();
                city = binding.grf2City.getText().toString();
                state = binding.edittingGrf2State.getSelectedItem().toString();
                zip = binding.grf2Zip.getText().toString();
                affiliation = binding.edittingGrf2Affiliation.getSelectedItem().toString();
                age = binding.edittingGrf2Age.getSelectedItem().toString();
                gender = binding.edittingGrf2Gender.getSelectedItem().toString();

                //fragment 3
                dietary = binding.edittingGrf3Dietary.getSelectedItemsAsString();
                employment = binding.edittingGrf3StatusEmployment.getSelectedItemsAsString();
                health = binding.edittingGrf3StatusHealth.getSelectedItemsAsString();
                housing = binding.edittingGrf3StatusHousing.getSelectedItemsAsString();
                snap = binding.edittingGrf3Snap.getSelectedItem().toString();
                otherProg = binding.edittingGrf3OtherProgs.getSelectedItem().toString();
                income = binding.grf3Income.getText().toString();

                //fragment4
                householdNum = binding.edittingGrf4NumPeople.getSelectedItem().toString();
                children1 = binding.edittingGrf4Children1.getSelectedItem().toString();
                children5 = binding.edittingGrf4Children5.getSelectedItem().toString();
                children12 = binding.edittingGrf4Children12.getSelectedItem().toString();
                children18 = binding.edittingGrf4Children18.getSelectedItem().toString();

                // Barcode info
                barcode = binding.edittextEnterUpc.getText().toString();

                if(isValid){
                    updateTextViews();
                }else {
                    hasValidationErrors = true;
                    createConfirmedDialog();
                }
            }
        });







    }

    private void updateTextViews(){
        binding.grf1FName.setText(fname);
        binding.grf1LName.setText(lname);
        binding.grf1Phone.setText("(" + phoneNum.substring(0, 3) + ") " + phoneNum.substring(3, 6) + "-" + phoneNum.substring(6));

        binding.grf2Address1.setText(streetAddress1);
        //if street adress 2 was subbmitted then assign to street address 2
        if (streetAddress2 != null)
            binding.grf2Address2.setText(streetAddress2);
        //setting the rest of the summary fragment textviews
        binding.grf2City.setText(city);
        binding.grf2State.setText(state);
        binding.grf2Zip.setText(zip);
        binding.grf2Affiliation.setText(affiliation);
        binding.grf2Age.setText(age);
        binding.grf2Gender.setText(gender);
        binding.grf2NccId.setText(nccId);


        binding.grf3Dietary.setText(dietary.replaceAll(", ", "\n"));
        binding.grf3Snap.setText(snap);
        binding.grf3OtherProgs.setText(otherProg);
        binding.grf3StatusEmployment.setText(employment.replaceAll(", ", "\n"));
        binding.grf3StatusHealth.setText(health.replaceAll(", ", "\n"));
        binding.grf3StatusHousing.setText(housing.replaceAll(", ", "\n"));
        binding.grf3Income.setText(income);

        binding.grf4NumPeople.setText(householdNum);
        binding.grf4Children1.setText(children1);
        binding.grf4Children5.setText(children5);
        binding.grf4Children12.setText(children12);
        binding.grf4Children18.setText(children18);
    }

    private void initializeAndPreselectSpinner(Spinner spinner, int arrayResId, String savedValue) {
        // Create the adapter for the spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                requireContext(),
                arrayResId,
                android.R.layout.simple_spinner_item
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        // Preselect the item by iterating through the adapter
        if (savedValue != null) {
            for (int i = 0; i < adapter.getCount(); i++) {
                String item = adapter.getItem(i).toString();
                if (item.equalsIgnoreCase(savedValue.trim())) { // Case-insensitive matching
                    spinner.setSelection(i);
                    break;
                }
            }
        }

        Log.d(TAG+" Spinner Debug", "savedValue: " + savedValue);
        Log.d(TAG+" Spinner Debug", "Spinner Items: " + Arrays.toString(getResources().getStringArray(arrayResId)));

    }

    private void initializeMultiSelectSpinners(String dietary, String housing, String health, String employment) {
        // Bind MultiSelectSpinner views
        multiselectDietary = binding.edittingGrf3Dietary;
        multiselectEmployment = binding.edittingGrf3StatusEmployment;
        multiselectHealth = binding.edittingGrf3StatusHealth;
        multiselectHousing = binding.edittingGrf3StatusHousing;

        // Load items into MultiSelectSpinners, this gets the list of all the items
        multiselectDietary.setItems(getResources().getStringArray(R.array.dietary_needs));
        multiselectEmployment.setItems(getResources().getStringArray(R.array.employment_status));
        multiselectHealth.setItems(getResources().getStringArray(R.array.health_status));
        multiselectHousing.setItems(getResources().getStringArray(R.array.housing_status));


        //this section preselects the options the user selected before
        if (dietary != null && !dietary.isEmpty()) {
            String[] dietaryItems = dietary.split(",");
            // Log the dietary items that will be selected
            Log.d("MultiSelect", "Preselecting dietary items: " + Arrays.toString(dietaryItems));
            multiselectDietary.setSelection2(dietaryItems);
        } else {
            Log.d("MultiSelect", "No dietary items to preselect");
        }

        if (housing != null && !housing.isEmpty()) {
            String[] housingItems = housing.split(",");
            // Log the housing items that will be selected
            Log.d(TAG+ " MultiSelect", "Preselecting housing items: " + Arrays.toString(housingItems));
            multiselectHousing.setSelection2(housingItems);
        } else {
            Log.d(TAG+ " MultiSelect", "No housing items to preselect");
        }

        if (health != null && !health.isEmpty()) {
            String[] healthItems = health.split(",");
            // Log the health items that will be selected
            Log.d(TAG+ " MultiSelect", "Preselecting health items: " + Arrays.toString(healthItems));
            multiselectHealth.setSelection2(healthItems);
        } else {
            Log.d(TAG+ " MultiSelect", "No health items to preselect");
        }

        if (employment != null && !employment.isEmpty()) {
            String[] employmentItems = employment.split(",");
            // Log the employment items that will be selected
            Log.d(TAG+ " MultiSelect", "Preselecting employment items: " + Arrays.toString(employmentItems));
            multiselectEmployment.setSelection2(employmentItems);
        } else {
            Log.d(TAG+ " MultiSelect", "No employment items to preselect");
        }

    }

    /*
   editable starts as false when fragment is created
   when edit is clicked, editable becomes true
   when save is clicked, editable becomes false

   for things you want to happen only when edit is clicked, check editable for values true
   for things you want to happen only when save is clicked, check editable for values false

     */
    private void hideOrShow(boolean editable) {
        // Set the EditText fields to be editable or not based on the flag
        EditText[] editTexts = {
                binding.grf1FName,
                binding.grf1LName,
                binding.grf1Phone,

                binding.grf2Address1,
                binding.grf2Address2,

                binding.grf2City,
                 //
                binding.grf2NccId,
                 //
                binding.grf2Zip,



                binding.edittextEnterUpc
        };

        TextView[] editTextViews = {
                binding.grf2Affiliation, //
                binding.grf2Age, //
                binding.grf2Gender,
                binding.grf2State,
                //Need to change
                binding.grf3Dietary,  //
                binding.grf3Income,
                binding.grf3OtherProgs,
                binding.grf3Snap,
                binding.grf3StatusEmployment,
                binding.grf3StatusHealth,
                binding.grf3StatusHousing, //

                binding.grf4Children1,//
                binding.grf4Children5,
                binding.grf4Children12,
                binding.grf4Children18,
                binding.grf4NumPeople//
        };
        if(editable==true){
            for(TextView text :editTextViews){
                text.setVisibility(View.GONE);
            }
        }
        else{
            for(TextView text :editTextViews){
                text.setVisibility(View.VISIBLE);
            }
        }

        MultiSelectSpinner[] multiSelectSpinners = {
                binding.edittingGrf3Dietary,
                binding.edittingGrf3StatusEmployment,
                binding.edittingGrf3StatusHealth,
                binding.edittingGrf3StatusHousing,

        };

        Spinner[] spinners = {
                binding.edittingGrf2Affiliation,
                binding.edittingGrf2Age,
                binding.edittingGrf2Gender,
                binding.edittingGrf2State,
                binding.edittingGrf3OtherProgs,
                binding.edittingGrf3Snap,
                binding.edittingGrf4Children1,
                binding.edittingGrf4Children5,
                binding.edittingGrf4Children12,
                binding.edittingGrf4Children18,
                binding.edittingGrf4NumPeople
        };
        if(editable==true){
            for(MultiSelectSpinner multi :multiSelectSpinners){
                multi.setVisibility(View.VISIBLE);
            }
            for(Spinner select :spinners){
                select.setVisibility(View.VISIBLE);
            }
        }
        else {
            for (MultiSelectSpinner multi : multiSelectSpinners) {
                multi.setVisibility(View.GONE);
            }
            for (Spinner select : spinners) {
                select.setVisibility(View.GONE);
            }
        }

        // Set all EditText fields to be editable or not based on the flag
        for (EditText editText : editTexts) {
            editText.setFocusable(editable);
            editText.setFocusableInTouchMode(editable);
            editText.setClickable(editable);
        }

        // Update the button visibility based on the editability state
        if (editable) {
            binding.buttonConfirmRegistration.setVisibility(View.GONE);
            binding.buttonEditInfo.setVisibility(View.GONE);
            binding.buttonSaveInfo.setVisibility(View.VISIBLE);
        } else {
            binding.buttonConfirmRegistration.setVisibility(View.VISIBLE);
            binding.buttonEditInfo.setVisibility(View.VISIBLE);
            binding.buttonSaveInfo.setVisibility(View.GONE);
        }


    }

    /**
     * home method --
     * description: this method goes to the nest home screen
     */
    public void home() {
        // sending to guestform testing so they can login. Otherwise switch to "Choose.class" to go to app home.
        Intent intent = new Intent(getActivity(), GuestFormTesting.class);
        startActivity(intent);
    }

    public boolean createConfirmedDialog() {
        //creates the dialog prevents it from being canceled
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);

        //sets the title
        builder.setTitle("Thank you!");

        //sets the main text accordingly
        if (doneButtonClicked)
            builder.setMessage("Your registration has been confirmed. " +
                    "You will now be taken back to the Home Page. " +
                    "You can now log in and start your first visit!");
        else if (hasValidationErrors){
                builder.setTitle("Incomplete Registration");
                builder.setMessage("Please edit all required fields. " +
                        "Your registration cannot be confirmed or saved until all necessary information is provided.");
        }else if (regButtonClicked) {
            builder.setMessage("Your registration has been confirmed. " +
                    "You will now be taken back to the Registration Page.");
        }else
            builder.setMessage("Your registration has been confirmed. " +
                    "You will now be taken to the Check-in Page. " +
                    "You can now log in and start your first visit!");

        // close button
        builder.setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //close the dialog
                dialog.dismiss();

                if (doneButtonClicked) { //if the "done" button is clicked send user to home screen
                    getActivity().finish();
                    launchHome();
                    Log.d(TAG, "onClick: done button clicked");
                } else if (hasValidationErrors) { //if a submission button is clicked with invalid entries
                    hasValidationErrors = false;
                    Log.d(TAG, "onClick: confirm registration button clicked with invalid entries");
                } else if (regButtonClicked) { //if "Registration button clicked send user to registration
                    getActivity().finish();
                    launchGuestReg();
                    Log.d(TAG, "onClick: reg button clicked");
                } else if (checkInButtonClicked) { //if "Check-in" button is clicked send user to Check in
                    getActivity().finish();
                    launchGuestVisit();
                    Log.d(TAG, "onClick: checkin button clicked");
                } else { //if something UNEXPECTED happens send user to home screen
                    getActivity().finish();
                    launchHome();
                    Log.d(TAG, "onClick:NOTHING clicked");
                }
            }
        });
        //opens the dialog
        builder.show();
        return true;
    }

    /**
     * launchGuestVisit - starts the GuestVisitActivity activity
     */
    public void launchGuestVisit() {
        Intent intent = new Intent(requireActivity(), GuestVisitActivity.class);
        startActivity(intent);
    }

    /**
     * launchGuestDatabaseRegistrationActivity - starts the GuestDatabaseRegistrationActivity activity
     */
    public void launchGuestReg() {
        Intent intent = new Intent(requireActivity(), GuestDatabaseRegistrationActivity.class);
        startActivity(intent);
    }

    public void launchHome() {
        Intent intent = new Intent(requireActivity(), Choose.class);
        startActivity(intent);
    }

    /**
     * launchGetUPC - starts the Get UPC activity
     */
    public void launchGetUPC() {
        Intent intent = new Intent(requireActivity(), CheckExpirationDateActivity.class);
        startActivity(intent);
    }

    /**
     * Translates a value from Spanish to English using the English context and resource array.
     */
    private String translateToEnglish(Context englishContext, int arrayResId, String value) {
        String[] englishArray = englishContext.getResources().getStringArray(arrayResId);
        String[] spanishArray = getResources().getStringArray(arrayResId);

        // Find the index of the Spanish value
        for (int i = 0; i < spanishArray.length; i++) {
            if (spanishArray[i].equals(value)) {
                return englishArray[i]; // Return the corresponding English value
            }
        }

        // If not found, return the original value (fallback)
        return value;
    }


    /**
     * saveToDatabase- Saves to which ever database it's able to connect to
     */
    public void saveToDatabase() {
        // Create an English context
        Configuration config = getResources().getConfiguration();
        config.setLocale(new Locale("en"));
        Context englishContext = getContext().createConfigurationContext(config);

        // Translate conflicting attributes to English
        dietary = translateToEnglish(englishContext, R.array.dietary_needs, dietary);
        employment = translateToEnglish(englishContext, R.array.employment_status, employment);
        health = translateToEnglish(englishContext, R.array.health_status, health);
        housing = translateToEnglish(englishContext, R.array.housing_status, housing);
        otherProg = translateToEnglish(englishContext, R.array.yes_no_select_one, otherProg);
        snap = translateToEnglish(englishContext, R.array.yes_no_select_one, snap);
        affiliation = translateToEnglish(englishContext, R.array.affiliation_array, affiliation);
        age = translateToEnglish(englishContext, R.array.age_array, age);
        gender = translateToEnglish(englishContext, R.array.gender_array, gender);

        OffsetDateTime todaysDate = OffsetDateTime.now();
        lastVisit = todaysDate.toString();
        Log.d(TAG, "todays date: " + lastVisit);

        // Connecting to database
        if (db.isConnected()) {
            db.insertData(fname + " " + lname, phoneNum, nccId, streetAddress1 + ", " + streetAddress2,
                    city, zip, state, affiliation, age, gender, dietary, otherProg, snap, employment, health, housing,
                    income, householdNum, children1, children5, children12, children18,
                    barcode, lastVisit);
        } else {
            // Opens Local Database
            GuestRegistryHelper localRegDatabase = new GuestRegistryHelper(getContext());

            // Adds to local database
            boolean addedData = localRegDatabase.addToLocalGRDataBase(fname + " " + lname, phoneNum, nccId, streetAddress1 + ", " + streetAddress2,
                    city, zip, state, affiliation, age, gender, dietary, otherProg, snap, employment, health, housing,
                    income, householdNum, children1, children5, children12, children18, barcode, lastVisit);

            // Checks if the data was added successfully or not
            if (addedData)
                Log.d(TAG, "data successfully added to LOCAL DATABASE");
            else
                Log.d(TAG, "onViewCreated: data --NOT-- added to LOCAL DATABASE!");
        }
    }
}