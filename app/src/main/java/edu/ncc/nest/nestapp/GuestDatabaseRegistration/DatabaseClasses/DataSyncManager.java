package edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.widget.Toast;

import java.time.OffsetDateTime;

import edu.ncc.nest.nestapp.R;


public class DataSyncManager {

    public Context context; // Context from an activity with access to shared preferences. Most likely will be the context from Choose.java
    private GuestRegistrySource remoteDB; // Remote database
    private GuestRegistryHelper localDB; // Local database



    public static final String TAG = DataSyncManager.class.getSimpleName(); // Tag for logging

    public DataSyncManager(Context context) {
        this.context = context;



        try {
            remoteDB = new GuestRegistrySource(context);
            localDB = new GuestRegistryHelper(context);
        } catch (Exception exception) {
            Log.e(TAG, "Error initializing databases.", exception);
        }
    }

    /**
     * updateLastSync method -- Updates the last sync date stored in shared preferences.
     */
    public void updateLastSync() {
        try {
            SharedPreferences pref = context.getSharedPreferences(context.getString(R.string.syncDate), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();

            // Puts current time into shared preferences
            editor.putString(context.getString(R.string.syncDate), OffsetDateTime.now().toString());
            editor.apply();
            Log.d(TAG, "Updated the date in shared preferences.");
        } catch (Exception exception) {
            Log.e(TAG, "Error updating last sync date in shared preferences.", exception);
        }
    }


    public boolean isDataInLocal(){
        return localDB.isDataInLocal();
    }

    /**
     * syncToRemoteDb method -- Syncs the local and remote databases
     *
     * @return True if sync was successful, false if unsuccessful.
     */
    public boolean syncToRemoteDb() {
        Log.d("DataSyncManager", "Begin syncing...");

        boolean didSync = true;

        // Updates the date saved in shared preferences if no data exists in local database
        if (!localDB.isDataInLocal()) {
            Log.d(TAG, "No data in local database");
            Toast.makeText(this.context.getApplicationContext(), this.context.getString(R.string.empty_local),Toast.LENGTH_SHORT).show();
            updateLastSync();
            return true;
        }

        if (remoteDB.isConnected()) {

            Log.d(TAG, "Connected to remote database");

            // Retrieve entries from local database
            SQLiteDatabase SqlDb = null;
            Cursor cursor = null;
            try {

                /*
                Questman.syncToRemoteQb(); // tries to sync with localDB along side GuestRegistry
                 */
                SqlDb = localDB.getWritableDatabase();
                cursor = SqlDb.rawQuery("SELECT * FROM NESTGuestRegistry", null);
                cursor.moveToFirst();

                while (!cursor.isAfterLast()) {
                    // values in this entry
                    String name = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.NAME));
                    String phone = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.PHONE));
                    String nccID = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.NCC_ID));
                    String address = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.ADDRESS));
                    String city = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.CITY));
                    String zipcode = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.ZIP));
                    String state = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.STATE));
                    String affiliation = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.AFFILIATION));
                    String age = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.AGE));
                    String gender = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.GENDER));
                    String diet = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.DIET));
                    String programs = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.PROGRAMS));
                    String snap = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.SNAP));
                    String employment = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.EMPLOYMENT));
                    String health = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.HEALTH));
                    String housing = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.HOUSING));
                    String income = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.INCOME));
                    String householdNum = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.HOUSEHOLD_NUM));
                    String children1 = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.CHILDREN_1));
                    String children5 = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.CHILDREN_5));
                    String children12 = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.CHILDREN_12));
                    String children18 = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.CHILDREN_18));
                    String barcode = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.BARCODE));
                    String lastVisit = cursor.getString(cursor.getColumnIndex(GuestRegistryHelper.LASTVISIT));

                    // Checks if record already exists in the remote database
                    if (remoteDB.doesExist(phone)) {
                        Log.d(TAG, "Entry with id " + cursor.getString(0) + "already exists in the remote database! Removing entry from the local database.");
                        localDB.deleteTableRow(cursor.getInt(0)); // Deletes if already exists in remote
                    } else if (remoteDB.insertData(name, phone, nccID, address, city,
                            zipcode, state, affiliation, age, gender, diet,
                            programs, snap, employment, health, housing, income,
                            householdNum, children1, children5, children12, children18, barcode, lastVisit)) {
                        // If not in remote, inserts and removes current record from local database
                        Log.d(TAG, "Synced entry with id " + cursor.getString(0) + " and removed it from local.");
                        localDB.deleteTableRow(cursor.getInt(0));
                    } else {
                        didSync = false; // If even one item doesn't sync, this whole method will return false
                        Log.e(TAG, "Error inserting to remote database for some reason :(");
                    }
                    // Moves to next record regardless of success or not. Prevents infinite loops.
                    cursor.moveToNext();
                }
            } catch (SQLiteException exception) {
                Log.e(TAG, "Error while syncing databases.", exception);
                didSync = false;
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
                if (SqlDb != null) {
                    SqlDb.close();
                }
            }
        } else {
            didSync = false; // Returns false if the database wasn't able to connect
            Log.d(TAG, "Unable to connect to remote database.");
        }

        String message;
        if (didSync){
            message = this.context.getString(R.string.sync_success);
        } else{
            message = this.context.getString(R.string.sync_failed);
        }
        Toast.makeText(this.context.getApplicationContext(), message,Toast.LENGTH_SHORT).show();
        Log.d(TAG,"Sync status: " + didSync);

        // Updates the last synced text in shared preferences if sync was successful
        if (didSync)
            updateLastSync();
        return didSync;
    }
}
