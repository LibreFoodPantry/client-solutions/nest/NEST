/**
 *
 * Copyright (C) 2023 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package edu.ncc.nest.nestapp.GuestVisit.Fragments;

import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.fragment.NavHostFragment;

//import java.util.concurrent.ExecutionException;

import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper;
//import edu.ncc.nest.nestapp.CheckExpirationDate.Fragments.StartFragment;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistrySource;
import edu.ncc.nest.nestapp.R;

public class ManualFragment extends Fragment {

    public static final String TAG = ManualFragment.class.getSimpleName();
    String barcode, phone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_guest_visit_manual_entry, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) { //Recovering after a screen rotation
        super.onViewCreated(view, savedInstanceState);

        Log.d(TAG, "In Manual Fragment");

        // Create an instance of the database helper
        GuestRegistrySource db = new GuestRegistrySource(requireContext());

        // Functioning barcode: GHI-9012
        /*"John Doe", "123-456-7890", "N00123456",
                    "01-01-9999", "123 Simple Ave", "Nothingtown", "1234", "NY",
                    "NY", null, null, null, null, null,
                    null, null, null, null, null, null,
                    null, null, null, null,null, null, "GHI-9012");
         */



        ( view.findViewById(R.id.manual_submit_btn)).setOnClickListener(view1 -> {

            //Saving the information in the EditText views
            barcode = String.valueOf(((EditText) view.findViewById(R.id.guest_visit_barcode_entry)).getText());
            Log.d(TAG, "Barcode : " + barcode);

            //Up for debate
            phone = String.valueOf(((EditText) view.findViewById(R.id.guest_visit_msie_pt2)).getText());
            Log.d(TAG, "Phone : " + phone);

            Bundle entryResults = new Bundle();
            // this String determines whether the user is in the database or not. if this string is assigned null, the user is not in the database
            boolean registered = false;
            
            // if the user enters no input, a Toast is displayed to tell the user to enter sign in information
            if (barcode.equals("") && phone.equals("")) {
                Toast.makeText(ManualFragment.this.getContext(), R.string.no_input_detected_manual_sign_in, Toast.LENGTH_SHORT).show();
                // if the user doesn't sign in using a barcode and leaves the id or name field empty, a Toast is displayed telling them to enter both name and id
            } else if (!db.isConnected()) {
                // If the device isn't connected to the database, display a small toast informing the user to check their connection.
                Toast.makeText(ManualFragment.this.getContext(), R.string.check_connection_manual_sign_in, Toast.LENGTH_LONG).show();
                db.close(); // Closing db to prevent unwanted behavior

                //Opens up local database to store data. Users without barcodes are sent to local registration
                GuestRegistryHelper localRegDatabase = new GuestRegistryHelper(getContext());

                //searches for barcode in local database
                if (!barcode.equals("")) {
                    String registeredData;
                    // put entered barcode into the bundle
                    entryResults.putString("barcode", barcode);
                    try {
                        // isRegistered returns an string that contains the phone number
                        // associated with the barcode
                        registeredData = localRegDatabase.isRegistered(barcode);

                        if (registeredData != null) {
                            Log.d(TAG, "isRegistered returned " + registeredData);
                            registered = true;
                            entryResults.putString("phone", registeredData);
                        } else {
                            //data not found in local database, send to registration
                        }
                    } catch (Exception e) {
                        Log.d(TAG, "Error pulling from database.");
                    }

                }

                // the result of put into the bundle
                entryResults.putBoolean("IS_REGISTERED", registered);
                // give ConfirmationFragment access to the bundle and navigate to that fragment
                getParentFragmentManager().setFragmentResult("SCAN_CONFIRMED", entryResults);
                NavHostFragment.findNavController(ManualFragment.this)
                        .navigate(R.id.action_GV_ManualFragment_to_ConfirmationFragment);
            } else {
                // barcode takes precedence when signing in. If the user enters both a barcode and
                // a number, the barcode code will be executed first.
                if (!barcode.equals("")) {
                    String registeredData;
                    // put entered barcode into the bundle
                    entryResults.putString("barcode", barcode);
                    try {
                        // isRegistered returns an string that contains the phone number
                        // associated with the barcode
                        registeredData = db.isRegistered(barcode);

                        if (registeredData != null) {
                            Log.d(TAG, "isRegistered returned " + registeredData);
                            registered = true;
                            entryResults.putString("phone", registeredData);
                        }
                    } catch (Exception e) {
                        Log.d(TAG, "Error pulling from database.");
                    }

                } else { // Use phone number
                    // put the entered phone into the bundle
                    entryResults.putString("phone", phone);
                    try {
                        String[] userInfo = db.isRegisteredPhone(phone);
                        if(userInfo[1]!= null){
                            Log.d(TAG, "Barcode after phone# " + userInfo[1]);

                            registered = true;
                            entryResults.putString("barcode",userInfo[1]);

                        }


                    }catch (Exception e) {
                        Log.d(TAG, "Error pulling from database.");
                    }
                }

                // the result of put into the bundle
                entryResults.putBoolean("IS_REGISTERED", registered);
                // give ConfirmationFragment access to the bundle and navigate to that fragment
                getParentFragmentManager().setFragmentResult("SCAN_CONFIRMED", entryResults);
                NavHostFragment.findNavController(ManualFragment.this)
                        .navigate(R.id.action_GV_ManualFragment_to_ConfirmationFragment);

            }
        });
    }
}
