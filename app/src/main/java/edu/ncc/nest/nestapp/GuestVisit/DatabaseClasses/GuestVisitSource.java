package edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses;

/**
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import static edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper.TABLE_NAME;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.SimpleDateFormat;
import android.util.Log;

import androidx.annotation.NonNull;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import edu.ncc.nest.nestapp.BuildConfig;

/**
 * GuestVisitSource: This class finds and inserts data from and to the
 * {@link GuestVisitHelper} class / database.
 */
public class GuestVisitSource {

    public static final String TAG = GuestVisitSource.class.getSimpleName();



    private final GuestVisitHelper guestHelper;
    private SQLiteDatabase writableDatabase;
    private SQLiteDatabase readableDatabase;
    private String[] counterStr = {GuestVisitHelper.ROW_ID, GuestVisitHelper.GUEST_ID, GuestVisitHelper.ADULT_COUNT,
    GuestVisitHelper.SENIOR_COUNT, GuestVisitHelper.CHILD_COUNT, GuestVisitHelper.FIRST_VISIT, GuestVisitHelper.DATE, GuestVisitHelper.VISIT_COUNTER};

    private static final String USERNAME = BuildConfig.GUEST_REGISTRY_DB_USERNAME; // Username for the database
    private static final String PASSWORD = BuildConfig.GUEST_REGISTRY_DB_PASSWORD; // Password for the database
    private static final String DRIVER = "jdbc:mysql://";                          // Driver that is used to connect to the database
    private static final String DB_NAME = "GuestRegistry";                         // Name of the remote DB
    private static final String SERVER = "stargate.ncc.edu:3306/";                 // Name of the server that the DB is hosted on
    private final String URI = DRIVER + SERVER + DB_NAME;                          // URI to communicate with the database
    private final String TABLE_NAME = "NestGuestVisit";                            // Name of the remote table in GuestRegistry


    /************ Constructor ************/

    public GuestVisitSource(Context context) {
        guestHelper = new GuestVisitHelper(context);

        writableDatabase = guestHelper.getWritableDatabase();
        readableDatabase = guestHelper.getReadableDatabase();


    }

    /************ Custom Methods Start ************/

    /**
     * open --
     * Opens the database
     * @return Returns a reference to this GuestVisitSource object
     */
    public GuestVisitSource open() throws SQLException {

        writableDatabase = guestHelper.getWritableDatabase();

        readableDatabase = guestHelper.getReadableDatabase();

        return this;

    }

    /**
     * close --
     * Closes the database
     */
    public void close() {

        writableDatabase.close();

        readableDatabase.close();

    }

    public void clearData() {
        writableDatabase.delete(GuestVisitHelper.TABLE_NAME, null, null);
        readableDatabase.delete(GuestVisitHelper.TABLE_NAME, null, null);
    }

    /**
     * submitQuestionnaire --
     * Submits answers by guest (guestID) into the database
     * @param guestID The ID of the guest the submission belongs to
     * @return Whether or not there was an error submitting the questionnaire
     */
    public long submitQuestionnaire(@NonNull String guestID, @NonNull String adultCount, @NonNull String seniorCount,
                                    @NonNull String childCount, @NonNull String firstVisit, @NonNull String visitCounter,@NonNull String date) {

        ContentValues submissionValues = new ContentValues();

        //Setup the data and time for when the Questionnaire was submitted
        DateTimeFormatter frmt = DateTimeFormatter.ofPattern("MMddyyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String currentTime = frmt.format(now);
        //todo testing for wuery methods added a date parameter.
        // Put the guest's id into cValues
        submissionValues.put(GuestVisitHelper.GUEST_ID, guestID);
        submissionValues.put(GuestVisitHelper.ADULT_COUNT, adultCount);
        submissionValues.put(GuestVisitHelper.SENIOR_COUNT, seniorCount);
        submissionValues.put(GuestVisitHelper.CHILD_COUNT, childCount);
        submissionValues.put(GuestVisitHelper.FIRST_VISIT, firstVisit);
        submissionValues.put(GuestVisitHelper.DATE, date);
        submissionValues.put(GuestVisitHelper.VISIT_COUNTER, visitCounter);

        // Insert the submission in the database and return the row id it was stored at
        return (writableDatabase.insert(GuestVisitHelper.TABLE_NAME, null, submissionValues));

    }

    /**
     * insertData method --
     * Takes the data passed as arguments and inserts it into the database appropriately.
     * If the data is inserted without issue, the value for the row ID will be returned,
     * and in turn the method will return a boolean value of true. If an issue does occur,
     * -1 will be returned and the method will return a boolean value of false.
     *
     * @param
     * @return true if the data has been inserted without issue, false otherwise
     * Credit to Bryan Granda
     */
    public boolean insertData(@NonNull String guestID, @NonNull String adultCount, @NonNull String seniorCount,
                              @NonNull String childCount, @NonNull String firstVisit, @NonNull String date, @NonNull String visitCounter) {

        // Creates an ExecutorService that uses a single worker thread operating off an unbounded queue.
        ExecutorService executor = Executors.newSingleThreadExecutor();

        AtomicBoolean didInsert = new AtomicBoolean(false);

        // Executes the thread that will insert the data into the remote db
        executor.execute(() -> {

            //Try to establish a connection to the db
            try (Connection connection = DriverManager.getConnection(URI, USERNAME, PASSWORD)) {

                //Compiles the sql statement
                PreparedStatement statement = connection.prepareStatement("INSERT INTO " + TABLE_NAME + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

                // sets the values for the statement
                statement.setString(1, null);
                statement.setString(2, guestID);
                statement.setString(3, adultCount);
                statement.setString(4, seniorCount);
                statement.setString(5, childCount);
                statement.setString(6, firstVisit);
                statement.setString(7, date);
                statement.setString(8, visitCounter);

                // Execute the statement
                statement.executeUpdate();
                didInsert.set(true);
            } catch (java.sql.SQLException e) {
                Log.e(TAG, "Error inserting data into database", e);
            }
        });

        // Shuts down the executor service so that it no longer accepts new tasks
        executor.shutdown();

        // Waits for executor to finish before returning from this method.
        // Otherwise, this method will always return false
        try {
            executor.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        if(didInsert.get())
            Log.d(TAG, "Inserted into remote");

        // Insert method will return a negative 1 if there was an error with the insert
        return didInsert.get();

    }


    /**
     * findSubmissions --
     * Finds submissions by a guest ( guestID ) and adds
     * them to a list of submissions.
     * @param guestID The ID to search for
     * @return The list of submissions
     */
    /*
    public List<GuestVisitSubmission> findSubmissions(@NonNull String guestID) {

        List<GuestVisitSubmission> submissions = new ArrayList<>();

        // Get all the guest records from the table (TABLE_NAME) who's field name (GUEST_ID) matches the field value (?).
        String sqlQuery = "SELECT * FROM " + GuestVisitHelper.TABLE_NAME +
                " WHERE " + GuestVisitHelper.GUEST_ID + " = ?";

        // Run the SQL query from above and replace the '?' character with the respective argument stored in the String[] array
        Cursor cursor = readableDatabase.rawQuery(sqlQuery, new String[] {guestID});

        // Loop through each cursor, convert them to submissions, and add it to the list
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())

            submissions.add(convertCursorToSubmission(cursor));

        // Make sure we close the cursor since we don't need it anymore
        cursor.close();

        return submissions;

    }

    */

    /*
    /**
     * printSubmissions --
     * Searches through the database for questionnaires
     * submitted by a guest ( guestID ). Prints each
     * questionnaire submitted by the guest.
     * @param guestID The ID of the guest we're getting the submissions of
     */
    public void printSubmissions(@NonNull String guestID) {

        Log.i(TAG, "Printing all submissions by guest " + guestID + " from " + GuestVisitHelper.DATABASE_NAME + ":");

        // Find each questionnaire submitted by the guest and print it to the log

    }
    /**
     * convertCursorToSubmission --
     * Converts a Cursor object to a GuestVisitSubmission object.
     * @param c The Cursor to convert
     * @return Returns the result of the conversion
     */
    private GuestVisitSubmission convertCursorToSubmission(Cursor c) {
        GuestVisitSubmission entry = new GuestVisitSubmission(c.getLong(0),
                c.getString(1),
                c.getString(2),
                c.getString(3),
                c.getString(4),
                c.getString(5),
                c.getString(6),
                c.getString(7));

        return entry;
    }

    /**
     * Finds the number of times the guest has visited
     * @param barcode the guest to find
     * @return the number of times this guest has visited as a string
     */
    public String getVisitCount(String barcode){
        try {
            GuestVisitSubmission entry;
            Cursor cursor = readableDatabase.query(guestHelper.TABLE_NAME,
                    counterStr,
                    guestHelper.GUEST_ID + " = '" + barcode + "' ",
                    null,
                    null,
                    null,
                    guestHelper.VISIT_COUNTER + " DESC ");
            Log.d("**CURSOR CHECK**", cursor.toString());
            cursor.moveToFirst();
            entry = convertCursorToSubmission(cursor);
            Log.d("**CURSOR CHECK**", entry.VISIT_COUTNER);
            return entry.VISIT_COUTNER;
        } catch(CursorIndexOutOfBoundsException e) {
            return "0";
        }
    }

    ////////////// Methods for use with the User Reports //////////////
    /**
     * getTotalVisitors - Counts the total number of visitors in a given month

     * @param month - the month you wish to check
     * @param year - the year you wish to check
     * @return - The number of visitors in the given month
     */
    public int getTotalVisitors(int month, int year) {
        String queryArgument = guestHelper.monthFormat.format(month) + "__" + year + "%";
        // Retrieve all table entries from given month
        Cursor c = readableDatabase.query(
                guestHelper.TABLE_NAME,
                new String[]{guestHelper.ROW_ID},
                guestHelper.DATE + " LIKE ? ",
                new String[]{queryArgument},
                null,
                null,
                null
        );
        // Count the entries
        int rowCount = c.getCount();
        // Close the database link
        c.close();
        // return the number of entries
        return rowCount;
    }

    /**
     * getTotalServed - Counts the total number of people served in a given month based on the
     * visitor's family size
     * @param month - The month you wish to check
     * @param year - The year you wish to check
     * @return - The number of people served in the given month
     */
    public int getTotalServed(int month, int year) {
        int servedCount = 0;
        String queryArgument = guestHelper.monthFormat.format(month) + "__" + year + "%";
        // Retrieve all table entries from given month
        Cursor c = readableDatabase.query(
                guestHelper.TABLE_NAME,
                new String[]{guestHelper.ADULT_COUNT, guestHelper.SENIOR_COUNT,
                        guestHelper.CHILD_COUNT},
                guestHelper.DATE + " LIKE ? ",
                new String[]{queryArgument},
                null,
                null,
                null
        );
        // Count the entries
        while (c.moveToNext()) {
            servedCount += c.getInt(0) + c.getInt(1) + c.getInt(2);
        }
        // Close the database link
        c.close();
        // return the number of entries
        return servedCount;
    }

    /**
     * getTotalServedByAge - Counts the number of people served within a given age range and month
     * based on the visitors' family size
     * @param month - The month you wish to check
     * @param year - The year you wish to check
     * @param age - Use GuestVisitHelper.AGE_RANGE enum for this value. Specifies the age range you
     *            wish to search within
     * @return - The number of people served in the given month and age range
     */
    public int getTotalServedByAge(int month, int year,int age) {
        int servedCount = 0;
        Cursor c;
        String ageRange;
        String queryArgument = guestHelper.monthFormat.format(month) + "__" + year + "%";
        // Sets which column to pull up based on the given AGE_RANGE variable
        switch(age) {
            case 0:
               ageRange = guestHelper.CHILD_COUNT;
                break;
            case 1:
                ageRange = guestHelper.ADULT_COUNT;

                break;
            case 2:
                ageRange =guestHelper.SENIOR_COUNT;
                break;
            default:
                ageRange = "";
        }
        // Pulls the necessary data from the database
        c = readableDatabase.query(
                guestHelper.TABLE_NAME,
                new String[]{ageRange},
                guestHelper.DATE + " LIKE ?",
                new String[]{queryArgument},
                null,
                null,
                null
        );
        // Count the entries
        while (c.moveToNext())
            servedCount += c.getInt(0);
        // Close the database link
        c.close();
        // return the number of entries
        return servedCount;
    }

    /**
     * getTotalPersonVisits - Counts the number of times a specific guest has visited in a given
     * month
     * @param month - The month you wish to check
     * @param year - the year you wish to check
     * @param guestId - The ID of the guest you wish to check
     * @return - the number of times a specific guest has visited in the given month
     */
    public int getTotalPersonVisits(int month, int year, String guestId) {
        int visitCount = 0;
        String queryArgument = guestHelper.monthFormat.format(month) + "__" + year + "%";
        // Pulls the necessary data from the database
        Cursor c = readableDatabase.query(
                guestHelper.TABLE_NAME,
                new String[]{guestHelper.VISIT_COUNTER},
                guestHelper.DATE + " LIKE ? AND " + guestHelper.GUEST_ID + " = ?",
                new String[]{queryArgument, guestId},
                null,
                null,
                null
        );
        // Count the entries
        while (c.moveToNext())
            visitCount++;
        // Close the database link
        c.close();
        // return the number of entries
        return visitCount;
    }

    /**
     * isConnected checks to see if the app is currently connected to the database.
     * Newer phones seem to hang if the database is called and there is none. After a specified time,
     * false is returned.
     *
     * @return true if connected, false if a connection couldn't be found.
     * Credit to Christopher Chase
     */
    public boolean isConnected() {
        //This line of code helps improve performance, as older machines such as the pixel 2
        // hang a little longer.
        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        try {
            CompletableFuture<Boolean> future = CompletableFuture.supplyAsync(() -> {
                try {
                    DriverManager.setLoginTimeout(1); // Set a timeout for the connection attempt (e.g., 1 second)
                    try (Connection connection = DriverManager.getConnection(URI, USERNAME, PASSWORD)) {
                        Log.d(TAG, "Connected to remote database!");
                        return true;
                    }
                } catch (java.sql.SQLException e) {
                    Log.d(TAG, "Not connected to remote database!");
                    return false;
                }
            }, executor);

            return future.get(1, TimeUnit.SECONDS); // Wait for the CompletableFuture to complete within 1 second and return the result
        } catch (TimeoutException e) {
            return false; // Return false if the CompletableFuture takes more than 3 seconds to complete
        } catch (Exception e) {
            return false;
        } finally {
            executor.shutdown(); // Remember to shut down the executor to release resources
        }
    }







}
