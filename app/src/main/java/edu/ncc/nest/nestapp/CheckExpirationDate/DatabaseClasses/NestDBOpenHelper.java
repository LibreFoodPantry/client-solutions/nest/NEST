package edu.ncc.nest.nestapp.CheckExpirationDate.DatabaseClasses;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Looper;
import android.util.JsonReader;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.common.util.Strings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.OptionalInt;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.net.ssl.HttpsURLConnection;

import edu.ncc.nest.nestapp.async.BackgroundTask;
import edu.ncc.nest.nestapp.async.TaskHelper;

/**
 *

 Copyright (C) 2020 The LibreFoodPantry Developers.


 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.


 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.


 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.
 */

/**
 * This class uses the Singleton pattern to ensure only one instance of it
 * is used throughout the application.  This allows multiple threads to access
 * the database while automatically providing synchronization of database
 * operations (which is built into SQLiteopenHelper).  To use this class,
 * call its static getInstance() method.
 */
public class NestDBOpenHelper extends SQLiteOpenHelper {

    public static final String LOG_TAG = NestDBDataSource.class.getSimpleName();

    // Whether or not to print executed sql statements to the log.
    public static final boolean DEBUG_SQL = false;

    private static int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Nest.db";
    private static final String DATABASE_CREATE_SQL = "NestDB.Create.sql";
    private static final String DATABASE_DESTROY_SQL = "NestDB.Destroy.sql";
    private Context mContext;

    // keep static reference to single instance of this class (Singleton pattern)
    @SuppressLint("StaticFieldLeak")  // we use Application Context only (no memory leak)
    private static NestDBOpenHelper mInstance = null;

    // getInstance static factory method (Singleton pattern)
    public static NestDBOpenHelper getInstance(Context context, int version) {

        if (mInstance == null) {

            DATABASE_VERSION = version;

            mInstance = new NestDBOpenHelper(context.getApplicationContext(), version);
        }
        return mInstance;

    }

    /**
     * Default constructor is private to prevent direct instantiation,
     * use the getInstance() factory method instead (Singleton pattern)
     * @param context application context
     * @param databaseVersion
     */
    private NestDBOpenHelper(Context context, int databaseVersion) {

        super(context, DATABASE_NAME, null, databaseVersion);

        mContext = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        executeSqlFile(db, DATABASE_CREATE_SQL, DATABASE_NAME + " creation failed");

        populateFromFoodKeeperAPI(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        executeSqlFile(db, DATABASE_DESTROY_SQL,
                DATABASE_NAME + " upgrade preparation failed");

        onCreate(db);

    }

    private void executeSqlFile(SQLiteDatabase db, String fileName, String errorMsg) {

        db.beginTransaction();

        try (Scanner scan = new Scanner(mContext.getAssets().open(fileName))) {

            // Everything up to the next semicolon is the next statement
            for (scan.useDelimiter(";"); scan.hasNext();) {

                String sql = scan.next().trim();

                // Avoid trying to execute empty statements (execSQL doesn't like that)
                if (!sql.isEmpty()) {

                    if (DEBUG_SQL)

                        Log.d(LOG_TAG, "sql statement is:\n" + sql);

                    db.execSQL(sql);

                } else if (DEBUG_SQL)

                    Log.d(LOG_TAG, "sql statement is empty");

            }

            db.setTransactionSuccessful();

        } catch (Exception e) {

            throw new RuntimeException(errorMsg + ": " + e.getMessage(), e);

        } finally {

            db.endTransaction();

        }

    }

    private void populateFromFoodKeeperAPI(SQLiteDatabase db) {

        if (Looper.getMainLooper().isCurrentThread()) {

            Log.w(LOG_TAG, "Populating database from main thread may cause UI to freeze.");

            Toast.makeText(mContext, "Warning: Populating database from main thread",
                    Toast.LENGTH_LONG).show();

        }

        TaskHelper taskHelper = new TaskHelper();

        try {

            ArrayList<Future<?>> futures = new ArrayList<>();

            futures.add(taskHelper.submit(new GetDataTask(db)));

            // futures.add(taskHelper.submit(new GetCookingTipsTask(db)));

            // futures.add(taskHelper.submit(new GetCookingMethodsTask(db)));

            // futures.add(taskHelper.submit(new GetProductsTask(db)));

            for (Future<?> future : futures) {

                try {
                    future.get();
                } catch (InterruptedException | ExecutionException e) {

                    taskHelper.shutdownNow();

                    executeSqlFile(db, DATABASE_DESTROY_SQL,
                            DATABASE_NAME + " failed to populate");

                    throw new RuntimeException(e);
                }
            }

        } finally {
            taskHelper.shutdown();
        }

    }

    /**
     * Inner class to retrieve all categories from the government API
     */
    private static class GetDataTask extends BackgroundTask<Integer, String> {

        private SQLiteDatabase db;
        private static final String[] catStrings = { "id", "name", "subcategory" };
        private static final String[] catJSONStrings = { "ID", "Category_Name", "Subcategory_Name" };

        private static final String[] cookTipStrings = {"id", "productId", "tips", "safeMinTemp",
                "restTime", "restTimeMetric" };
        private static final String[] cookTipJSONStrings = {"ID", "Product_ID", "Tips",
                "Safe_Minimum_Temperature", "Rest_Time", "Rest_Time_metric" };

        private static final String[] cookMethodStrings = { "id", "productId", "method", "measureFrom",
                "measureTo", "sizeMetric", "cookingTemp", "timingFrom", "timingTo", "timingMetric",
                "timingPer"};
        private static final String[] cookMethodJSONStrings = { "ID", "Product_ID", "Cooking_Method",
                "Measure_from", "Measure_to", "Size_metric", "Cooking_Temperature", "Timing_from",
                "Timing_to", "Timing_metric", "Timing_per"};

        private static final String[] productStrings = { "id", "categoryId", "name", "subtitle", "keywords",
                "min", "max", "metric", "tips",
                "min", "max", "metric", "tips",
                "min", "max", "metric",
                "min", "max", "metric", "tips",
                "min", "max", "metric", "tips",
                "min", "max", "metric",
                "min", "max", "metric",
                "min", "max", "metric", "tips",
                "min", "max", "metric", "tips"};
        private static final String[] productJSONStrings = { "ID", "Category_ID", "Name", "Name_subtitle", "Keywords",
                "Pantry_Min", "Pantry_Max", "Pantry_Metric", "Pantry_tips",
                "DOP_Pantry_Min", "DOP_Pantry_Max", "DOP_Pantry_Metric", "DOP_Pantry_tips",
                "Pantry_After_Opening_Min", "Pantry_After_Opening_Max", "Pantry_After_Opening_Metric",
                "Refrigerate_Min", "Refrigerate_Max", "Refrigerate_Metric", "Refrigerate_tips",
                "DOP_Refrigerate_Min", "DOP_Refrigerate_Max", "DOP_Refrigerate_Metric", "DOP_Refrigerate_tips",
                "Refrigerate_After_Opening_Min", "Refrigerate_After_Opening_Max", "Refrigerate_After_Opening_Metric",
                "Refrigerate_After_Thawing_Min", "Refrigerate_After_Thawing_Max", "Refrigerate_After_Thawing_Metric",
                "Freeze_Min", "Freeze_Max", "Freeze_Metric", "Freeze_Tips",
                "DOP_Freeze_Min", "DOP_Freeze_Max", "DOP_Freeze_Metric", "DOP_Freeze_Tips"};

        private final int[] pushArray = { 8, 12, 15, 19, 23, 26, 29, 33, 37 };

        public GetDataTask(@NonNull SQLiteDatabase db) { this.db = db; }

        @Override
        protected String doInBackground() throws Exception {

            HttpURLConnection urlConnection;
            BufferedReader reader;
            String result = "";

            // set the URL for the API call
            String apiCall = "https://www.fsis.usda.gov/shared/data/EN/foodkeeper.json";
            Log.d(DATABASE_NAME, "apiCall = " + apiCall);
            URL url = new URL(apiCall);
            // connect to the site to read information
            try {
                urlConnection = (HttpsURLConnection) url.openConnection();
                Log.d("urlConnection", "Connection Opened");
                urlConnection.setRequestMethod("GET");
                Log.d("urlConnection", "GET method sent");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.connect();
                Log.d("urlConnection", "Connection Established");

                // store the data retrieved by the request
                InputStream inputStream = urlConnection.getInputStream();

                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }

                // connect a BufferedReader to the input stream at URL
                reader = new BufferedReader(new InputStreamReader(inputStream));
                // store the data in result string
                String s;
                while ((s = reader.readLine()) != null)
                    result += s;

                return result;

            } catch (UnknownHostException e) {
                Log.e("API Connection", "Error: Failed to connect to website!");
            }

            // no data returned by the request, so terminate the method
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result != null) {
                Log.d(DATABASE_NAME, "JSON result length = " + result.length());
                try {
                    JSONArray resultsArray = new JSONObject(result).getJSONArray("sheets");
                    JSONArray versionArray = resultsArray.getJSONObject(0).getJSONArray("data");
                    JSONArray categoryArray = resultsArray.getJSONObject(1).getJSONArray("data");
                    JSONArray cookingTipsArray = resultsArray.getJSONObject(3).getJSONArray("data");
                    JSONArray cookingMethodsArray = resultsArray.getJSONObject(4).getJSONArray("data");
                    JSONArray productsArray = resultsArray.getJSONObject(2).getJSONArray("data");

                    // Check database version
                    int version = versionArray.getJSONArray(versionArray.length()-1).getJSONObject(0)
                            .getInt("Data_Version_Number");
                    Log.d("**TESTING**", "Current remote DB Version: " + version);
                    if (version != DATABASE_VERSION) {
                        DATABASE_VERSION = version;
                        mInstance = new NestDBOpenHelper(mInstance.mContext, version);
                        db = mInstance.getWritableDatabase();
                    }

                    Log.d("**TESTING**", "Local DB Version: " + db.getVersion());

                    db.beginTransaction();
                    // Populate Category Table
                    Log.d(DATABASE_NAME, "processing " + categoryArray.length() + " categories array entries...");
                    ContentValues values = new ContentValues();
                    int count = 0;
                    for(int i = 0; i < categoryArray.length(); i++) {
                        JSONArray entryArr = categoryArray.getJSONArray(i);
                        values.clear();

                        for (int j = 0; j < entryArr.length(); j++) {
                            values.put(catStrings[j], entryArr.getJSONObject(j).getString(catJSONStrings[j]));
                        }
                        
                        db.insert("categories", null, values);
                        count++;
                    }
                    Log.d(DATABASE_NAME, "inserted " + count + " categories");

                    // Populate Cooking Tips Table
                    Log.d(DATABASE_NAME, "processing " + cookingTipsArray.length() + " cookingTips array entries...");
                    values = new ContentValues();
                    count = 0;

                    for(int i = 0; i < cookingTipsArray.length(); i++) {
                        JSONArray entryArr = cookingTipsArray.getJSONArray(i);
                        values.clear();
                        for (int j = 0; j < entryArr.length(); j++) {
                            values.put(cookTipStrings[j], entryArr.getJSONObject(j).getString(cookTipJSONStrings[j]));
                        }
                        db.insert("cookingTips", null, values);
                        count++;
                    }

                    Log.d(DATABASE_NAME, "inserted " + count + " cookingTips");

                    // Populate Cooking Methods Table
                    Log.d(DATABASE_NAME, "processing " + cookingMethodsArray.length() + " cookingMethods array entries...");
                    values = new ContentValues();
                    count = 0;

                    for(int i = 0; i < cookingMethodsArray.length(); i++) {
                        JSONArray entryArr = cookingMethodsArray.getJSONArray(i);
                        values.clear();
                        for (int j = 0; j < entryArr.length(); j++) {
                            values.put(cookMethodStrings[j], entryArr.getJSONObject(j).getString(cookMethodJSONStrings[j]));
                        }
                        db.insert("cookingMethods", null, values);
                        count++;
                    }

                    Log.d(DATABASE_NAME, "inserted " + count + " cookingMethods");

                    // Populate Products Table
                    Log.d(DATABASE_NAME, "processing " + productsArray.length() + " product array entries...");
                    values = new ContentValues();
                    ContentValues slValues = new ContentValues(); // for shelfLives population
                    int slRecordsAdded = 0;
                    count = 0;
                    ArrayList<Integer> productPushPoints = new ArrayList<>();
                    for (int i : pushArray) {
                        productPushPoints.add(i);
                    }

                    for(int i = 0; i < productsArray.length(); i++) {
                        JSONArray entryArr = productsArray.getJSONArray(i);
                        values.clear();

                        int j;
                        for (j = 0; j < entryArr.length(); j++) {
                            if (j < 5)
                                values.put(productStrings[j], entryArr.getJSONObject(j).getString(productJSONStrings[j]));
                            else {
                                if (!entryArr.getJSONObject(j).isNull(productJSONStrings[j]))
                                    slValues.put(productStrings[j], entryArr.getJSONObject(j).getString(productJSONStrings[j]));
                                if (productPushPoints.contains(j) && slValues.size() > 0) {
                                    slValues.put("productId", values.getAsString("id"));
                                    if (j == pushArray[0]) {
                                        slValues.put("typeCode", "pl");
                                        slValues.put("typeIndex", 0);
                                    }
                                    else if (j == pushArray[1]) {
                                        slValues.put("typeCode", "dop_pl");
                                        slValues.put("typeIndex", 6);
                                    }
                                    else if (j == pushArray[2]) {
                                        slValues.put("typeCode", "paol");
                                        slValues.put("typeIndex", 1);
                                    }
                                    else if (j == pushArray[3]) {
                                        slValues.put("typeCode", "rl");
                                        slValues.put("typeIndex", 2);
                                    }
                                    else if (j == pushArray[4]) {
                                        slValues.put("typeCode", "raol");
                                        slValues.put("typeIndex", 3);
                                    }
                                    else if (j == pushArray[5]) {
                                        slValues.put("typeCode", "dop_rl");
                                        slValues.put("typeIndex", 7);
                                    }
                                    else if (j == pushArray[6]) {
                                        slValues.put("typeCode", "ratl");
                                        slValues.put("typeIndex", 4);
                                    }
                                    else if (j == pushArray[7]) {
                                        slValues.put("typeCode", "fl");
                                        slValues.put("typeIndex", 5);
                                    }
                                    else {
                                        slValues.put("typeCode", "dop_fl");
                                        slValues.put("typeIndex", 8);
                                    }

                                    db.insert("shelfLives", null, slValues);
                                    slRecordsAdded++;
                                    slValues.clear();
                                }
                            }

                        }
                        if (slValues.size() > 0) {
                            slValues.put("productId", values.getAsString("id"));
                            if (j == pushArray[0]) {
                                slValues.put("typeCode", "pl");
                                slValues.put("typeIndex", 0);
                            }
                            else if (j == pushArray[1]) {
                                slValues.put("typeCode", "dop_pl");
                                slValues.put("typeIndex", 6);
                            }
                            else if (j == pushArray[2]) {
                                slValues.put("typeCode", "paol");
                                slValues.put("typeIndex", 1);
                            }
                            else if (j == pushArray[3]) {
                                slValues.put("typeCode", "rl");
                                slValues.put("typeIndex", 2);
                            }
                            else if (j == pushArray[4]) {
                                slValues.put("typeCode", "raol");
                                slValues.put("typeIndex", 3);
                            }
                            else if (j == pushArray[5]) {
                                slValues.put("typeCode", "dop_rl");
                                slValues.put("typeIndex", 7);
                            }
                            else if (j == pushArray[6]) {
                                slValues.put("typeCode", "ratl");
                                slValues.put("typeIndex", 4);
                            }
                            else if (j == pushArray[7]) {
                                slValues.put("typeCode", "fl");
                                slValues.put("typeIndex", 5);
                            }
                            else {
                                slValues.put("typeCode", "dop_fl");
                                slValues.put("typeIndex", 8);
                            }
                            db.insert("shelfLives", null, slValues);
                            slRecordsAdded++;
                            slValues.clear();
                        }

                        db.insert("products", null, values);
                        count++;

                        // handle all the different shelf life fields - DEPRECATED
                        /*for(int j = 0; j < jsonNames.length; j++) {
                            JSONObject jsonShelfLife = jsonObj.getJSONObject(jsonNames[j]);
                            slValues.clear();   // begin possible new shelfLives record
                            for(String f:fields) {
                                //(unComment to store shelf life values in products) String dbField = dbPrefixes[j] + "_" + f;
                                if (!jsonShelfLife.isNull(f)) {
                                    //(unComment to store shelf life values in products) values.put(dbField, jsonShelfLife.getString(f));
                                    slValues.put(f, jsonShelfLife.getString(f));
                                }
                            }
                            // add record to shelfLives if something to add
                            if (slValues.size() > 0) {
                                // fill product id, shelf life type code and index value and add record
                                slValues.put("productId", values.getAsString("id"));
                                slValues.put("typeCode", dbPrefixes[j].toUpperCase());
                                slValues.put("typeIndex", j);  // matches ShelfLife.java
                                db.insert("shelfLives", null, slValues);
                                db.yieldIfContendedSafely();
                                slRecordsAdded++;
                            }
                        }
                        db.insert("products", null, values);*/
                        db.yieldIfContendedSafely();
                    }

                    db.setTransactionSuccessful();
                    Log.d(DATABASE_NAME, "inserted " + count + " products");
                    Log.d(DATABASE_NAME, "inserted " + slRecordsAdded + " shelfLives records");

                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    db.endTransaction();
                }
            } else {
                Log.d(DATABASE_NAME, "Couldn't get any categories data from the url");
            }

        }

    }
// These classes have been combined into a single class. Since the government database puts all
// info into a single JSON file we no longer need multiple API calls.
//    /**
//     * Inner class to retrieve all cookingTips from the government API
//     */
//    private static class GetCookingTipsTask extends BackgroundTask<Integer, String> {
//
//        private final SQLiteDatabase db;
//
//        public GetCookingTipsTask(@NonNull SQLiteDatabase db) { this.db = db; }
//
//        @Override
//        protected String doInBackground() throws IOException {
//
//            HttpURLConnection urlConnection;
//            BufferedReader reader;
//            String result = "";
//
//            // set the URL for the API call
//            String apiCall = "https://www.fsis.usda.gov/shared/data/EN/foodkeeper.json";
//            Log.d(DATABASE_NAME, "apiCall = " + apiCall);
//            URL url = new URL(apiCall);
//            // connect to the site to read information
//            urlConnection = (HttpsURLConnection) url.openConnection();
//            urlConnection.setRequestMethod("GET");
//            urlConnection.connect();
//
//            // store the data retrieved by the request
//            InputStream inputStream = urlConnection.getInputStream();
//            // no data returned by the request, so terminate the method
//            if (inputStream == null) {
//                // Nothing to do.
//                return null;
//            }
//
//            // connect a BufferedReader to the input stream at URL
//            reader = new BufferedReader(new InputStreamReader(inputStream));
//            // store the data in result string
//            String s;
//            while ((s = reader.readLine()) != null)
//                result += s;
//
//            return result;
//
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//
//            if (result != null) {
//                Log.d(DATABASE_NAME, "cookingTips JSON result length = " + result.length());
//                db.beginTransaction();
//                try {
//                    JSONArray jsonArray = new JSONObject(result).getJSONArray("sheets")
//                            .getJSONObject(3).getJSONArray("data");
//                    Log.d(DATABASE_NAME, "processing " + jsonArray.length() + " cookingTips array entries...");
//                    ContentValues values = new ContentValues();
//                    for(int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject jsonObj = jsonArray.getJSONObject(i);
//                        values.clear();
//                        values.put("id", jsonObj.getString("ID"));
//                        values.put("tips", jsonObj.getString("Tips"));
//                        values.put("safeMinTemp", jsonObj.getInt("Safe_Minimum_Temperature"));
//                        values.put("restTime", jsonObj.getInt("Rest_Time"));
//                        values.put("restTimeMetric", jsonObj.getString("Rest_Time_Metric"));
//                        values.put("productId", jsonObj.getString("Product_ID"));
//                        db.insert("cookingTips", null, values);
//                        db.yieldIfContendedSafely();
//                    }
//                    db.setTransactionSuccessful();
//                    Log.d(DATABASE_NAME, "inserted " + jsonArray.length() + " cookingTips");
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } finally {
//                    db.endTransaction();
//                }
//
//                Log.d(DATABASE_NAME, "Category Data Completed");
//            } else {
//                Log.d(DATABASE_NAME, "Couldn't get any cookingTips data from the url");
//            }
//
//        }
//
//    }
//
//    /**
//     * Inner class to retrieve all cookingmethods from the government API
//     */
//    private static class GetCookingMethodsTask extends BackgroundTask<Integer, String> {
//
//        private final SQLiteDatabase db;
//
//        public GetCookingMethodsTask(@NonNull SQLiteDatabase db) { this.db = db; }
//
//        @Override
//        protected String doInBackground() throws IOException {
//
//            HttpURLConnection urlConnection;
//            BufferedReader reader;
//            String result = "";
//
//            // set the URL for the API call
//            String apiCall = "https://www.fsis.usda.gov/shared/data/EN/foodkeeper.json";
//            Log.d(DATABASE_NAME, "apiCall = " + apiCall);
//            URL url = new URL(apiCall);
//            // connect to the site to read information
//            urlConnection = (HttpsURLConnection) url.openConnection();
//            urlConnection.setRequestMethod("GET");
//            urlConnection.connect();
//
//            // store the data retrieved by the request
//            InputStream inputStream = urlConnection.getInputStream();
//            // no data returned by the request, so terminate the method
//            if (inputStream == null) {
//                // Nothing to do.
//                return null;
//            }
//
//            // connect a BufferedReader to the input stream at URL
//            reader = new BufferedReader(new InputStreamReader(inputStream));
//            // store the data in result string
//            String s;
//            while ((s = reader.readLine()) != null)
//                result += s;
//
//            return result;
//
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//
//            if (result != null) {
//                Log.d(DATABASE_NAME, "cookingMethods JSON result length = " + result.length());
//                db.beginTransaction();
//                try {
//                    JSONArray jsonArray = new JSONObject(result).getJSONArray("sheets")
//                            .getJSONObject(4).getJSONArray("data");
//                    Log.d(DATABASE_NAME, "processing " + jsonArray.length() + " cookingMethods array entries...");
//                    ContentValues values = new ContentValues();
//                    for(int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject jsonObj = jsonArray.getJSONObject(i);
//                        values.clear();
//                        values.put("id", jsonObj.getInt("ID"));
//                        values.put("method", jsonObj.getString("Cooking_Method"));
//                        values.put("measureFrom", jsonObj.getInt("Measure_from"));
//                        values.put("measureTo", jsonObj.getInt("Measure_to"));
//                        values.put("sizeMetric", jsonObj.getString("Size_metric"));
//                        values.put("cookingTemp", jsonObj.getInt("Cooking_Temperature"));
//                        values.put("timingFrom", jsonObj.getInt("Timing_from"));
//                        values.put("timingTo", jsonObj.getInt("Timing_to"));
//                        values.put("timingMetric", jsonObj.getString("Timing_metric"));
//                        values.put("timingPer", jsonObj.getString("Timing_per"));
//                        values.put("productId", jsonObj.getInt("Product_ID"));
//                        db.insert("cookingMethods", null, values);
//                        db.yieldIfContendedSafely();
//                    }
//                    db.setTransactionSuccessful();
//                    Log.d(DATABASE_NAME, "inserted " + jsonArray.length() + " cookingMethods");
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } finally {
//                    db.endTransaction();
//                }
//                Log.d(DATABASE_NAME, "Category Data Completed");
//            } else {
//                Log.d(DATABASE_NAME, "Couldn't get any cookingMethods data from the url");
//            }
//
//        }
//
//    }
//
//    /**
//     * Inner class to retrieve all products from the government API
//     */
//    private static class GetProductsTask extends BackgroundTask<Integer, String> {
//
//        private final SQLiteDatabase db;
//
//        public GetProductsTask(@NonNull SQLiteDatabase db) { this.db = db; }
//
//        @Override
//        protected String doInBackground() throws IOException {
//
//            HttpURLConnection urlConnection;
//            BufferedReader reader;
//            String result = "";
//
//            // set the URL for the API call
//            String apiCall = "https://www.fsis.usda.gov/shared/data/EN/foodkeeper.json";
//            Log.d(DATABASE_NAME, "apiCall = " + apiCall);
//            URL url = new URL(apiCall);
//            // connect to the site to read information
//            urlConnection = (HttpsURLConnection) url.openConnection();
//            urlConnection.setRequestMethod("GET");
//            urlConnection.connect();
//
//            // store the data retrieved by the request
//            InputStream inputStream = urlConnection.getInputStream();
//            // no data returned by the request, so terminate the method
//            if (inputStream == null) {
//                // Nothing to do.
//                return null;
//            }
//
//            // connect a BufferedReader to the input stream at URL
//            reader = new BufferedReader(new InputStreamReader(inputStream));
//            // store the data in result string
//            String s;
//            while ((s = reader.readLine()) != null)
//                result += s;
//
//            return result;
//
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//
//            if (result != null) {
//                Log.d(DATABASE_NAME, "products JSON result length = " + result.length());
//                db.beginTransaction();
//                try {
//                    JSONArray jsonArray = new JSONObject(result).getJSONArray("sheets")
//                            .getJSONObject(2).getJSONArray("data");
//                    Log.d(DATABASE_NAME, "processing " + jsonArray.length() + " product array entries...");
//                    // string arrays used for the different shelf life fields (db side and api json side)
//                    String[] fields = { "min", "max", "metric", "tips"};
//                    // DEPRECATED String[] dbPrefixes = { "pl", "paol", "rl", "raol", "ratl", "fl", "dop_pl", "dop_rl", "dop_fl" };
//                    /* DEPRECATED String[] jsonNames = {
//                            "pantryLife",
//                            "pantryAfterOpeningLife",
//                            "refrigeratorLife",
//                            "refrigerateAfterOpeningLife",
//                            "refrigerateAfterThawingLife",
//                            "freezerLife",
//                            "dop_pantryLife",
//                            "dop_refrigeratorLife",
//                            "dop_freezerLife"
//                    };*/
//                    ContentValues values = new ContentValues();
//                    ContentValues slValues = new ContentValues(); // for shelfLives population
//                    int slRecordsAdded = 0;
//                    for(int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject jsonObj = jsonArray.getJSONObject(i);
//                        values.clear();
//                        values.put("id", jsonObj.getInt("ID"));
//                        values.put("categoryId", jsonObj.getInt("Category_ID"));
//                        values.put("name", jsonObj.getString("Name"));
//                        values.put("subtitle", jsonObj.getString("Name_subtitle"));
//                        values.put("keywords", jsonObj.getString("Keywords"));
//
//                        // Checks for Pantry Shelf Life
//                        if (!jsonObj.isNull("Pantry_Min") || !jsonObj.isNull("Pantry_tips")) {
//                            slValues.clear();
//
//                            slValues.put(fields[0], jsonObj.getInt("Pantry_Min"));
//                            slValues.put(fields[1], jsonObj.getInt("Pantry_Max"));
//                            slValues.put(fields[2], jsonObj.getString("Pantry_Metric"));
//                            if (!jsonObj.isNull("Pantry_tips"))
//                                slValues.put(fields[3], jsonObj.getString("Pantry_tips"));
//                            else
//                                slValues.put(fields[3], "");
//                            slValues.put("productId", values.getAsString("id"));
//                            slValues.put("typeCode", "pl");
//                            slValues.put("typeIndex", slRecordsAdded);
//                            slRecordsAdded++;
//                            db.insert("shelfLives", null, slValues);
//                            db.yieldIfContendedSafely();
//                        }
//
//                        // Checks for DOP Pantry Shelf Life
//                        if (!jsonObj.isNull("DOP_Pantry_Min") || !jsonObj.isNull("DOP_Pantry_tips")) {
//                            slValues.clear();
//
//                            slValues.put(fields[0], jsonObj.getInt("DOP_Pantry_Min"));
//                            slValues.put(fields[1], jsonObj.getInt("DOP_Pantry_Max"));
//                            slValues.put(fields[2], jsonObj.getString("DOP_Pantry_Metric"));
//                            if (!jsonObj.isNull("DOP_Pantry_tips"))
//                                slValues.put(fields[3], jsonObj.getString("DOP_Pantry_tips"));
//                            else
//                                slValues.put(fields[3], "");
//                            slValues.put("productId", values.getAsString("id"));
//                            slValues.put("typeCode", "dop_pl");
//                            slValues.put("typeIndex", slRecordsAdded);
//                            slRecordsAdded++;
//                            db.insert("shelfLives", null, slValues);
//                            db.yieldIfContendedSafely();
//                        }
//
//                        // Checks for Pantry After Opening Shelf Life
//                        if (!jsonObj.isNull("Pantry_After_Opening_Min")) {
//                            slValues.clear();
//
//                            slValues.put(fields[0], jsonObj.getInt("Pantry_After_Opening_Min"));
//                            slValues.put(fields[1], jsonObj.getInt("Pantry_After_Opening_Max"));
//                            slValues.put(fields[2], jsonObj.getString("Pantry_After_Opening_Metric"));
//                            slValues.put(fields[3], "");
//                            slValues.put("productId", values.getAsString("id"));
//                            slValues.put("typeCode", "paol");
//                            slValues.put("typeIndex", slRecordsAdded);
//                            slRecordsAdded++;
//                            db.insert("shelfLives", null, slValues);
//                            db.yieldIfContendedSafely();
//                        }
//
//                        // Checks for Refrigeration Shelf Life
//                        if (!jsonObj.isNull("Refrigerate_Min") || !jsonObj.isNull("Refrigerate_tips")) {
//                            slValues.clear();
//
//                            slValues.put(fields[0], jsonObj.getInt("Refrigerate_Min"));
//                            slValues.put(fields[1], jsonObj.getInt("Refrigerate_Max"));
//                            slValues.put(fields[2], jsonObj.getString("Refrigerate_Metric"));
//                            if (!jsonObj.isNull("Refrigerate_tips"))
//                                slValues.put(fields[3], jsonObj.getString("Refrigerate_tips"));
//                            else
//                                slValues.put(fields[3], "");
//                            slValues.put("productId", values.getAsString("id"));
//                            slValues.put("typeCode", "rl");
//                            slValues.put("typeIndex", slRecordsAdded);
//                            slRecordsAdded++;
//                            db.insert("shelfLives", null, slValues);
//                            db.yieldIfContendedSafely();
//                        }
//
//                        // Checks for DOP Refrigeration Shelf Life
//                        if (!jsonObj.isNull("DOP_Refrigerate_Min") || !jsonObj.isNull("DOP_Refrigerate_tips")) {
//                            slValues.clear();
//
//                            slValues.put(fields[0], jsonObj.getInt("DOP_Refrigerate_Min"));
//                            slValues.put(fields[1], jsonObj.getInt("DOP_Refrigerate_Max"));
//                            slValues.put(fields[2], jsonObj.getString("DOP_Refrigerate_Metric"));
//                            if (!jsonObj.isNull("DOP_Refrigerate_tips"))
//                                slValues.put(fields[3], jsonObj.getString("DOP_Refrigerate_tips"));
//                            else
//                                slValues.put(fields[3], "");
//                            slValues.put("productId", values.getAsString("id"));
//                            slValues.put("typeCode", "dop_rl");
//                            slValues.put("typeIndex", slRecordsAdded);
//                            slRecordsAdded++;
//                            db.insert("shelfLives", null, slValues);
//                            db.yieldIfContendedSafely();
//                        }
//
//                        // Checks for Refrigeration After Opening Shelf Life
//                        if (!jsonObj.isNull("Refrigerate_After_Opening_Min")) {
//                            slValues.clear();
//
//                            slValues.put(fields[0], jsonObj.getInt("Refrigerate_After_Opening_Min"));
//                            slValues.put(fields[1], jsonObj.getInt("Refrigerate_After_Opening_Max"));
//                            slValues.put(fields[2], jsonObj.getString("Refrigerate_After_Opening_Metric"));
//                            slValues.put(fields[3], "");
//                            slValues.put("productId", values.getAsString("id"));
//                            slValues.put("typeCode", "raol");
//                            slValues.put("typeIndex", slRecordsAdded);
//                            slRecordsAdded++;
//                            db.insert("shelfLives", null, slValues);
//                            db.yieldIfContendedSafely();
//                        }
//
//                        // Checks for Refrigeration After Thawing Shelf Life
//                        if (!jsonObj.isNull("Refrigerate_After_Thawing_Min")) {
//                            slValues.clear();
//
//                            slValues.put(fields[0], jsonObj.getInt("Refrigerate_After_Thawing_Min"));
//                            slValues.put(fields[1], jsonObj.getInt("Refrigerate_After_Thawing_Max"));
//                            slValues.put(fields[2], jsonObj.getString("Refrigerate_After_Thawing_Metric"));
//                            slValues.put(fields[3], "");
//                            slValues.put("productId", values.getAsString("id"));
//                            slValues.put("typeCode", "ratl");
//                            slValues.put("typeIndex", slRecordsAdded);
//                            slRecordsAdded++;
//                            db.insert("shelfLives", null, slValues);
//                            db.yieldIfContendedSafely();
//                        }
//
//                        // Checks for Freeze Shelf Life
//                        if (!jsonObj.isNull("Freeze_Min") || !jsonObj.isNull("Freeze_Tips")) {
//                            slValues.clear();
//
//                            slValues.put(fields[0], jsonObj.getInt("Freeze_Min"));
//                            slValues.put(fields[1], jsonObj.getInt("Freeze_Max"));
//                            slValues.put(fields[2], jsonObj.getString("Freeze_Metric"));
//                            if (!jsonObj.isNull("Freeze_Tips"))
//                                slValues.put(fields[3], jsonObj.getString("Freeze_Tips"));
//                            else
//                                slValues.put(fields[3], "");
//                            slValues.put("productId", values.getAsString("id"));
//                            slValues.put("typeCode", "fl");
//                            slValues.put("typeIndex", slRecordsAdded);
//                            slRecordsAdded++;
//                            db.insert("shelfLives", null, slValues);
//                            db.yieldIfContendedSafely();
//                        }
//
//                        // Checks for DOP Freeze Shelf Life
//                        if (!jsonObj.isNull("DOP_Freeze_Min") || !jsonObj.isNull("DOP_Freeze_Tips")) {
//                            slValues.clear();
//
//                            slValues.put(fields[0], jsonObj.getInt("DOP_Freeze_Min"));
//                            slValues.put(fields[1], jsonObj.getInt("DOP_Freeze_Max"));
//                            slValues.put(fields[2], jsonObj.getString("DOP_Freeze_Metric"));
//                            if (!jsonObj.isNull("DOP_Freeze_Tips"))
//                                slValues.put(fields[3], jsonObj.getString("DOP_Freeze_Tips"));
//                            else
//                                slValues.put(fields[3], "");
//                            slValues.put("productId", values.getAsString("id"));
//                            slValues.put("typeCode", "dop_fl");
//                            slValues.put("typeIndex", slRecordsAdded);
//                            slRecordsAdded++;
//                            db.insert("shelfLives", null, slValues);
//                            db.yieldIfContendedSafely();
//                        }
//
//                        // handle all the different shelf life fields - DEPRECATED
//                        /*for(int j = 0; j < jsonNames.length; j++) {
//                            JSONObject jsonShelfLife = jsonObj.getJSONObject(jsonNames[j]);
//                            slValues.clear();   // begin possible new shelfLives record
//                            for(String f:fields) {
//                                //(unComment to store shelf life values in products) String dbField = dbPrefixes[j] + "_" + f;
//                                if (!jsonShelfLife.isNull(f)) {
//                                    //(unComment to store shelf life values in products) values.put(dbField, jsonShelfLife.getString(f));
//                                    slValues.put(f, jsonShelfLife.getString(f));
//                                }
//                            }
//                            // add record to shelfLives if something to add
//                            if (slValues.size() > 0) {
//                                // fill product id, shelf life type code and index value and add record
//                                slValues.put("productId", values.getAsString("id"));
//                                slValues.put("typeCode", dbPrefixes[j].toUpperCase());
//                                slValues.put("typeIndex", j);  // matches ShelfLife.java
//                                db.insert("shelfLives", null, slValues);
//                                db.yieldIfContendedSafely();
//                                slRecordsAdded++;
//                            }
//                        }
//                        db.insert("products", null, values);
//                        db.yieldIfContendedSafely();*/
//                    }
//                    db.setTransactionSuccessful();
//                    Log.d(DATABASE_NAME, "inserted " + jsonArray.length() + " products");
//                    Log.d(DATABASE_NAME, "inserted " + slRecordsAdded + " shelfLives records");
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } finally {
//                    db.endTransaction();
//                }
//                Log.d(DATABASE_NAME, "Category Data Completed");
//            } else {
//                Log.d(DATABASE_NAME, "Couldn't get any data from the url");
//            }
//
//        }
//
//    }

}
