package edu.ncc.nest.nestapp.reports.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses.GuestVisitSource;
import edu.ncc.nest.nestapp.R;
import edu.ncc.nest.nestapp.databinding.FragmentReportBinding;


/**
 *
 * Copyright (C) 2023 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

public class ReportFragment extends Fragment {
    GuestVisitSource dbtest;
    private FragmentReportBinding binding;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        dbtest = new GuestVisitSource(this.getContext());
        binding = FragmentReportBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.regReportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ReportFragment.this)
                        .navigate(R.id.action_ReportFragment_to_RegReportFragment);
            }
        });


        binding.questReportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavHostFragment.findNavController(ReportFragment.this)
                        .navigate(R.id.action_ReportFragment_to_QuestReportFragment);
            }
        });
        //this is for testing the Query methods
        binding.testingdbQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbtest.open();
                dbtest.submitQuestionnaire("123456","245","1","2","0","0","11142022");
                dbtest.submitQuestionnaire("112233","63","1","1","0","0","11122022");
                dbtest.submitQuestionnaire("554466","22","1","2","0","0","12112022");
                dbtest.submitQuestionnaire("223344","45","2","2","0","0","11142022");
                dbtest.submitQuestionnaire("334455","56","38","2","0","0","12152022");
                dbtest.submitQuestionnaire("445566","2567","1","4","0","3","12162022");
                dbtest.submitQuestionnaire("556677","378","5","2","0","0","12232022");
                dbtest.submitQuestionnaire("778899","72","19999","5","0","0","12122022");
                dbtest.submitQuestionnaire("990088","18","1","2","0","0","11232022");
                dbtest.close();
            }
        });


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}